import { GameBox } from "@raining.cards/server";

import { TABLE_OF_BLUFFS_BOX } from "@raining.cards/game--table-of-bluffs";
import { GARDENERS_BOX } from "@raining.cards/game--gardeners";
import { WHO_KNOWS_WHOM_BOX } from "@raining.cards/app--who-knows-whom";
import { SCARED_YET_BOX } from "@raining.cards/game--scared-yet_";

export const GAME_BOXES: GameBox<any, any>[] = [
  TABLE_OF_BLUFFS_BOX,
  GARDENERS_BOX,
  SCARED_YET_BOX,
  WHO_KNOWS_WHOM_BOX,
];
