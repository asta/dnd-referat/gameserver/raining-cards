export {};

declare global {
  interface ImportMeta {
    env: ImportMetaEnv;
  }

  interface ImportMetaEnv {
    // must contain trailing slash
    BASE_URL: string;
    // must contain trailing slash
    VITE_API_URL: string;
    // defaults to API URL with protocol replacement (http -> ws, https -> wss)
    VITE_WS_URL?: string;
  }
}
