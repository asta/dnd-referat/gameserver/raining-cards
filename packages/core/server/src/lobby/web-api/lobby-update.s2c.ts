import { Lobby } from "@/lobby/Lobby";
import {
  AnyGameIds,
  S2CMessageDTOLobbyUpdate,
  ServerCommand,
} from "@raining.cards/common";
import { lobbyToDTO } from "@/lobby/lobby.dto";

export interface S2CMessageServerLobbyUpdate<GI extends AnyGameIds, GameProps> {
  cmd: ServerCommand.LOBBY_TEAMS | ServerCommand.GAME_OPTIONS;
  lobby: Lobby<GI, GameProps>;
}

export function s2cLobbyUpdateToDTO<GI extends AnyGameIds, GameProps>({
  lobby,
}: S2CMessageServerLobbyUpdate<GI, GameProps>): S2CMessageDTOLobbyUpdate {
  return lobbyToDTO(lobby);
}
