import {
  S2CMessageDTONotification,
  S2CNotificationType,
  ServerCommand,
} from "@raining.cards/common";

export interface S2CMessageServerNotification {
  cmd: ServerCommand.NOTIFICATION;
  date?: Date;
  type: S2CNotificationType;
  timeout?: number | false;
  message: string;
  title?: string;
}

export function s2cNotificationToDTO({
  date,
  type,
  timeout,
  message,
  title,
}: S2CMessageServerNotification): S2CMessageDTONotification {
  return [
    date?.getTime() ?? null,
    type,
    timeout ?? null,
    message,
    title ?? null,
  ];
}
