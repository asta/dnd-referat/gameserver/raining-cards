import { AnyGameIds, UIMeta } from "@/types";

export interface MaterialFaceTypeDTO<GI extends AnyGameIds> {
  id: GI["materialFaceType"];
  label?: string;
  description?: string;
  uiMeta?: UIMeta;
}

export interface MaterialFaceDTO<GI extends AnyGameIds> {
  typeId: GI["materialFaceType"];
  customMeta?: Partial<Omit<MaterialFaceTypeDTO<GI>, "id">>;
}

export interface MaterialViewDTO<GI extends AnyGameIds> {
  global: MaterialFaceDTO<GI>;
  secret?: MaterialFaceDTO<GI>;
}

export interface MaterialChunkDTO<GI extends AnyGameIds>
  extends MaterialViewDTO<GI> {
  count: number;
}

export interface MaterialFaceTypeGroupDTO<GI extends AnyGameIds> {
  id: GI["materialFaceTypeGroup"];
  faceIds: GI["materialFaceType"][];
}
