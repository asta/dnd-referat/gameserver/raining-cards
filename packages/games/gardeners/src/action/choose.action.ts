import { defineAction, GameLogs, IdFactory } from "@raining.cards/server";
import { titleCase } from "@raining.cards/util";
import { G_MATERIAL_COLORS } from "@/material/material-faces";
import { ElementType, GameIds } from "@raining.cards/common";
import { HighestInfo } from "@/state/highest-info";
import { Dealer } from "@/dealer";

export function useActionChoose(
  idf: IdFactory<GameIds["actionSchema"]>,
  dealer: Dealer,
  highestInfo: HighestInfo,
  gameLogs: GameLogs
) {
  return G_MATERIAL_COLORS.map((color) =>
    defineAction(
      {
        id: idf(`choose:${color}`),
        name: titleCase(color),
        description:
          "The other players must serve this color (or duck with a white card), if possible.",
        origin: { type: ElementType.GLOBAL },
        target: { type: ElementType.GLOBAL },
      },

      (action) => {
        highestInfo.set(action.player, color);
        dealer.setToken(color);
        gameLogs.add(`${action.player.name} chooses a ${color} trick`);
      }
    )
  );
}
