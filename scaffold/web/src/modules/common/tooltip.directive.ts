import tippy, { Instance, Placement } from "tippy.js";
import { ObjectDirective, DirectiveBinding } from "@vue/runtime-core";

const INSTANCES = new WeakMap<HTMLElement, Instance>();

export const tooltip: ObjectDirective<HTMLElement> = {
  mounted: createTippy,
  updated: createTippy,
  unmounted(el) {
    INSTANCES.get(el)?.destroy();
  },
};

function createTippy(el: HTMLElement, binding: DirectiveBinding) {
  INSTANCES.get(el)?.destroy();
  if (!binding.value) {
    return;
  }
  const instance = tippy(el, {
    content: binding.value,
    placement: binding.arg as Placement,
    allowHTML: binding.modifiers.html,
  });
  INSTANCES.set(el, instance);
}
