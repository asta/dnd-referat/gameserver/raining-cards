import { createApp } from "vue";
import TheApp from "./TheApp.vue";
import Toast from "vue-toastification";
import router from "./router";
import { tooltip } from "@/modules/common/tooltip.directive";

const app = createApp(TheApp).use(router).use(Toast);

app.directive("tooltip", tooltip);

app.mount("#app");
