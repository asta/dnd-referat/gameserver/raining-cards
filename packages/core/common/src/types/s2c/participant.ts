import { UIMeta } from "./dom";

export interface ParticipantsDTO {
  players: PlayerDTO[];
  spectators: SpectatorDTO[];
  teams?: PlayerDTO["id"][][];
}

export interface PlayerDTO {
  id: string;
  name: string;
  uiMeta?: UIMeta;
  secret?: string;
}

export interface SpectatorDTO {
  id: string;
  name: string;
  uiMeta?: UIMeta;
  secret?: string;
}

export type ClientDTO = PlayerDTO | SpectatorDTO;
