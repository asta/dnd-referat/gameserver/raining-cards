import { onUnmounted, ref, Ref } from "vue";
import {
  S2CMessageClientUpdateClosure,
  UIActionRevision,
  UIGameLog,
  UIGameState,
} from "@raining.cards/client";

export function useGameLogs(gameState: UIGameState): Ref<UIGameLog[]> {
  const res = ref([...gameState.logs]);
  onUnmounted(gameState.on("log", () => (res.value = [...gameState.logs])));
  return res;
}

export function useAllowedActions(
  gameState: UIGameState
): Ref<UIActionRevision> {
  const res = ref(gameState.allowedActions);
  onUnmounted(
    gameState.on("allowedActions", ({ current }) => (res.value = current))
  );
  return res;
}

export function useGameClosure(
  gameState: UIGameState
): Ref<S2CMessageClientUpdateClosure | undefined> {
  const res = ref<S2CMessageClientUpdateClosure>();
  onUnmounted(gameState.once("closure", (it) => (res.value = it)));
  return res;
}
