import {
  DefinedGame,
  defineGame,
  GameLogs,
  Material,
  Participants,
  Player,
  pushMultiUpdate,
  Transaction,
  TurnOrder,
} from "@raining.cards/server";
import { Dealer } from "@/dealer";
import { Score } from "@/state/score";
import { ActionSchemaGroups } from "@/action/action-schema-groups";
import { HighestInfo } from "@/state/highest-info";
import { Zones } from "@/zones";
import { pickOne } from "@raining.cards/util";
import { Actions } from "@/action/actions";
import { Materials } from "@/material/material";
import { GameMeta } from "@/game-meta";

export type RoundScoreData = { player: Player; points: number; total: number };

export function useGame(
  participants: Participants,
  transaction: Transaction,
  gameMeta: GameMeta,
  dealer: Dealer,
  score: Score,
  gameLogs: GameLogs,
  turnOrder: TurnOrder,
  actionSchemaGroups: ActionSchemaGroups,
  highestInfo: HighestInfo,
  zones: Zones,
  materials: Materials,
  actions: Actions
): DefinedGame {
  return defineGame({
    meta: gameMeta,
    board: zones.board,
    main: async () => {
      for (let i = 0; i < 5; i++) {
        turnOrder.current = pickOne(turnOrder.players);
        dealer.deal();
        await roundFlow();
      }
      pushMultiUpdate(participants, transaction.close()).catch(console.error);
    },
    closure: score.closure,
    logs: gameLogs,
  });

  async function roundFlow() {
    const startPlayer = turnOrder.current;
    gameLogs.add(`${startPlayer.name} has drawn the sun and may begin`);
    dealer.setToken();
    transaction.addPlayerAction(startPlayer, actionSchemaGroups.choose);
    await actions.next();
    turnOrder.next();
    // don't let start player play a card for first trick
    let firstTrick = true;
    const _len = turnOrder.size;
    do {
      if (!firstTrick) {
        dealer.setToken();
      }
      for (let i = 0; i < (firstTrick ? _len - 1 : _len); i++) {
        const player = turnOrder.current;
        transaction.addPlayerAction(
          player,
          actionSchemaGroups.of(player).playCard
        );
        await actions.next();
        turnOrder.next();
      }
      transaction.flush(1600);
      const highestPlayer = highestInfo.access().player;
      await takeTrick(highestPlayer);
      turnOrder.current = highestPlayer;
      highestInfo.set(highestPlayer);
      firstTrick = false;
    } while (!roundEnds());
    const roundScores = collectRoundScores();
    gameLogs.add(
      `— ${roundScores
        .map(
          ({ player, points, total }) =>
            `${player.name} ${total} (${points >= 0 ? "+" : ""}${points})`
        )
        .join("; ")} —`
    );
  }

  function roundEnds(): boolean {
    const playerZones = zones.player.asLookup[highestInfo.access().player.id];
    if (playerZones.hand.materials.length === 0) {
      return true;
    }
    // current player got the trick, so they have the only possible game-ending
    // score
    return playerZones.score.materials.length > 4;
  }

  async function takeTrick(player: Player) {
    const { score } = zones.player.asLookup[player.id];
    const clippers = dealer.takeAndCountClippers(score);
    const hasLost = score.materials.length > 4;
    if (hasLost) {
      gameLogs.add(`${player.name} got his 5th pile and concludes the round`);
    } else if (clippers > 0) {
      gameLogs.add(
        `${player.name} got the trick and discards ${clippers} pile(s)`
      );
      await dealer.dropHighestCard(score, clippers);
    } else {
      gameLogs.add(`${player.name} got the trick`);
    }
  }

  function collectRoundScores(): RoundScoreData[] {
    const roundScores: RoundScoreData[] = [];
    for (const player of turnOrder.players) {
      const scoreZone = zones.player.asLookup[player.id].score;
      let points: number;
      if (scoreZone.materials.length > 4) {
        points = -3;
      } else {
        points = scoreZone.materials.reduce(
          (value: number, material: Material) =>
            value + materials.getColorAndValue(material)!.value,
          0
        );
      }
      score.add(player, points);
      roundScores.push({
        player,
        points,
        total: score.access(player),
      });
    }
    return roundScores.sort((a, b) => b.total - a.total);
  }
}
