// API

export { uiFetchGameBoxes as uiInit } from "./game-boxes/game-boxes.service";
export { isS2CMessageClientError } from "./lobby/dto/s2c/error";

// Types

export * from "./area/zone.types";
// todo remove this export
export { S2CMessageClientUpdateClosure } from "./game/dto/s2c/update-closure";
export { UIGameBoxesState } from "./game-boxes/game-boxes.service";
export { UISocketState } from "./lobby/socket.service";
export {
  UILobbyState,
  UILobbyReconnectOptions,
  UILobbyJoinOptions,
  UILobbyCreateOptions,
} from "./lobby/lobby.service";
export * from "./game/action-schema.types";
export { UIGameState } from "./game/game.service";
export * from "./game/game.types";
export * from "./game/game-delta.types";
export * from "./game/material";
