[Base Repository](https://gitlab.com/xoria/raining-cards/)

# A Raining Cards Game: Table of Bluffs

- Players: 2 - 6
- Winner: Last player alive
- Related games:
  [Coup](https://www.indieboardsandcards.com/index.php/games/coup/)
  ([BGG](https://www.boardgamegeek.com/boardgame/131357/coup)),
  [Coup: Reformation](https://indieboardsandcards.com/index.php/our-games/coup-reformation/)
  ([BGG](https://boardgamegeek.com/boardgame/148931/coup-reformation))

In **Table of Bluffs** each player starts with two roles and two credits. The
roles are secret to the player.
In turn order each player can perform a single action until only one player is
left. Every player can perform any action or reaction of any role, regardless of
him/her owning such a role. Some actions can be reacted to by the other players.
Each role related action can be doubted. If a player has been doubted and does
not own the role for the action he attempted, he permanently loses one role; a
player without role withdraws from the game.
If on the other hand a doubt was not successful, the player who initiated the
doubt loses one role.

In addition, each player is assigned one of two teams. The affiliation can be
changed during the game due to player actions. If two, but not all, players are
in the same team, those players cannot perform any role related actions,
reactions and doubts against each other.
