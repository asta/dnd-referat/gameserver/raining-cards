import {
  defineActionSchemaGroup,
  idFactory,
  Lobby,
  Player,
  Transaction,
  useActions as useCoreActions,
} from "@raining.cards/server";
import { Zones } from "@/zones";
import { onceBy, withoutUnique } from "@raining.cards/util";
import { GameIds } from "@raining.cards/common";
import { useAnswerActions } from "@/action/answer.actions";
import { useClearAction } from "@/action/clear.action";
import { useRevealAction } from "@/action/reveal.action";
import { State } from "@/state";
import { Materials } from "@/materials";

export type Actions = ReturnType<typeof useActions>;

export function useActions(
  lobby: Lobby,
  transaction: Transaction,
  materials: Materials,
  zones: Zones,
  state: State
) {
  const idf = idFactory<GameIds["actionSchema"]>("actions");
  const idfGroup = idFactory<GameIds["actionSchemaGroup"]>(
    "action-schema-groups"
  );

  const clear = useClearAction(idf, lobby, transaction, zones, state);
  const reveal = useRevealAction(idf, transaction, zones, state);
  const coreActions = useCoreActions([clear, reveal]);

  return {
    next,
    of: onceBy(of),
  };

  async function next() {
    const action = await transaction.pushUpdate();
    await coreActions.perform(action);
  }

  function of(player: Player) {
    const otherPlayers = withoutUnique(lobby.participants.players, player);

    const answer = useAnswerActions(
      idf,
      player,
      otherPlayers,
      transaction,
      materials,
      zones
    );
    coreActions.register(answer);

    const answerGroup = defineActionSchemaGroup(idfGroup("answer"), answer);
    const clearGroup = defineActionSchemaGroup(idfGroup("clear"), [clear]);
    const revealGroup = defineActionSchemaGroup(idfGroup("reveal"), [reveal]);

    return {
      answer: answerGroup,
      clear: clearGroup,
      reveal: revealGroup,

      groups: [answerGroup, clearGroup, revealGroup],
      asList: [clear, reveal, ...answer],
    };
  }
}
