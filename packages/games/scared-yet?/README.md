[Base Repository](https://gitlab.com/xoria/raining-cards/)

# A Raining Cards Game: Scared yet?

## Scared yet?

- Players: 2 - 4
- Drop-out: Two unsuccessful dungeons
- Winner: Two successful dungeons / Last player in game
- Related games:
  [Welcome to the Dungeon](https://iellousa.com/products/welcome-to-the-dungeon)
  ([BGG](https://boardgamegeek.com/boardgame/150312))

In each turn a player can either add a drawn monster to the dungeon, weaken the
hero by removing some equipment, or fold.
The last player that did not fold must send the hero through the dungeon,
beating all the present monsters; one after the other.

A monster can be beaten by the specialties of the heroes' equipment. Otherwise,
the hero loses the monsters' strength in life points to defeat the monster.

If some player scores his second successful dungeon, they win the game.
If some player scores his second unsuccessful dungeon, they drop out of the
game. If all but one player are dropped out, the last remaining player wins the
game.

Monsters:

- Dragon (Strength: 9)
- Demon (Strength: 7)
- Ghost (Strength: 6)
- 2x Golem (Strength: 5)
- 2x Vampire (Strength: 4)
- 2x Wolf (Strength: 3)
- 2x Skeleton (Strength: 2)
- 2x Minion (Strength: 1)
