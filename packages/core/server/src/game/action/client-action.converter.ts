import { ClientAction, ClientActor } from "@/game/action/client-action.types";
import {
  AnyGameIds,
  ClientActionDTO,
  ClientActorDTO,
  ElementType,
  GameIds,
  MaterialPositionDescriptorDTO,
} from "@raining.cards/common";
import { Player } from "@/lobby/participant.types";
import { ClientContext } from "@/lobby/participant-context";
import { MaterialPositionDescriptor } from "@/game/material/material.types";
import { assert } from "ts-essentials";
import { GameBoard } from "@/game/game.types";

export function clientActionFromDTO(
  dto: ClientActionDTO<GameIds>,
  { client, lobby: { game, participants } }: ClientContext<GameIds>
): ClientAction {
  assert(game != null, "No active game.");
  const isPlayer = participants.players.includes(client);
  const board =
    typeof game.board === "function"
      ? game.board(client, isPlayer)
      : game.board;
  const meta =
    typeof game.meta === "function" ? game.meta(client, isPlayer) : game.meta;
  const target = clientActorFromDTO(dto.target, board);
  assert(target.type !== ElementType.MATERIALS, "Invalid target type.");
  return {
    schema: meta.actionSchemas.asLookup[dto.schema],
    player: client as Player,
    origin: clientActorFromDTO(dto.origin, board),
    target,
  };
}

function clientActorFromDTO<GI extends AnyGameIds>(
  dto: ClientActorDTO<GI>,
  board: GameBoard<GI>
): ClientActor<GI> {
  switch (dto.type) {
    case ElementType.GLOBAL:
      return dto;
    case ElementType.ZONE:
      const zone = board.asLookup[dto.zone];
      assert(zone != null);
      return { type: ElementType.ZONE, zone };
    case ElementType.MATERIAL:
      return {
        type: ElementType.MATERIAL,
        ...materialPositionDescriptorFromDTO(dto, board),
      };
    case ElementType.MATERIALS:
      return {
        type: ElementType.MATERIALS,
        materials: dto.materials.map((it) =>
          materialPositionDescriptorFromDTO(it, board)
        ),
      };
  }
}

function materialPositionDescriptorFromDTO<GI extends AnyGameIds>(
  dto: MaterialPositionDescriptorDTO<GI>,
  board: GameBoard<GI>
): MaterialPositionDescriptor<GI> {
  const zone = board.asLookup[dto.zone];
  assert(zone != null);
  return {
    index: dto.index | 0,
    zone,
  };
}
