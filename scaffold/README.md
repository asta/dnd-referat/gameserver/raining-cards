# Raining.Cards Scaffold

This project serves as a starting point to host your own collection of
_raining.cards_ games.

## Status Quo

The current web code mainly consists of routing and stylesheets. The heavy
lifting is done within `@raining.cards/client`.

However, the lobby and game UI are going to be separated from the scaffold code
-- in order to make the scaffold as simple as possible and avoid any manual
maintenance. Thus, be prepared for major changes, if you decide to use this
early version.

The vision is to set up for different lobby and game UIs depending on the game.
The current generic UI is going to become the fallback option for games without
custom UI then.

## Setup

### Prepare Scaffold

Unfortunately, the scaffold is not yet polished to be split from the repository
without the danger of missing crucial updates. The generic UI is currently
hard-wired with the scaffold...

So the best option may be to just fork this repository and ignore everything
except the scaffold directory.

Clone the repository. For the purpose of this documentation, we call the project
root _/opt/raining-cards/_.

### Server

1. Go into the _server/_ directory.
1. Run `npm i` to install the dependencies.
1. Copy the _.env_ file into _.env.production.local_ and modify as needed.
1. Run `npm run start` to start the server.
1. The server is now running (listening on port 7582 by default).
1. Use your favourite tools to make this process run on system boot, restart on
   exit, etc.

### Web Frontend

1. Go into the _web/_ directory.
1. Copy the _.env_ file into _.env.production.local_ and modify as needed (e.g.
   `VITE_API_URL=https://rc.example.com/`).

### Nginx configuration

To wire everything together, create a nginx file following the example below.
Replace `rc.example.com` with your domain.

```nginx
server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;
    server_name rc.example.com;

    # TLS certificate
    ssl_certificate /etc/letsencrypt/live/raining-cards.example.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/raining-cards.example.com/privkey.pem;

    # Content rules
    client_max_body_size 2M;
    gzip on;
    gzip_vary on;
    gzip_comp_level 5;
    gzip_min_length 256;
    gzip_types
        application/javascript
        application/x-javascript
        application/json
        application/rss+xml
        application/xml
        image/svg+xml
        image/x-icon
        application/vnd.ms-fontobject
        application/font-sfnt
        text/css
        text/plain;
    gzip_http_version 1.1;

    # Reverse proxy on /api
    location /api {
        rewrite /api/(.*) /$1  break;
        proxy_pass http://127.0.0.1:7582;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;

        # WebSocket support
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
    }

    # Static file delivery for web frontend
    location / {
        root /opt/raining-cards/scaffold/web/dist/;
        try_files $uri /index.html;
    }
}
```

## Maintenance

To warn users about upcoming maintenance or other kinds of downtime, create some
[notifications](https://gitlab.com/xoria/raining-cards/-/tree/master/scaffold/server/README_NOTIFICATIONS.md)
in advance.

You may also check the _https://rc.example.com/api/stats.json_ URL (adapted to
match your domain), to check any activity on your server.

## License

This project is published under the GNU Affero General Public License Version 3.
