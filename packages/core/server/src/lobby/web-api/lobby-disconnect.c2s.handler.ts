import { ClientContext } from "@/lobby/participant-context";
import { checkLobbyCleanup } from "@/lobby/Lobby";
import * as WebSocket from "ws";
import { AnyGameIds } from "@raining.cards/common";

export function c2sLobbyDisconnectHandler<GI extends AnyGameIds>(
  socket: WebSocket,
  ctx: ClientContext<GI>
) {
  checkLobbyCleanup(socket, ctx.lobby);
}
