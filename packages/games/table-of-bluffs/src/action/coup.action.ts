import { ToBGame } from "@/ToB.game";
import { ToBActionCondition } from "@/condition";
import { getLives, PlayerZonesMap, ToBZones } from "@/zone/zones";
import { ToBActionSchemaGroup, ToBActionSchema } from "@/action/action.types";
import { ToBGameIds } from "@/ToB.types";
import { assert } from "ts-essentials";
import {
  ActionSchema,
  ClientAction,
  ClientActorZone,
  last,
  Player,
} from "@raining.cards/server";
import { ElementType } from "@raining.cards/common";
import { matchAll, matchIt, matchSome, times } from "@raining.cards/util";
import { ToBZoneType } from "@/zone/zone-types";
import { CREDIT_SYMBOL } from "@/material/face-types";

export function coupActionSchema(
  game: ToBGame,
  { byPlayer }: PlayerZonesMap,
  _: ToBZones,
  otherPlayers: Player[]
): ActionSchema<ToBGameIds>[] {
  return otherPlayers.map((player) => {
    const id = `${ToBActionSchema.COUP}|${player.id}`;
    game.actionSchemaMapping[id] = ToBActionSchema.COUP;
    return {
      id,
      name: "Coup",
      description: `Costs: ${CREDIT_SYMBOL}7; cannot be blocked.<br/>Force some player to forfeit a life. This action must is the only option, if you own more than ${CREDIT_SYMBOL}9 at the start of your turn.`,
      origin: {
        type: ElementType.ZONE,
        zone: [byPlayer.get(player)![ToBZoneType.PLAYER_ROLES].id],
      },
      target: { type: ElementType.GLOBAL },
      condition: matchAll([
        matchIt(ToBActionCondition.CREDITS_MIN_7),
        matchIt(`alive:${player.id}`),
        matchSome([
          matchIt(ToBActionCondition.EQUAL_TEAMS),
          matchIt(`enemy:${player.id}`),
        ]),
      ]),
    };
  });
}

export async function performCoup(
  game: ToBGame,
  action: ClientAction<ToBGameIds>
) {
  const playerZone = game.zones.byPlayer.get(action.player)!;
  const position = last(playerZone[ToBZoneType.PLAYER_CREDITS]);
  times(7, () => game.transaction.destroy(position));
  const origin = action.origin as ClientActorZone<ToBGameIds>;
  const targetPlayer = game.playerByZone.get(origin.zone)!;
  game.logs.add(
    `${action.player.name} coups against ${targetPlayer.name} for ${CREDIT_SYMBOL}7`
  );
  const rolesZone = game.zones.byPlayer.get(targetPlayer)![
    ToBZoneType.PLAYER_ROLES
  ];
  const lives = getLives(rolesZone);
  if (lives.length > 1) {
    const actionStackItem = game.actionStack.push(() => {
      game.transaction.addPlayerAction(
        targetPlayer,
        ToBActionSchemaGroup.RECEIVE_COUP
      );
    });
    await game.performTask();
    game.actionStack.popAssert(actionStackItem);
  } else {
    assert(lives.length === 1, "Invalid state.");
    const pos = lives[0];
    game.transaction.revealGlobal(pos);
    game.logs.add(
      `${targetPlayer.name} forfeits a ${pos.material.faces[1].typeId} and is defeated`
    );
    game.transaction.playerDied(targetPlayer);
  }
  game.turn.step();
}
