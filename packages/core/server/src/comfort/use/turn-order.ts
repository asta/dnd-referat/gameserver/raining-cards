import { InjectionKey, RingPointer } from "@raining.cards/util";
import { Player } from "@/lobby/participant.types";

export const TURN_ORDER: InjectionKey<TurnOrder> = { name: "turn" };

export type TurnOrder = ReturnType<typeof useTurnOrder>;

export function useTurnOrder(players: Player[]) {
  const rp = new RingPointer(players);

  return {
    get current(): Player {
      return rp.value;
    },
    set current(item: Player) {
      rp.pointTo(item);
    },

    get size() {
      return rp.size;
    },

    get players() {
      return rp.items;
    },

    next: rp.step.bind(rp),
    remove: rp.remove.bind(rp),
    reset: rp.reset.bind(rp),
  };
}
