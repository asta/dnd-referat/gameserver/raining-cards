import { IdFactory } from "@/util";
import { MaterialFace, MaterialFaceType } from "@/game/material/material.types";
import { AnyGameIds, GameIds } from "@raining.cards/common";

export interface DefinedMaterialFace<GI extends AnyGameIds = GameIds>
  extends MaterialFace<GI> {
  type: MaterialFaceType<GI>;
  toJSON: () => MaterialFace<GI>;
}

export interface MaterialFaceDefinition<GI extends AnyGameIds = GameIds> {
  type: MaterialFaceType<GI>;
  factory: (meta?: MaterialFace<GI>["customMeta"]) => DefinedMaterialFace<GI>;
}

export interface MaterialFaceSingletonDefinition<
  GI extends AnyGameIds = GameIds
> extends MaterialFaceDefinition<GI> {
  instance: MaterialFace<GI>;
}

export function defineMaterialFace<GI extends AnyGameIds = GameIds>(
  idf: GI["materialFaceType"] | IdFactory<GI["materialFaceType"]>,
  typeData: Omit<MaterialFaceType<GI>, "id">
): MaterialFaceDefinition<GI> {
  const id = typeof idf === "function" ? idf() : idf;
  const type: MaterialFaceType<GI> = { id, ...typeData };
  return {
    type,
    factory: (customMeta) => ({
      typeId: type.id,
      type,
      customMeta,
      toJSON: faceToJSON,
    }),
  };
}

export function defineMaterialFaceSingleton<GI extends AnyGameIds = GameIds>(
  idf: GI["materialFaceType"] | IdFactory<GI["materialFaceType"]>,
  typeData: Omit<MaterialFaceType<GI>, "id">
): MaterialFaceSingletonDefinition<GI> {
  const def = defineMaterialFace(idf, typeData);
  const instance = def.factory();
  return {
    ...def,
    factory: () => instance,
    instance,
  };
}

function faceToJSON<GI extends AnyGameIds>(
  this: DefinedMaterialFace<GI>
): MaterialFace<GI> {
  return { typeId: this.typeId, customMeta: this.customMeta };
}
