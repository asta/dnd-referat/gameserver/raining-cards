import {
  ActionConditionDTO,
  ActionConditionItemDTO,
  AnyGameIds,
} from "@raining.cards/common";
import { matchIt } from "@raining.cards/util";

export type ActionCondition<GI extends AnyGameIds> = ActionConditionDTO<GI>;
export type ActionConditionItem<
  GI extends AnyGameIds
> = ActionConditionItemDTO<GI>;

export function actionConditionToDTO<GI extends AnyGameIds>(
  it: ActionCondition<GI>
): ActionConditionDTO<GI> {
  return it;
}

/**
 * Helper function to ensure correct types.
 *
 * @param it The condition item.
 */
export function matchCondition<GI extends AnyGameIds>(
  it: ActionConditionItem<GI>
) {
  return matchIt<ActionConditionItem<GI>>(it);
}
