import { ZoneWithMaterialChunks } from "@/game/game.dto";
import {
  AnyGameIds,
  GameResultGroupDTO,
  MaterialUpdateDelta,
} from "@raining.cards/common";

export enum PushUpdateType {
  KEYFRAME = "keyframe",
  DELTA = "delta",
  CLOSURE = "closure",
}

export interface PushUpdateKeyframe<GI extends AnyGameIds> {
  type: PushUpdateType.KEYFRAME;
  state: ZoneWithMaterialChunks<GI>[];
}

export interface PushUpdateDelta<GI extends AnyGameIds> {
  type: PushUpdateType.DELTA;
  actionSchemaGroups?: GI["actionSchemaGroup"][];
  steps: MaterialUpdateDelta<GI>[];
}

export interface PushUpdateClosureDTO {
  type: PushUpdateType.CLOSURE;
  endTime: Date;
  groups: GameResultGroupDTO[];
}
