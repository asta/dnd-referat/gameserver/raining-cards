import { AnyGameIds, ServerCommand } from "@raining.cards/common";
import { Game } from "@/game/game.types";
import { broadcast } from "@/lobby/communication";
import { accessLobby, Lobby } from "@/lobby/Lobby";

export function pushLobbyParticipants<GI extends AnyGameIds, GameProps>(
  lobby: Lobby<GI, GameProps>
) {
  broadcast(lobby.participants, { cmd: ServerCommand.LOBBY_TEAMS, lobby });
}

/**
 * Pushes a participants update to all clients. This can be used for example to
 * alter uiMeta of some client.
 *
 * @param game The game to push participant update for.
 */
export function pushParticipants<GI extends AnyGameIds>(game: Game<GI>) {
  pushLobbyParticipants(accessLobby(game));
}
