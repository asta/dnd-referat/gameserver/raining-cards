import { ToBGame } from "@/ToB.game";
import {
  backwardSteal,
  forwardSteal,
  LATEST_STEAL,
} from "@/action/steal.action";
import { ToBActionSchemaGroup, ToBActionSchema } from "@/action/action.types";
import { ToBGameIds } from "@/ToB.types";
import { ActionSchema, ClientAction } from "@raining.cards/server";
import { ElementType } from "@raining.cards/common";
import { doubtActionStackItem } from "@/action/doubt.action";

export function stealBlockSchema(): ActionSchema<ToBGameIds>[] {
  return [
    {
      id: ToBActionSchema.BLOCK_STEAL_CAPTAIN,
      name: "Block theft (Captain)",
      description: `Role: <em>Captain</em>.<br/>Blocks some theft.`,
      origin: { type: ElementType.GLOBAL },
      target: { type: ElementType.GLOBAL },
    },
    {
      id: ToBActionSchema.BLOCK_STEAL_AMBASSADOR,
      name: "Block theft (Ambassador)",
      description: `Role: <em>Ambassador</em>.<br/>Blocks some theft.`,
      origin: { type: ElementType.GLOBAL },
      target: { type: ElementType.GLOBAL },
    },
  ];
}

export async function performStealBlock(
  game: ToBGame,
  action: ClientAction<ToBGameIds>
) {
  game.logs.add(
    `${action.player.name} blocks the theft (${
      action.schema.id === ToBActionSchema.BLOCK_STEAL_CAPTAIN
        ? "captain"
        : "ambassador"
    })`
  );
  backwardSteal(game);
  const { toZone } = LATEST_STEAL.get(game)!;
  const player = game.playerByZone.get(toZone)!;
  const actionStackItem = game.actionStack.push([
    () => {
      game.transaction.addPlayerAction(player, ToBActionSchemaGroup.CONTINUE);
    },
    doubtActionStackItem(game, { action, then: () => forwardSteal(game) }),
  ]);
  await game.performTask();
  game.actionStack.popAssert(actionStackItem);
}
