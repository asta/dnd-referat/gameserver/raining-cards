import { broadcast, broadcastIndividual } from "@/lobby/communication";
import { Client, Player } from "@/lobby/participant.types";
import { Zone } from "@/game/zone/zone.types";
import { PushUpdateType } from "@/comfort/push/push-update.dto";
import { getMaterialView } from "@/game/material/material";
import { accessLobby } from "@/lobby/Lobby";
import { S2CMessageServerGameMeta } from "@/game/web-api/game-meta.s2c";
import { S2CMessageServerUpdateKeyframe } from "@/game/web-api/game-update.s2c";
import { Game, GameResultGroup } from "@/game/game.types";
import {
  AnyGameIds,
  MaterialChunkDTO,
  MaterialFaceDTO,
  MaterialViewDTO,
  ServerCommand,
} from "@raining.cards/common";
import { ZoneWithMaterialChunks } from "@/game/game.dto";
import { getLatestActionRevision } from "@/game/action/action-revision";

export function sendMetas<GI extends AnyGameIds>(game: Game<GI>) {
  broadcastIndividual(accessLobby(game).participants, (client, isPlayer) =>
    getMetaMessage(game, client, isPlayer)
  );
}

export function getMetaMessage<GI extends AnyGameIds>(
  game: Game<GI>,
  client: Client,
  isPlayer: boolean
): S2CMessageServerGameMeta<GI> {
  return {
    cmd: ServerCommand.GAME_META,
    meta:
      typeof game.meta === "function" ? game.meta(client, isPlayer) : game.meta,
  };
}

export function sendKeyframes<GI extends AnyGameIds>(game: Game<GI>) {
  broadcastIndividual(accessLobby(game).participants, (client, isPlayer) =>
    getKeyframeMessage(game, client, isPlayer)
  );
}

export function getKeyframeMessage<GI extends AnyGameIds>(
  game: Game<GI>,
  client: Client,
  isPlayer: boolean
): S2CMessageServerUpdateKeyframe<GI> {
  return {
    cmd: ServerCommand.UPDATE_KEYFRAME,
    actionRevision: getLatestActionRevision(client),
    data: {
      type: PushUpdateType.KEYFRAME,
      state: getGameState(game, client, isPlayer),
    },
  };
}

export function sendClosure<GI extends AnyGameIds>(
  game: Game<GI>,
  gameResultGroups: GameResultGroup[]
) {
  broadcast(accessLobby(game).participants, {
    cmd: ServerCommand.UPDATE_CLOSURE,
    data: {
      type: PushUpdateType.CLOSURE,
      endTime: new Date(),
      gameResultGroups,
    },
  });
}

function getGameState<GI extends AnyGameIds>(
  game: Game<GI>,
  client: Client,
  isPlayer: boolean
): ZoneWithMaterialChunks<GI>[] {
  const zoneToZoneWithMaterialChunks = (
    zone: Zone<GI>
  ): ZoneWithMaterialChunks<GI> => ({
    zone,
    children: zone.children?.map(zoneToZoneWithMaterialChunks),
    materialChunks: visibleMaterialChunks(zone, isPlayer ? client : null),
  });
  return (typeof game.board === "function"
    ? game.board(client, isPlayer)
    : game.board
  ).asTree.map(zoneToZoneWithMaterialChunks);
}

function visibleMaterialChunks<GI extends AnyGameIds>(
  zone: Zone<GI>,
  player: Player | null
): MaterialChunkDTO<GI>[] {
  const chunks: MaterialChunkDTO<GI>[] = [];
  let latestChunk: MaterialChunkDTO<GI> | null = null;
  for (const material of zone.materials) {
    const view = getMaterialView(player, material, zone);
    if (latestChunk != null && isSameView(latestChunk, view)) {
      latestChunk.count++;
    } else {
      chunks.push(
        (latestChunk = { count: 1, global: view.global, secret: view.secret })
      );
    }
  }
  return chunks;
}

function isSameView<GI extends AnyGameIds>(
  a: MaterialViewDTO<GI>,
  b: MaterialViewDTO<GI>
): boolean {
  return (
    isSameFaceView(a.global, b.global) && isSameFaceView(a.secret, b.secret)
  );
}

function isSameFaceView<GI extends AnyGameIds>(
  a?: MaterialFaceDTO<GI>,
  b?: MaterialFaceDTO<GI>
): boolean {
  return a?.typeId === b?.typeId && a?.customMeta === b?.customMeta;
}
