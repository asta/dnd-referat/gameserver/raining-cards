import {
  ActionSchema,
  ActionSchemaGroup,
} from "@/game/action/action-schema.types";
import { IdFactory } from "@/util";
import { LooseDefinedAction } from "@/comfort";
import { ClientAction } from "@/game/action/client-action.types";
import { AnyGameIds, GameIds } from "@raining.cards/common";

export interface DefinedActionSchemaGroup<GI extends AnyGameIds = GameIds>
  extends ActionSchemaGroup<GI> {
  schemas: ActionSchema<GI>[];
  toJSON: () => ActionSchemaGroup<GI>;
}

export function defineActionSchemaGroup<
  GI extends AnyGameIds = GameIds,
  CA extends ClientAction<GI> = ClientAction<GI>
>(
  idf: GI["actionSchemaGroup"] | IdFactory<GI["actionSchemaGroup"]>,
  actions: LooseDefinedAction<GI, CA>[]
): DefinedActionSchemaGroup<GI> {
  const id = typeof idf === "function" ? idf() : idf;
  return {
    id,
    schemaIds: actions.map((it) => it.schema.id),
    schemas: actions.map((it) => it.schema),
    toJSON: groupToJSON,
  };
}

function groupToJSON<GI extends AnyGameIds>(
  this: DefinedActionSchemaGroup<GI>
): ActionSchemaGroup<GI> {
  return { id: this.id, schemaIds: this.schemaIds };
}
