import { EquipmentMaterials } from "@/material/equipment-materials";
import {
  defineMaterialFaceSingleton,
  defineMaterialSingleton,
  IdFactory,
  MaterialFaceSingletonDefinition,
  MaterialFaceType,
  MaterialSingletonDefinition,
} from "@raining.cards/server";
import { EquipmentDefinition } from "@/material/equipment-factories";
import { GameIds } from "@raining.cards/common";
import { omit } from "@raining.cards/util";

export enum Hero {
  ROGUE = "rogue",
  BERSERK = "berserk",
  PALADIN = "paladin",
}

export const HEROES = Object.values(Hero);

export interface HeroData {
  name: string;
  face: MaterialFaceSingletonDefinition;
  material: MaterialSingletonDefinition;
  baseHP: number;
  equipment: EquipmentDefinition[];
}

export type HeroMaterials = ReturnType<typeof useHeroMaterials>;

interface UseHeroData extends Omit<MaterialFaceType, "id"> {
  name: string;
  hp: number;
  equipment: EquipmentDefinition[];
}

export function useHeroMaterials(
  idfFace: IdFactory<GameIds["materialFaceType"]>,
  idfMaterial: IdFactory<GameIds["material"]>,
  equipmentMaterials: EquipmentMaterials
): Record<Hero, HeroData> {
  return {
    [Hero.ROGUE]: useHero(Hero.ROGUE, {
      name: "Rogue",
      hp: 3,
      equipment: [
        equipmentMaterials.shield,
        equipmentMaterials.armor,
        equipmentMaterials.talisman,
        equipmentMaterials.stinkBomb,
        equipmentMaterials.cloak,
        equipmentMaterials.healthPotion,
      ],
      label: "Rogue: 3HP",
      description: "The rogue hero. Starts with 3 base health points.",
    }),

    [Hero.BERSERK]: useHero(Hero.BERSERK, {
      name: "Berserk",
      hp: 4,
      equipment: [
        equipmentMaterials.armGuard,
        equipmentMaterials.loincloth,
        equipmentMaterials.axe,
        equipmentMaterials.stinkBomb,
        equipmentMaterials.hammer,
        equipmentMaterials.ragePotion,
      ],
      label: "Berserk: 4HP",
      description: "The berserk hero. Starts with 4 base health points.",
    }),

    [Hero.PALADIN]: useHero(Hero.PALADIN, {
      name: "Paladin",
      hp: 3,
      equipment: [
        equipmentMaterials.shield,
        equipmentMaterials.armor,
        equipmentMaterials.sword,
        equipmentMaterials.ban,
        equipmentMaterials.deadBeGone,
        equipmentMaterials.lance,
      ],
      label: "Paladin: 3HP",
      description: "The paladin hero. Starts with 3 base health points.",
    }),
  };

  function useHero(id: string, data: UseHeroData): HeroData {
    const face = defineMaterialFaceSingleton(
      idfFace(id),
      omit(data, ["hp", "equipment"])
    );
    const material = defineMaterialSingleton(idfMaterial(id), face);
    return {
      face,
      material,
      name: data.name,
      baseHP: data.hp,
      equipment: data.equipment,
    };
  }
}
