[Base Repository](https://gitlab.com/xoria/raining-cards/)

# Raining Cards: Client

This package provides an API to manage the network layer for a client
communicating with a _raining.cards_ server instance. In addition, the lobby and
game state is managed.

Documentation outstanding, sorry!
