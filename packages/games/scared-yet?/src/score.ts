import {
  GameLogs,
  GameResultGroup,
  Player,
  TurnOrder,
} from "@raining.cards/server";
import { fromEntries, pickOne } from "@raining.cards/util";

export type Score = ReturnType<typeof useScore>;

export function useScore(
  players: Player[],
  turnOrder: TurnOrder,
  gameLogs: GameLogs
) {
  const dungeonsLost = fromEntries(players.map((it) => [it.id, 0]));
  const dungeonsWon = { ...dungeonsLost };

  return {
    addDungeonsLost,
    addDungeonsWon,

    isConcluded,
    closure,
  };

  function addDungeonsLost(player: Player) {
    dungeonsLost[player.id]++;
    if (dungeonsLost[player.id] === 2) {
      turnOrder.remove(player);
      turnOrder.current = pickOne(turnOrder.players);
      gameLogs.add(
        `${player.name} has lost two heroes and withdraws from the game.`
      );
    } else {
      gameLogs.add(`${player.name} gets one step closer to defeat.`);
    }
  }

  function addDungeonsWon(player: Player) {
    dungeonsWon[player.id]++;
    if (dungeonsWon[player.id] === 2) {
      turnOrder.reset([player]);
      gameLogs.add(
        `${player.name} has finished two dungeons and wins the game.`
      );
    } else {
      gameLogs.add(`${player.name} gets one step closer to victory.`);
    }
  }

  function isConcluded(): boolean {
    return turnOrder.size === 1;
  }

  function closure(): GameResultGroup[] {
    const winner = turnOrder.current;
    return [
      {
        name: "Winner",
        stats: [{ player: winner, text: playerScoreText(winner) }],
      },
      {
        name: "Maybe next time",
        stats: players
          .filter((player) => player !== winner)
          .map((player) => ({ player, text: playerScoreText(player) })),
      },
    ];
  }

  function playerScoreText(player: Player): string {
    return `${dungeonsWon[player.id]} dungeon(s) won, ${
      dungeonsLost[player.id]
    } dungeon(s) lost.`;
  }
}
