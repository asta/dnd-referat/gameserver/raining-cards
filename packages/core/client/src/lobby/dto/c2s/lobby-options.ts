import { C2SMessageDTOLobbyOptions } from "@raining.cards/common";

export interface C2SMessageClientLobbyOptions {
  data: Record<string, unknown>;
}

export function c2sLobbyOptionsToDTO({
  data,
}: C2SMessageClientLobbyOptions): string {
  const payload: C2SMessageDTOLobbyOptions = data;
  return JSON.stringify(payload);
}
