import { Player } from "@/lobby/participant.types";
import { Zone } from "@/game/zone/zone.types";
import { MaterialPositionDescriptor } from "@/game/material/material.types";
import {
  AnyGameIds,
  ClientActorGlobalDTO,
  ElementType,
  GameIds,
} from "@raining.cards/common";
import { ActionSchema } from "@/game/action/action-schema.types";

export interface ClientAction<
  GI extends AnyGameIds = GameIds,
  Origin extends ClientActor<GI> = ClientActor<GI>,
  Target extends ClientActorSingle<GI> = ClientActorSingle<GI>
> {
  schema: ActionSchema<GI>;
  player: Player;
  origin: Origin;
  target: Target;
}

export type ClientActorSingle<GI extends AnyGameIds = GameIds> =
  | ClientActorGlobal
  | ClientActorZone<GI>
  | ClientActorMaterial<GI>;

export type ClientActor<GI extends AnyGameIds = GameIds> =
  | ClientActorSingle<GI>
  | ClientActorMaterials<GI>;

// meta type, not used for runtime code
export interface ClientActorByElementType<GI extends AnyGameIds = GameIds> {
  [ElementType.GLOBAL]: ClientActorGlobal;
  [ElementType.ZONE]: ClientActorZone<GI>;
  [ElementType.MATERIAL]: ClientActorMaterial<GI>;
  [ElementType.MATERIALS]: ClientActorMaterials<GI>;
}

export type ClientActorGlobal = ClientActorGlobalDTO;

export interface ClientActorZone<GI extends AnyGameIds = GameIds> {
  type: ElementType.ZONE;
  zone: Zone<GI>;
}

export interface ClientActorMaterial<GI extends AnyGameIds = GameIds>
  extends MaterialPositionDescriptor<GI> {
  type: ElementType.MATERIAL;
}

export interface ClientActorMaterials<GI extends AnyGameIds = GameIds> {
  type: ElementType.MATERIALS;
  materials: MaterialPositionDescriptor<GI>[];
}
