import {
  ActionRevisionDTO,
  AnyGameIds,
  ELEMENT_TYPES,
} from "@raining.cards/common";
import { EMPTY_SET, listToObject } from "@raining.cards/util";
import {
  UIActionSchemaLookup,
  UIActionRevision,
  UIGameMeta,
} from "@/game/game.types";
import { UIActionSchema } from "@/game/action-schema.types";

export function actionRevisionFromDTO<GI extends AnyGameIds>(
  meta: UIGameMeta<GI>,
  dto: ActionRevisionDTO<GI> | null
): UIActionRevision<GI> {
  if (dto === null) {
    return {
      revision: -1,
      schemas: EMPTY_SET as Set<UIActionSchema<GI>>,
      ...createSchemaLookup<GI>(),
    };
  }
  const schemas = new Set<UIActionSchema<GI>>(
    dto.groups.flatMap((id) => meta.actionSchemaGroups[id])
  );
  const actionSchemaLookup = createSchemaLookup<GI>();
  for (const action of schemas) {
    const list = actionSchemaLookup[action.origin.type][
      action.target.type
    ] as UIActionSchema<
      GI,
      typeof action["origin"]["type"],
      typeof action["target"]["type"]
    >[];
    list.push(action);
  }
  return {
    revision: dto.revision,
    schemas: schemas,
    ...actionSchemaLookup,
  };
}

function createSchemaLookup<GI extends AnyGameIds>(): UIActionSchemaLookup<GI> {
  return listToObject(ELEMENT_TYPES, (origin) => [
    origin,
    listToObject(ELEMENT_TYPES, (target) => [target, []]),
  ]);
}
