import {
  commonPkgMeta,
  defineGameBox,
  Game,
  pushLobbyParticipants,
  useGameLogs,
  useTransaction,
  useTurnOrder,
} from "@raining.cards/server";
import * as pkg from "../package.json";
import { useGameMeta } from "@/game-meta";
import { useGame } from "@/game";
import { useMaterialFaces } from "@/material/material-faces";
import { useMaterials } from "@/material/material";
import { useScore } from "@/state/score";
import { useHighestInfo } from "@/state/highest-info";
import { useMaterialFaceGroups } from "@/material/material-face-groups";
import { useZones } from "@/zones";
import { useDealer } from "@/dealer";
import { useActions } from "@/action/actions";
import { useActionConditions } from "@/action/action-conditions";
import { useActionSchemaGroups } from "@/action/action-schema-groups";
import { shuffle } from "@raining.cards/util";

const description = `
Related game:
<ul>
<li><a href="https://www.amigo-spiele.de/spiel/druids">Druids</a> (<a href="https://boardgamegeek.com/boardgame/232417/druids">BGG</a>)</li>
</ul>

<p>A trick-taking game.</p>
Cards:
<ul>
<li>5 different colors with values ranging from 1 to 12 each</li>
<li>2x rain</li>
<li>2x hedge clippers</li>
</ul>
<p>Whoever gets five different colors first, loses the round (-3 points).
All other players get the sum of their collected high-cards in points.
After 5 rounds, the player with most points wins the game.</p>
<p>While the rain simply does nothing, the hedge clippers remove the highest
color of the receiving players score zone.</p>`;

export const GARDENERS_BOX = defineGameBox(
  "gardeners",
  { players: { min: 3, max: 5 } },
  { name: "Gardeners", description, meta: commonPkgMeta(pkg) },
  (lobby): Game => {
    shuffle(lobby.participants.players);
    pushLobbyParticipants(lobby);
    const transaction = useTransaction(lobby.participants);
    const zones = useZones(lobby.participants.players);
    const turnOrder = useTurnOrder(lobby.participants.players);
    const gameLogs = useGameLogs(lobby.participants);
    const score = useScore(lobby.participants.players, turnOrder);
    const highestInfo = useHighestInfo();

    const materialFaces = useMaterialFaces();
    const materialFaceGroups = useMaterialFaceGroups(materialFaces);
    const materials = useMaterials(materialFaces);

    const dealer = useDealer(transaction, materials, zones, turnOrder);

    const actionConditions = useActionConditions(
      materialFaces,
      materialFaceGroups,
      zones
    );
    const actions = useActions(
      transaction,
      actionConditions,
      dealer,
      materials,
      materialFaceGroups,
      zones,
      gameLogs,
      highestInfo
    );
    const actionSchemaGroups = useActionSchemaGroups(actions);

    const gameMeta = useGameMeta(
      materialFaces,
      materialFaceGroups,
      zones,
      actions,
      actionConditions,
      actionSchemaGroups
    );

    return useGame(
      lobby.participants,
      transaction,
      gameMeta,
      dealer,
      score,
      gameLogs,
      turnOrder,
      actionSchemaGroups,
      highestInfo,
      zones,
      materials,
      actions
    );
  }
);
