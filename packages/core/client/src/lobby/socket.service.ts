import { Emitter, withEmitter } from "@raining.cards/util";
import {
  uiCreateLobby,
  uiJoinLobby,
  UILobbyCreateOptions,
  UILobbyJoinOptions,
  UILobbyReconnectOptions,
  UILobbyState,
  uiReconnectToLobby,
} from "@/lobby/lobby.service";
import {
  AnyGameIds,
  ClientCommand,
  GameIds,
  ServerCommand,
  SocketMessageLowLevelDTO,
} from "@raining.cards/common";
import { UIGameBoxesState } from "@/game-boxes/game-boxes.service";
import { debugClone } from "@/util/debug";

export type SocketMessageLowLevel = { cmd: ServerCommand; payload?: unknown };
export type UISocketEventMap = Record<ServerCommand, SocketMessageLowLevel>;

export interface UISocketState<GI extends AnyGameIds = GameIds>
  extends Emitter<UISocketEventMap> {
  socket: WebSocket;

  send: (cmd: ClientCommand, payload?: SocketMessageLowLevelDTO) => void;
  createLobby: (options: UILobbyCreateOptions) => Promise<UILobbyState<GI>>;
  joinLobby: (options: UILobbyJoinOptions) => Promise<UILobbyState<GI>>;
  reconnectLobby: (
    options: UILobbyReconnectOptions
  ) => Promise<UILobbyState<GI>>;
  destroy: () => void;
}

export async function uiConnectWebSocket<GI extends AnyGameIds>(
  gameBoxesState: UIGameBoxesState<GI>,
  wsUrl: string
): Promise<UISocketState<GI>> {
  const socket = new WebSocket(wsUrl);
  const state = withEmitter<UISocketState<GI>, UISocketEventMap>({
    socket,

    send: send.bind(null, socket) as UISocketState<GI>["send"],

    createLobby,
    joinLobby,
    reconnectLobby,
    destroy,
  });

  socket.addEventListener("error", console.error);
  socket.addEventListener("message", ({ data: msg }) => {
    const cmd: ServerCommand = msg.substr(0, 2) as ServerCommand;
    const payload = msg.length > 3 ? JSON.parse(msg.substr(3)) : undefined;
    const debugPayload = debugClone(payload);
    console.debug("socket:recv", cmd, debugPayload);
    if (cmd === ServerCommand.NOTIFICATION) {
      // dirty small delay due to event listeners being attached too late; this
      //       should be fixed by moving notification and error listeners from
      //       lobby to socket level
      setTimeout(next, 75);
    } else {
      next();
    }

    function next() {
      if (!state.emit(cmd, { cmd, payload })) {
        console.warn("message ignored", cmd, payload);
      }
    }
  });

  await new Promise<void>((resolve) => {
    socket.addEventListener("open", listener);

    function listener() {
      socket.removeEventListener("open", listener);
      resolve();
    }
  });

  let currentLobby: UILobbyState<GI> | null = null;

  return state;

  async function createLobby(
    options: UILobbyCreateOptions
  ): Promise<UILobbyState<GI>> {
    return (currentLobby = await uiCreateLobby(gameBoxesState, state, options));
  }

  async function joinLobby(
    options: UILobbyJoinOptions
  ): Promise<UILobbyState<GI>> {
    return (currentLobby = await uiJoinLobby(gameBoxesState, state, options));
  }

  async function reconnectLobby(
    options: UILobbyReconnectOptions
  ): Promise<UILobbyState<GI>> {
    return (currentLobby = await uiReconnectToLobby(
      gameBoxesState,
      state,
      options
    ));
  }

  function destroy() {
    socket.close();
    currentLobby?.destroy();
    currentLobby = null;
  }
}

function send(
  socket: WebSocket,
  cmd: ClientCommand,
  payload?: SocketMessageLowLevelDTO
) {
  console.debug("socket:send", cmd, payload);
  socket.send(payload === undefined ? cmd : `${cmd}:${payload}`);
}
