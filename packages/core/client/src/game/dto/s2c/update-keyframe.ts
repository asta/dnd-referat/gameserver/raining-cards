import {
  UIGame,
  UIGameMeta,
  UIMaterialFaceType,
  UIParticipants,
} from "@/game/game.types";
import { AnyGameIds, MaterialChunkDTO, ZoneDTO } from "@raining.cards/common";
import { materialViewFromDTO, UIMaterialChunk } from "@/game/material";
import { UIZone, UIZoneEventMap } from "@/area/zone.types";
import { increaseMapValue, mapValues, withEmitter } from "@raining.cards/util";
import { debugClone } from "@/util/debug";
import { zoneTypeMetaFromDTO } from "@/game/dto/s2c/game-meta";

export function zoneFromDTO<GI extends AnyGameIds>(
  participants: UIParticipants,
  meta: UIGameMeta<GI>,
  dto: ZoneDTO<GI>,
  _exec = (it: ZoneDTO<GI>) => zoneFromDTO(participants, meta, it)
): UIZone<GI> {
  const materialChunks = materialChunksFromDTOS(meta, dto.materialChunks ?? []);
  const zone = withEmitter<UIZone<GI>, UIZoneEventMap<GI>>({
    id: dto.id,
    type: meta.zoneTypes[dto.typeId],
    customMeta: dto.customMeta
      ? zoneTypeMetaFromDTO(participants, dto.customMeta)
      : undefined,
    children: dto.children?.map(_exec),
    materialChunks,
    ...countMaterialFaceTypes(materialChunks),
  });
  console.debug("zone:updated", debugClone(zone));
  return zone;
}

export function zonesToLookupMaps<GI extends AnyGameIds>(
  zones: UIZone<GI>[],
  zoneTypes: UIGameMeta<GI>["zoneTypes"],
  targetAsMap = {} as Record<GI["zone"], UIZone<GI>>,
  targetByType = mapValues(zoneTypes, (): UIZone<GI>[] => [])
): Pick<UIGame<GI>["zones"], "asMap" | "byType"> {
  for (const zone of zones) {
    targetAsMap[zone.id] = zone;
    targetByType[zone.type.id].push(zone);
    if (zone.children !== undefined) {
      zonesToLookupMaps(zone.children, zoneTypes, targetAsMap, targetByType);
    }
  }
  return { asMap: targetAsMap, byType: targetByType };
}

function materialChunksFromDTOS<GI extends AnyGameIds>(
  meta: UIGameMeta<GI>,
  dtos: MaterialChunkDTO<GI>[]
): UIMaterialChunk<GI>[] {
  let offset = 0;
  const result: UIMaterialChunk<GI>[] = Array(dtos.length);
  for (let i = 0; i < dtos.length; i++) {
    const chunk = materialChunkFromDTO(meta, offset, dtos[i]);
    result[i] = chunk;
    offset += chunk.count;
  }
  return result;
}

function materialChunkFromDTO<GI extends AnyGameIds>(
  meta: UIGameMeta<GI>,
  offset: number,
  dto: MaterialChunkDTO<GI>
): UIMaterialChunk<GI> {
  return {
    count: dto.count,
    offset,
    ...materialViewFromDTO(meta, dto),
  };
}

function countMaterialFaceTypes<GI extends AnyGameIds>(
  materialChunks: UIMaterialChunk<GI>[]
) {
  const materialFaceTypeCount = new Map<UIMaterialFaceType<GI>, number>();
  let materialFaceTypeCountTotal = 0;
  for (const { secret, global, count } of materialChunks) {
    materialFaceTypeCountTotal += count;
    if (secret !== undefined) {
      increaseMapValue(materialFaceTypeCount, secret.type, count);
    }
    if (global !== secret) {
      increaseMapValue(materialFaceTypeCount, global.type, count);
    }
  }
  return { materialFaceTypeCount, materialFaceTypeCountTotal };
}
