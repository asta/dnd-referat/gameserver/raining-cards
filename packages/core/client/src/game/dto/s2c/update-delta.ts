import { UIActionRevision, UIGame } from "@/game/game.types";
import {
  AnyGameIds,
  DeltaType,
  GameIds,
  MaterialUpdateDelta,
  S2CMessageDTOUpdateDelta,
} from "@raining.cards/common";
import { UIGameDeltaStep } from "@/game/game-delta.types";
import { UIGameState } from "@/game/game.service";
import { actionRevisionFromDTO } from "@/game/dto/s2c/update-actions";
import { materialViewFromDTO } from "@/game/material";

export interface S2CMessageClientUpdateDelta<GI extends AnyGameIds = GameIds> {
  allowedActions?: UIActionRevision<GI>;
  steps: UIGameDeltaStep<GI>[];
}

export function s2cGameUpdateDeltaFromDTO<GI extends AnyGameIds>(
  { game }: UIGameState<GI>,
  [actions, steps]: S2CMessageDTOUpdateDelta<GI>
): S2CMessageClientUpdateDelta<GI> {
  return {
    allowedActions: actions
      ? actionRevisionFromDTO(game.meta, actions)
      : undefined,
    steps: steps.map((it) => gameDeltaFromDTO(game, it)),
  };
}

function gameDeltaFromDTO<GI extends AnyGameIds>(
  game: UIGame<GI>,
  dto: MaterialUpdateDelta<GI>
): UIGameDeltaStep<GI> {
  switch (dto.type) {
    case DeltaType.FLUSH:
      return dto;
    case DeltaType.CREATE:
      return {
        type: DeltaType.CREATE,
        index: dto.targetIndex,
        materialView: materialViewFromDTO(game.meta, dto.targetView),
        zone: game.zones.asMap[dto.targetZoneId],
      };
    case DeltaType.DESTROY:
      return {
        type: DeltaType.DESTROY,
        index: dto.originIndex,
        zone: game.zones.asMap[dto.originZoneId],
      };
    case DeltaType.UPDATE:
      return {
        type: DeltaType.UPDATE,
        newMaterialView: dto.targetView
          ? materialViewFromDTO(game.meta, dto.targetView)
          : undefined,
        origin: {
          zone: game.zones.asMap[dto.originZoneId],
          index: dto.originIndex,
        },
        target: dto.targetZoneId
          ? {
              zone: game.zones.asMap[dto.targetZoneId],
              index: dto.targetIndex!,
            }
          : undefined,
      };
  }
}
