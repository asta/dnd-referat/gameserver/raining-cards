import {
  Material,
  MaterialPosition,
  MaterialPositionDescriptor,
} from "@/game/material/material.types";
import { positionFromInsertDescriptor } from "@/game/material/material.position";
import { debug } from "@/util";
import { AnyGameIds } from "@raining.cards/common";
import { TransactionDirtyState } from "@/comfort/use/transaction/transaction-dirty-state";
import {
  bakeDeltaReveal,
  insertMaterial,
} from "@/comfort/use/transaction/material.util";

export interface TransactionCreate<GI extends AnyGameIds> {
  create: (
    material: Material<GI>,
    pos: MaterialPositionDescriptor<GI>
  ) => MaterialPosition<GI>;
}

export function useTransactionCreate<GI extends AnyGameIds>(
  dirtyState: TransactionDirtyState<GI>
): TransactionCreate<GI> {
  return { create };

  function create(
    material: Material<GI>,
    pos: MaterialPositionDescriptor<GI>
  ): MaterialPosition<GI> {
    const target = positionFromInsertDescriptor(pos);
    debug("Transaction: Create", { position: target, material });
    insertMaterial(material, target);
    dirtyState.add(null, {
      material,
      location: { origin: null, target },
      reveal: bakeDeltaReveal(material, target),
    });
    return { ...target };
  }
}
