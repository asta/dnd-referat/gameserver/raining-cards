import { assert } from "ts-essentials";
import { Predicate } from "@/util/types";
import { fromEntries, pluckId } from "@/util/object";

export interface ListAndLookup<
  T,
  Id extends string | number = T extends { id: any } ? T["id"] : string
> {
  asList: T[];
  asLookup: Record<Id, T>;
}

export interface ListAndPartialLookup<
  T,
  Id extends string | number = T extends { id: any } ? T["id"] : string
> {
  asList: T[];
  asLookup: Partial<Record<Id, T>>;
}

export function withLookup<T extends { id: string }>(
  asList: T[]
): ListAndLookup<T, T["id"]>;
export function withLookup<T, Id extends string | number>(
  asList: T[],
  pluck: (it: T) => Id
): ListAndLookup<T, Id>;
export function withLookup<T, Id extends string | number>(
  asList: T[],
  pluck = pluckId as (value: T) => Id
): ListAndLookup<T, Id> {
  return {
    asList,
    asLookup: fromEntries(asList.map((it) => [pluck(it), it])),
  };
}

export function withPartialLookup<T extends { id: string }>(
  asList: T[]
): ListAndPartialLookup<T, T["id"]>;
export function withPartialLookup<T, Id extends string | number>(
  asList: T[],
  pluck: (it: T) => Id
): ListAndPartialLookup<T, Id>;
export function withPartialLookup<T, Id extends string | number>(
  asList: T[],
  pluck = pluckId as (value: T) => Id
): ListAndPartialLookup<T, Id> {
  return {
    asList,
    asLookup: fromEntries(asList.map((it) => [pluck(it), it])),
  };
}

export function pickOne<T>(list: T[]): T {
  return list[(Math.random() * list.length) | 0];
}

export function pickNext<T>(list: T[], current: T): T {
  const idx = list.indexOf(current);
  assert(idx !== -1, "Item should be in list");
  if (idx === list.length - 1) {
    return list[0];
  }
  return list[idx + 1];
}

export function pickPrev<T>(list: T[], current: T): T {
  const idx = list.indexOf(current);
  assert(idx !== -1, "Item should be in list");
  if (idx === 0) {
    return list[list.length - 1];
  }
  return list[idx - 1];
}

export function times<T>(count: number, factory: (idx: number) => T): T[] {
  return Array.from(Array(count).keys()).map(factory);
}

export function flatten1<T>(list: T[][]): T[] {
  return Array.prototype.concat.apply([], list);
}

export function shuffle<T>(list: T[]): T[] {
  return list.sort(() => Math.random() - 0.5);
}

export function findLastIndex<T>(
  list: T[],
  iteratee: Predicate<[T, number]>,
  i = list.length - 1
): number {
  for (; i >= 0; i--) {
    if (iteratee(list[i], i)) {
      return i;
    }
  }
  return -1;
}

export type KeyValue<K, V> = [K, V];

export function listToObject<ResultKey extends string, ResultValue, T>(
  list: T[],
  iteratee: (value: T, idx: number) => KeyValue<ResultKey, ResultValue>
): Record<ResultKey, ResultValue> {
  const obj = {} as Record<ResultKey, ResultValue>;
  for (let i = 0; i < list.length; i++) {
    const [key, value] = iteratee(list[i], i);
    obj[key] = value;
  }
  return obj;
}

export function mergeAll<T extends Record<any, any>>(list: T[]): T {
  const obj = {} as T;
  for (const item of list) {
    for (const key in item) {
      obj[key] = item[key];
    }
  }
  return obj;
}

export function withoutUnique<T>(list: T[], item: T): T[] {
  const idx = list.indexOf(item);
  if (idx === 0) {
    return list.slice(1);
  }
  if (idx === list.length - 1) {
    return list.slice(0, list.length - 1);
  }
  return list.slice(0, idx).concat(list.slice(idx + 1));
}

export function groupBy<T, K extends string | number | symbol>(
  list: T[],
  iteratee: (value: T, idx: number) => K
): Record<K, T[]> {
  const result = {} as Record<K, T[]>;
  for (let i = 0; i < list.length; i++) {
    const item = list[i];
    const key = iteratee(item, i);
    if (Reflect.has(result, key)) {
      result[key]!.push(item);
    } else {
      result[key] = [item];
    }
  }
  return result;
}

export function spliceCopy<T>(
  list: T[],
  index: number,
  deleteCount = 0,
  insert?: T[]
): T[] {
  if (deleteCount === 0) {
    if (insert === undefined) {
      // no insert, no delete, just copy
      return list.slice();
    }
    // no delete, just combine
    if (index <= 0) {
      return insert.concat(list);
    } else if (index >= list.length) {
      return list.concat(insert);
    }
    return list.slice(0, index).concat(insert, list.slice(index));
  }
  if (insert === undefined) {
    // no insert, just delete
    if (index <= 0) {
      return list.slice(deleteCount);
    } else if (index + deleteCount >= list.length) {
      return list.slice(0, index);
    }
    return list.slice(0, index).concat(list.slice(index + deleteCount));
  }
  // insert and delete
  if (index <= 0) {
    return insert.concat(list.slice(deleteCount));
  } else if (index + deleteCount >= list.length) {
    return list.slice(0, index).concat(insert);
  }
  return list.slice(0, index).concat(insert, list.slice(index + deleteCount));
}
