import { ToBGame } from "@/ToB.game";
import { ToBActionCondition } from "@/condition";
import { creditMaterialFactory } from "@/material/material-factories";
import { ToBActionSchemaGroup, ToBActionSchema } from "@/action/action.types";
import { ToBGameIds } from "@/ToB.types";
import {
  ActionSchema,
  ClientAction,
  ClientActorZone,
  last,
  Zone,
} from "@raining.cards/server";
import { ElementType } from "@raining.cards/common";
import { matchIt, times } from "@raining.cards/util";
import { PlayerZonesMap } from "@/zone/zones";
import { ToBZoneType } from "@/zone/zone-types";
import { CREDIT_SYMBOL } from "@/material/face-types";

interface LatestForeignAid {
  zone: Zone<ToBGameIds>;
}

export const LATEST_FOREIGN_AID = new WeakMap<ToBGame, LatestForeignAid>();

export function foreignAidSchema(
  game: ToBGame,
  { zonesSelf }: PlayerZonesMap
): ActionSchema<ToBGameIds> {
  return {
    id: ToBActionSchema.FOREIGN_AID,
    name: "Foreign aid",
    description: `Income: ${CREDIT_SYMBOL}2; can be blocked by <em>Duke</em>.`,
    origin: {
      type: ElementType.ZONE,
      zone: [zonesSelf[ToBZoneType.PLAYER_CREDITS].id],
    },
    target: { type: ElementType.GLOBAL },
    condition: matchIt(ToBActionCondition.CREDITS_MAX_9),
  };
}

export async function performForeignAid(
  game: ToBGame,
  action: ClientAction<ToBGameIds>
) {
  game.logs.add(`${action.player.name} collects ${CREDIT_SYMBOL}2 foreign aid`);
  const { zone } = action.origin as ClientActorZone<ToBGameIds>;

  LATEST_FOREIGN_AID.set(game, { zone });
  const transaction = forwardForeignAid(game);

  game.actionStack.peekAssert().add(() => {
    game.transaction.addEnemyAction(
      action.player,
      ToBActionSchemaGroup.BLOCK_FOREIGN_AID
    );
    // apply only once
    return false;
  });
  game.turn.step();
  return transaction;
}

export function forwardForeignAid(game: ToBGame) {
  const { zone } = LATEST_FOREIGN_AID.get(game)!;
  times(2, () =>
    game.transaction.create(creditMaterialFactory(game), last(zone))
  );
}

export function backwardForeignAid(game: ToBGame) {
  const { zone } = LATEST_FOREIGN_AID.get(game)!;
  times(2, () => game.transaction.destroy(last(zone)));
}
