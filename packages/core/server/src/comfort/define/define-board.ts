import { AnyGameIds, GameIds } from "@raining.cards/common";
import { Zone } from "@/game/zone/zone.types";
import { withLookup } from "@raining.cards/util";
import { GameBoard } from "@/game/game.types";

export type DefinedBoard<GI extends AnyGameIds = GameIds> = GameBoard<GI>;

export function defineBoard<GI extends AnyGameIds>(
  asTree: Zone<GI>[]
): DefinedBoard<GI> {
  return { asTree, ...withLookup(flatZones(asTree)) };
}

function flatZones<GI extends AnyGameIds>(zones: Zone<GI>[]): Zone<GI>[] {
  let result = zones;
  for (const zone of zones) {
    if (zone.children !== undefined) {
      result = result.concat(flatZones(zone.children));
    }
  }
  return result;
}
