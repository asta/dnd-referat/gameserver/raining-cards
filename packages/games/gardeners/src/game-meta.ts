import { MaterialFaceGroups } from "@/material/material-face-groups";
import { defineGameMeta, Game } from "@raining.cards/server";
import { MaterialFaces } from "@/material/material-faces";
import { Zones } from "@/zones";
import { ActionConditions } from "@/action/action-conditions";
import { Actions } from "@/action/actions";
import { ActionSchemaGroups } from "@/action/action-schema-groups";

export type GameMeta = Game["meta"];

export function useGameMeta(
  materialFaces: MaterialFaces,
  materialFaceGroups: MaterialFaceGroups,
  zones: Zones,
  actions: Actions,
  actionConditions: ActionConditions,
  actionSchemaGroups: ActionSchemaGroups
): GameMeta {
  return defineGameMeta(
    {
      materialFaceTypes: materialFaces.asList,
      materialFaceTypeGroups: materialFaceGroups.asList,
      zoneTypes: zones.asList,
    },
    (client, isPlayer) => {
      return isPlayer
        ? {
            actionConditions: actionConditions.asList.concat(
              actionConditions.of(client).asList
            ),
            actionSchemas: actions.asList.concat(actions.of(client).asList),
            actionSchemaGroups: actionSchemaGroups.asList.concat(
              actionSchemaGroups.of(client).asList
            ),
          }
        : {};
    }
  );
}
