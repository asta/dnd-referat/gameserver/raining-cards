import { ToBGame } from "@/ToB.game";
import {
  backwardForeignAid,
  forwardForeignAid,
  LATEST_FOREIGN_AID,
} from "@/action/foreign-aid.action";
import { ToBActionSchemaGroup, ToBActionSchema } from "@/action/action.types";
import { ToBGameIds } from "@/ToB.types";
import { ActionSchema, ClientAction } from "@raining.cards/server";
import { ElementType } from "@raining.cards/common";
import { doubtActionStackItem } from "@/action/doubt.action";

export function foreignAidBlockSchema(): ActionSchema<ToBGameIds> {
  return {
    id: ToBActionSchema.BLOCK_FOREIGN_AID_DUKE,
    name: "Block foreign aid",
    description: `Role: <em>Duke</em>.<br/>Blocks some foreign aid.`,
    origin: { type: ElementType.GLOBAL },
    target: { type: ElementType.GLOBAL },
  };
}

export async function performForeignAidBlock(
  game: ToBGame,
  action: ClientAction<ToBGameIds>
) {
  backwardForeignAid(game);
  const { zone } = LATEST_FOREIGN_AID.get(game)!;
  const player = game.playerByZone.get(zone)!;
  game.logs.add(
    `${action.player.name} blocks foreign aid for ${player.name} (duke)`
  );
  const actionStackItem = game.actionStack.push([
    () => {
      game.transaction.addPlayerAction(player, ToBActionSchemaGroup.CONTINUE);
    },
    doubtActionStackItem(game, { action, then: () => forwardForeignAid(game) }),
  ]);
  await game.performTask();
  game.actionStack.popAssert(actionStackItem);
}
