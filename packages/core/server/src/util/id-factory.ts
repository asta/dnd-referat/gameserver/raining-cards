import { customAlphabet } from "nanoid";
import { getEnvironment } from "./environment";
import { Environment } from "./environment.types";

export enum Alphabet {
  // removed: 0O and 1I => 32 chars
  HUMAN_UPPER = "23456789ABCDEFGHJKLMNPQRSTUVWXYZ",
  // -._~ are unreserved characters and have no special meaning for URLs => 66 chars
  URL = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-._~",
  // 1-byte characters within UTF-8 except for \ and " as those would require escaping within JSON => 92 chars
  INTERNAL = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz !#$%&'()*+,-./:;<=>?@[]^_`{|}",
}

export interface IdFactoryOptions {
  alphabet: Alphabet;
  size: number;
}

export type IdFactory<Id = string> = (debugId?: string) => Id;

export function idFactory<Id extends string = string>(
  debugPrefix: string | null,
  options: IdFactoryOptions | Alphabet = Alphabet.INTERNAL,
  debugEnv: Environment[] = [Environment.DEVELOPMENT, Environment.TEST],
  debug = debugEnv.includes(getEnvironment())
): IdFactory<Id> {
  // simple incrementing integer factory
  if (options === undefined) {
    let prev = -1;
    if (debug) {
      return debugPrefix == null
        ? (debugId?: string) => (debugId ?? (++prev).toString()) as Id
        : (debugId?: string) => `${debugPrefix}#${debugId ?? ++prev}` as Id;
    }
    return () => (++prev).toString() as Id;
  }
  // simple character walkthrough
  if (typeof options === "string") {
    let prev = -1;
    if (debug) {
      return debugPrefix == null
        ? (debugId?: string) => (debugId ?? options[++prev].toString()) as Id
        : (debugId?: string) =>
            `${debugPrefix}#${debugId ?? options[++prev]}` as Id;
    }
    return () => options[++prev].toString() as Id;
  }
  // randomized values (check https://zelark.github.io/nano-id-cc/ for collision
  // chances)
  const gen = customAlphabet(options.alphabet, options.size);
  if (debug) {
    return debugPrefix == null
      ? (debugId?: string) => (debugId ?? gen()) as Id
      : (debugId?: string) => `${debugPrefix}#${debugId ?? gen()}` as Id;
  }
  return gen as IdFactory<Id>;
}
