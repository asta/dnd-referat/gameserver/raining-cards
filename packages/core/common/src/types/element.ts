export enum ElementType {
  GLOBAL = "global",
  ZONE = "zone",
  MATERIAL = "material",
  MATERIALS = "materials",
}

export type ElementTypeSingle = Exclude<ElementType, ElementType.MATERIALS>;

export const ELEMENT_TYPES = Object.values(ElementType);
export const ELEMENT_TYPES_SINGLE: ElementType[] = [
  ElementType.GLOBAL,
  ElementType.ZONE,
  ElementType.MATERIAL,
];
