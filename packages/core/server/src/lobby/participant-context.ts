import * as WebSocket from "ws";
import { createClient } from "@/lobby/participant";
import { Lobby } from "@/lobby/Lobby";
import { Client } from "@/lobby/participant.types";
import { assert } from "ts-essentials";
import { AnyGameIds, GameIds } from "@raining.cards/common";

const CLIENT_CONTEXT_MAP = new WeakMap<WebSocket, ClientContext<GameIds>>();

export interface ClientContext<GI extends AnyGameIds, GameProps = unknown> {
  lobby: Lobby<GI, GameProps>;
  client: Client;
}

export function createClientContext<GI extends AnyGameIds, GameProps>(
  socket: WebSocket,
  lobby: Lobby<GI, GameProps>,
  clientArg: string | Client
): ClientContext<GI, GameProps> {
  const client: Client =
    typeof clientArg === "string" ? createClient(socket, clientArg) : clientArg;
  const ctx: ClientContext<GI, GameProps> = { lobby, client };
  CLIENT_CONTEXT_MAP.set(socket, (ctx as unknown) as ClientContext<GameIds>);
  return ctx;
}

export function getClientContext<GI extends AnyGameIds = GameIds>(
  socket: WebSocket
): ClientContext<GI> | undefined {
  return CLIENT_CONTEXT_MAP.get(socket) as ClientContext<GI> | undefined;
}

export function deleteClientContext(socket: WebSocket) {
  CLIENT_CONTEXT_MAP.delete(socket);
}

export function assertNoContext(socket: WebSocket) {
  assert(getClientContext(socket) == null, "Already in other lobby.");
}

export function assertContext(socket: WebSocket): ClientContext<GameIds> {
  const ctx = getClientContext(socket);
  assert(ctx != null, "Not in lobby.");
  return ctx;
}
