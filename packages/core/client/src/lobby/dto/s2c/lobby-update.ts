import {
  AnyGameIds,
  S2CMessageDTOLobbyUpdate,
  ServerCommand,
} from "@raining.cards/common";
import { UILobby } from "@/game/game.types";
import { UIGameBoxesState } from "@/game-boxes/game-boxes.service";
import { lobbyFromDTO } from "@/lobby/dto/s2c/lobby-init";

export interface S2CMessageClientLobbyUpdate {
  cmd: ServerCommand.LOBBY_TEAMS | ServerCommand.GAME_OPTIONS;
  lobby: UILobby;
}

export function s2cLobbyUpdateFromDTO<GI extends AnyGameIds>(
  gameBoxesState: UIGameBoxesState<GI>,
  lobby: S2CMessageDTOLobbyUpdate
): Omit<S2CMessageClientLobbyUpdate, "cmd"> {
  return { lobby: lobbyFromDTO(gameBoxesState, lobby) };
}
