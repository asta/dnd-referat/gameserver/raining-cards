import { defineAsyncComponent, defineComponent } from "vue";

export type AsyncProvider<Props = void> = () => Promise<() => Props>;

export function asyncProvider<T>(provider: AsyncProvider<T>) {
  return defineAsyncComponent(async () => {
    const providerSetup = await provider();
    return defineComponent({
      setup(props, { slots }) {
        const result = providerSetup();
        return () => slots.default!(result);
      },
    });
  });
}

export function parallelAsyncProvider(...providers: AsyncProvider[]) {
  return defineAsyncComponent(async () => {
    const providerSetups = await Promise.all(
      providers.map((provider) => provider())
    );
    return defineComponent({
      setup(props, { slots }) {
        for (const providerSetup of providerSetups) {
          providerSetup();
        }
        return () => slots.default!();
      },
    });
  });
}
