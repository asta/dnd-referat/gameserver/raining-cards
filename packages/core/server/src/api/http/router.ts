import { IncomingMessage, Server, ServerResponse } from "http";
import { gameBoxesHandler } from "@/game/web-api/game-boxes.handler";
import { infoHandler } from "@/game/web-api/info.handler";
import { ServerOptions } from "@/server/server";
import { StatsContext } from "@/meta/stats";
import { statsHandler } from "@/game/web-api/stats.handler";

export function registerAPI(
  server: Server,
  options: ServerOptions,
  stats: StatsContext
) {
  server.addListener("request", (req: IncomingMessage, res: ServerResponse) => {
    // TODO restrict to known domains / localhost for development
    res.setHeader("Access-Control-Allow-Origin", "*");

    if (!req.url) {
      res.writeHead(404);
      return res.end();
    }

    switch (req.url) {
      case "/games.json":
        gameBoxesHandler(res, options);
        stats.httpRequestRouted(req.url);
        break;
      case "/info.json":
        infoHandler(res, options);
        stats.httpRequestRouted(req.url);
        break;
      case "/stats.json":
        stats.httpRequestRouted(req.url);
        statsHandler(res, stats);
        break;
      default:
        res.writeHead(404).end();
    }
  });
}
