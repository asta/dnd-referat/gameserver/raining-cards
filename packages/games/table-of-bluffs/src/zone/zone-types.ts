import { ZoneType } from "@raining.cards/server";
import { ToBGameIds } from "@/ToB.types";

export enum ToBZoneType {
  GLOBAL = "global",
  DECK = "deck",
  TREASURY = "treasury",
  PLAYER_DOMAIN = "player-domain",
  PLAYER_CREDITS = "player-credits",
  PLAYER_ROLES = "player-roles",
  PLAYER_TEAM = "player-team",
}

export type PlayerZoneType =
  | ToBZoneType.PLAYER_DOMAIN
  | ToBZoneType.PLAYER_CREDITS
  | ToBZoneType.PLAYER_ROLES
  | ToBZoneType.PLAYER_TEAM;

export type GlobalZoneType =
  | ToBZoneType.GLOBAL
  | ToBZoneType.DECK
  | ToBZoneType.TREASURY;

export const TOB_PLAYER_ZONE_TYPES: PlayerZoneType[] = [
  ToBZoneType.PLAYER_DOMAIN,
  ToBZoneType.PLAYER_CREDITS,
  ToBZoneType.PLAYER_ROLES,
  ToBZoneType.PLAYER_TEAM,
];

export const ZONE_TYPES: ZoneType<ToBGameIds>[] = [
  {
    id: ToBZoneType.GLOBAL,
    label: "Global",
    uiMeta: { styles: { gridColumn: "1 / -1", margin: "0 15% 22px" } },
  },
  { id: ToBZoneType.DECK, uiMeta: { styles: { margin: "auto" } } },
  { id: ToBZoneType.TREASURY, uiMeta: { styles: { margin: "auto" } } },

  { id: ToBZoneType.PLAYER_DOMAIN },
  { id: ToBZoneType.PLAYER_CREDITS },
  { id: ToBZoneType.PLAYER_ROLES },
  { id: ToBZoneType.PLAYER_TEAM },
];
