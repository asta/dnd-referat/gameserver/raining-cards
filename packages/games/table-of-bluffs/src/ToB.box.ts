import { ToBGame } from "@/ToB.game";
import { ToBGameIds } from "@/ToB.types";
import {
  commonPkgMeta,
  GameBox,
  pushLobbyParticipants,
} from "@raining.cards/server";
import * as pkg from "../package.json";
import { shuffle } from "@raining.cards/util";
import { Lobby } from "@raining.cards/server/dist/lobby/Lobby";

const meta = commonPkgMeta(pkg);

type ToBProps = unknown;

export const TABLE_OF_BLUFFS_BOX: GameBox<ToBGameIds, ToBProps> = {
  id: "table-of-bluffs",
  meta,
  name: "Table of Bluffs",
  description: `
Related games:
<ul>
  <li><a href="https://www.indieboardsandcards.com/index.php/games/coup/">Coup</a> (<a href="https://www.boardgamegeek.com/boardgame/131357/coup">BGG</a>)</li>
  <li><a href="https://indieboardsandcards.com/index.php/our-games/coup-reformation/">Coup: Reformation</a> (<a href="https://boardgamegeek.com/boardgame/148931/coup-reformation">BGG</a>)</li>
</ul>

<p>In <em>Table of Bluffs</em> each player starts with two roles and two credits. The
roles are secret to the player.</p>
<p>In turn order each player can perform a single action until only one player is
left. Every player can perform any action or reaction of any role, regardless of
him/her owning such a role. Some actions can be reacted to by the other players.
Each role related action can be doubted. If a player has been doubted and does
not own the role for the action he attempted, he permanently loses one role; a
player without role withdraws from the game.</p>
<p>If on the other hand a doubt was not successful, the player who initiated the
doubt loses one role.</p>

<p>In addition, each player is assigned one of two teams. The affiliation can be
changed during the game due to player actions. If two, but not all, players are
in the same team, those players cannot perform any role related actions,
reactions and doubts against each other.</p>
`,
  lobby: { players: { min: 2, max: 8 } },
  factory(lobby: Lobby<ToBGameIds, ToBProps>) {
    shuffle(lobby.participants.players);
    pushLobbyParticipants(lobby);
    return new ToBGame(lobby.participants);
  },
};
