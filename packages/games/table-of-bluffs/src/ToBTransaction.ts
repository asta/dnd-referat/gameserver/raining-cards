import { ToBGame } from "@/ToB.game";
import { deriveTeam } from "@/material/derive-team";
import { ToBGameIds, ToBPlayerUIMeta } from "@/ToB.types";
import { ToBActionSchemaGroup } from "@/action/action.types";
import {
  Player,
  pushParticipants,
  useTransaction,
} from "@raining.cards/server";
import { ToBZoneType } from "@/zone/zone-types";

export type ToBTransaction = ReturnType<typeof useToBTransaction>;

export function useToBTransaction(game: ToBGame) {
  const core = useTransaction<ToBGameIds>(game.participants);

  return {
    ...core,
    addEnemyAction,
    playerDied,
  };

  function addEnemyAction(player: Player, group: ToBActionSchemaGroup) {
    const {
      zones: { byPlayer },
      turn: { items: players },
    } = game;
    const team = deriveTeam(byPlayer.get(player)!);
    const enemyPlayers = players.filter(
      (it) => deriveTeam(byPlayer.get(it)!) !== team
    );
    for (const p of enemyPlayers.length ? enemyPlayers : players) {
      if (player !== p) {
        core.addPlayerAction(p, group);
      }
    }
  }

  function playerDied(player: Player) {
    const { zones } = game;
    const playerZones = zones.byPlayer.get(player)!;
    core.destroyAll(playerZones[ToBZoneType.PLAYER_TEAM]);
    core.destroyAll(playerZones[ToBZoneType.PLAYER_CREDITS]);
    (player.uiMeta as ToBPlayerUIMeta).classNames.add("defeated");
    pushParticipants(game);
    game.turn.remove(player);
  }
}
