import { C2SMessageDTOLobbyJoin } from "@raining.cards/common";

export interface C2SMessageClientLobbyJoin {
  lobbyId: string;
  clientName: string;
}

export function c2sLobbyJoinToDTO({
  lobbyId,
  clientName,
}: C2SMessageClientLobbyJoin): string {
  const payload: C2SMessageDTOLobbyJoin = [lobbyId, clientName];
  return JSON.stringify(payload);
}
