import {
  defineAction,
  GameLogs,
  IdFactory,
  last,
  Transaction,
} from "@raining.cards/server";
import { ElementType, GameIds } from "@raining.cards/common";
import { Zones } from "@/zones";

export function usePlaceAction(
  idf: IdFactory<GameIds["actionSchema"]>,
  transaction: Transaction,
  gameLogs: GameLogs,
  zones: Zones
) {
  return defineAction(
    {
      id: idf("place"),
      name: "Place",
      description:
        "Place the monster into the dungeon. The hero must defeat this monster when it's up.",
      origin: { type: ElementType.ZONE, zone: [zones.dungeon] },
      target: { type: ElementType.GLOBAL },
    },
    ({ player }) => {
      const origin = last(zones.drawn);
      transaction.clearSecret(origin);
      transaction.move(origin, last(zones.dungeon));
      gameLogs.add(`${player.name} placed a monster into the dungeon.`);
    }
  );
}
