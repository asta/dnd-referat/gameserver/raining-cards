import { assert } from "ts-essentials";
import { Subscription } from "./subscription";
import { once } from "./function";

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export interface InjectionKey<T> {
  name?: string;
}

const UNNAMED_KEY = "[[ unnamed injection key ]]";
const UNNAMED_SCOPE = "[[ unnamed injection scope ]]";

export class InjectionScope {
  private map = new WeakMap<InjectionKey<unknown>, unknown>();

  constructor(readonly name: string = UNNAMED_SCOPE) {}

  provide<T>(key: InjectionKey<T>, value: T): Subscription {
    assert(
      !this.map.has(key),
      `[${this.name}] Injection overrides are not allowed (key:${
        key.name ?? UNNAMED_KEY
      }).`
    );
    this.map.set(key, value);
    return once(() => {
      this.map.delete(key);
    });
  }

  injectAssert<T>(key: InjectionKey<T>): T {
    const value = this.map.get(key);
    assert(
      value !== undefined,
      `[${this.name}] Injection failed (key:${key.name ?? UNNAMED_KEY}).`
    );
    return value as T;
  }

  inject<T>(key: InjectionKey<T>): T | undefined {
    return this.map.get(key) as T | undefined;
  }
}
