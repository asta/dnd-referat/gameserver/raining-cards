import { ServerOptions } from "@/server/server";
import { GameBox } from "@/game/game.types";
import { AnyGameIds } from "@raining.cards/common";
import * as WebSocket from "ws";
import { RingBuffer } from "@raining.cards/util";

interface StatsSnapshot {
  datetime: Date;
  connections: number;
  lobbies: number;
  games: Record<GameBox["id"], number>;
  gamesTotal: number;
}

interface StatsInterval {
  start: Date;
  httpRequests: Record</* known url */ string, number>;
  connectionsCreated: number;
  lobbiesCreated: number;
  gamesCreated: Record<GameBox["id"], number>;
  gamesCreatedTotal: number;
  gamesResolved: Record<GameBox["id"], number>;
  gamesResolvedTotal: number;
  gamesAborted: Record<GameBox["id"], number>;
  gamesAbortedTotal: number;
}

interface Stats {
  server: {
    started: Date;
  };
  now: StatsSnapshot;
  total: StatsInterval;
  history: {
    "15minutely": StatsInterval[];
    hourly: StatsInterval[];
    daily: StatsInterval[];
  };
}

export const STATS_CONTEXT = new WeakMap<WebSocket, StatsContext>();

export type StatsContext = ReturnType<typeof useStats>;

export function useStats({ gameBoxes }: ServerOptions) {
  const GAME_IDS = gameBoxes.map((it) => it.id);

  const SERVER_STARTED = new Date();

  const HISTORY_15_MINUTES = new RingBuffer<StatsInterval>(24); // 6 hours
  const HISTORY_HOURLY = new RingBuffer<StatsInterval>(36); // 1.5 days
  const HISTORY_DAILY = new RingBuffer<StatsInterval>(30); // 30 days

  const TOTAL_INTERVAL = initInterval(SERVER_STARTED);
  const ACTIVE_SNAPSHOT = initSnapshot();

  let current15Minutes = initInterval(SERVER_STARTED);
  let currentHourly = initInterval(SERVER_STARTED);
  let currentDaily = initInterval(SERVER_STARTED);
  HISTORY_15_MINUTES.push(current15Minutes);
  HISTORY_HOURLY.push(currentHourly);
  HISTORY_DAILY.push(currentDaily);

  const MINUTES_15 = 900000;
  let i = 0;
  setInterval(() => {
    const now = new Date();
    current15Minutes = initInterval(now);
    HISTORY_15_MINUTES.push(current15Minutes);
    i++;
    if (i % 4 !== 0) {
      return;
    }
    currentHourly = initInterval(now);
    HISTORY_HOURLY.push(currentHourly);
    if (i % 96 !== 0) {
      return;
    }
    currentDaily = initInterval(now);
    HISTORY_DAILY.push(currentDaily);
  }, MINUTES_15);

  return {
    collect,
    httpRequestRouted,
    connectionCreated,
    connectionClosed,
    lobbyCreated,
    lobbyCleaned,
    gameCreated,
    gameResolved,
    gameAborted,
  };

  function collect(): Stats {
    const now = new Date();
    return {
      server: { started: SERVER_STARTED },
      now: { datetime: now, ...ACTIVE_SNAPSHOT },
      total: TOTAL_INTERVAL,
      history: {
        "15minutely": [...HISTORY_15_MINUTES],
        hourly: [...HISTORY_HOURLY],
        daily: [...HISTORY_DAILY],
      },
    };
  }

  function httpRequestRouted(url: string) {
    current15Minutes.httpRequests[url] =
      (current15Minutes.httpRequests[url] ?? 0) + 1;
    currentHourly.httpRequests[url] =
      (currentHourly.httpRequests[url] ?? 0) + 1;
    currentDaily.httpRequests[url] = (currentDaily.httpRequests[url] ?? 0) + 1;
    TOTAL_INTERVAL.httpRequests[url] =
      (TOTAL_INTERVAL.httpRequests[url] ?? 0) + 1;
  }

  function connectionCreated() {
    current15Minutes.connectionsCreated++;
    currentHourly.connectionsCreated++;
    currentDaily.connectionsCreated++;
    TOTAL_INTERVAL.connectionsCreated++;
    ACTIVE_SNAPSHOT.connections++;
  }

  function connectionClosed() {
    ACTIVE_SNAPSHOT.connections--;
  }

  function lobbyCreated() {
    current15Minutes.lobbiesCreated++;
    currentHourly.lobbiesCreated++;
    currentDaily.lobbiesCreated++;
    TOTAL_INTERVAL.lobbiesCreated++;
    ACTIVE_SNAPSHOT.lobbies++;
  }

  function lobbyCleaned() {
    ACTIVE_SNAPSHOT.lobbies--;
  }

  function gameCreated<GI extends AnyGameIds, GameProps>({
    id,
  }: GameBox<GI, GameProps>) {
    current15Minutes.gamesCreatedTotal++;
    current15Minutes.gamesCreated[id]++;
    currentHourly.gamesCreatedTotal++;
    currentHourly.gamesCreated[id]++;
    currentDaily.gamesCreatedTotal++;
    currentDaily.gamesCreated[id]++;
    TOTAL_INTERVAL.gamesCreatedTotal++;
    TOTAL_INTERVAL.gamesCreated[id]++;
    ACTIVE_SNAPSHOT.gamesTotal++;
    ACTIVE_SNAPSHOT.games[id]++;
  }

  function gameResolved<GI extends AnyGameIds, GameProps>({
    id,
  }: GameBox<GI, GameProps>) {
    current15Minutes.gamesResolvedTotal++;
    current15Minutes.gamesResolved[id]++;
    currentHourly.gamesResolvedTotal++;
    currentHourly.gamesResolved[id]++;
    currentDaily.gamesResolvedTotal++;
    currentDaily.gamesResolved[id]++;
    TOTAL_INTERVAL.gamesResolvedTotal++;
    TOTAL_INTERVAL.gamesResolved[id]++;
    ACTIVE_SNAPSHOT.gamesTotal--;
    ACTIVE_SNAPSHOT.games[id]--;
  }

  function gameAborted<GI extends AnyGameIds, GameProps>({
    id,
  }: GameBox<GI, GameProps>) {
    current15Minutes.gamesAbortedTotal++;
    current15Minutes.gamesAborted[id]++;
    currentHourly.gamesAbortedTotal++;
    currentHourly.gamesAborted[id]++;
    currentDaily.gamesAbortedTotal++;
    currentDaily.gamesAborted[id]++;
    TOTAL_INTERVAL.gamesAbortedTotal++;
    TOTAL_INTERVAL.gamesAborted[id]++;
    ACTIVE_SNAPSHOT.gamesTotal--;
    ACTIVE_SNAPSHOT.games[id]--;
  }

  function initSnapshot(): Omit<StatsSnapshot, "datetime"> {
    return {
      connections: 0,
      lobbies: 0,
      games: gamesRecord(),
      gamesTotal: 0,
    };
  }

  function initInterval(start: Date): StatsInterval {
    return {
      start,
      httpRequests: {},
      connectionsCreated: 0,
      lobbiesCreated: 0,
      gamesCreatedTotal: 0,
      gamesResolvedTotal: 0,
      gamesAbortedTotal: 0,
      gamesCreated: gamesRecord(),
      gamesResolved: gamesRecord(),
      gamesAborted: gamesRecord(),
    };
  }

  function gamesRecord(): Record<GameBox["id"], number> {
    const games: Record<GameBox["id"], number> = {};
    for (const id of GAME_IDS) {
      games[id] = 0;
    }
    return games;
  }
}
