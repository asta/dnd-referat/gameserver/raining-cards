import { ToBGame } from "@/ToB.game";
import { ToBActionCondition } from "@/condition";
import { ToBZoneType } from "@/zone/zone-types";
import { ToBActionSchema } from "@/action/action.types";
import { ToBGameIds } from "@/ToB.types";
import { PlayerZonesMap, ToBZones } from "@/zone/zones";
import {
  ActionSchema,
  ClientAction,
  ClientActorZone,
  last,
} from "@raining.cards/server";
import { ElementType } from "@raining.cards/common";
import { matchAll, matchIt, times } from "@raining.cards/util";
import { doubtActionStackItem } from "@/action/doubt.action";
import { CREDIT_SYMBOL } from "@/material/face-types";

export function collectTreasureSchema(
  game: ToBGame,
  _: PlayerZonesMap,
  zones: ToBZones
): ActionSchema<ToBGameIds> {
  return {
    id: ToBActionSchema.COLLECT_TREASURE,
    name: "Collect",
    description: `Role: <em>Not Duke</em>; cannot be blocked.<br/>Collect all ${CREDIT_SYMBOL} out of the treasury.`,
    origin: {
      type: ElementType.ZONE,
      zone: [zones.global[ToBZoneType.TREASURY].id],
    },
    target: { type: ElementType.GLOBAL },
    condition: matchAll([
      matchIt(ToBActionCondition.CREDITS_MAX_9),
      matchIt(ToBActionCondition.CREDITS_MIN_2),
      matchIt(ToBActionCondition.TREASURE_MIN_1),
    ]),
  };
}

export async function performCollectTreasure(
  game: ToBGame,
  action: ClientAction<ToBGameIds>
) {
  const origin = action.origin as ClientActorZone<ToBGameIds>;
  const playerCredits = last(
    game.zones.byPlayer.get(action.player)![ToBZoneType.PLAYER_CREDITS]
  );
  const treasuryPosition = last(game.zones.global[ToBZoneType.TREASURY]);
  const credits = origin.zone.materials.length;
  game.logs.add(
    `${action.player.name} collects treasure of ${CREDIT_SYMBOL}${credits} (not duke)`
  );
  times(credits, () => game.transaction.move(treasuryPosition, playerCredits));
  game.actionStack.peekAssert().add(
    doubtActionStackItem(game, {
      action,
      then: () => backwardCollectTreasure(game, action, credits),
    })
  );
  game.turn.step();
}

function backwardCollectTreasure(
  game: ToBGame,
  action: ClientAction<ToBGameIds>,
  credits: number
) {
  const playerCredits = last(
    game.zones.byPlayer.get(action.player)![ToBZoneType.PLAYER_CREDITS]
  );
  const treasuryPosition = last(game.zones.global[ToBZoneType.TREASURY]);
  times(credits, () => game.transaction.move(playerCredits, treasuryPosition));
}
