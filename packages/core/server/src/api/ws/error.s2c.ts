import {
  S2CMessageDTOError,
  S2CMessageErrorCode,
  ServerCommand,
} from "@raining.cards/common";

export interface S2CMessageServerError {
  cmd: ServerCommand.ERROR;
  code: S2CMessageErrorCode;
  message?: string;
}

export function s2cErrorToDTO({
  code,
  message,
}: S2CMessageServerError): S2CMessageDTOError {
  return message === undefined ? [code] : [code, message];
}

export function isS2CMessageServerError(
  err: unknown
): err is S2CMessageServerError {
  return (
    typeof err === "object" &&
    err !== null &&
    (err as any).cmd === ServerCommand.ERROR
  );
}
