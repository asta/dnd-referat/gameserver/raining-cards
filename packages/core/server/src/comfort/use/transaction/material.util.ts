import { Material, MaterialPosition } from "@/game/material/material.types";
import { AnyGameIds } from "@raining.cards/common";
import { bakeReveal } from "@/game/material/material";
import { MaterialDeltaReveal } from "@/comfort/use/transaction/transaction-dirty-state";

export function dropMaterial<GI extends AnyGameIds>({
  zone: { materials },
  index,
}: MaterialPosition<GI>) {
  if (index < 0 || index >= materials.length) {
    throw new Error("Invalid zone index.");
  }
  if (index === materials.length - 1) {
    materials.pop();
  } else if (index === 0) {
    materials.shift();
  } else {
    materials.splice(index, 1);
  }
}

export function insertMaterial<GI extends AnyGameIds>(
  material: Material<GI>,
  { zone: { materials }, index }: MaterialPosition<GI>
) {
  if (index < 0 || index > materials.length) {
    throw new Error(`Invalid State: Insert index (${index}) out of bounds.`);
  }
  if (index === materials.length) {
    materials.push(material);
  } else if (index === 0) {
    materials.unshift(material);
  } else {
    materials.splice(index, 0, material);
  }
}

export function bakeDeltaReveal<GI extends AnyGameIds>(
  material: Material<GI>,
  { zone }: MaterialPosition<GI>
): MaterialDeltaReveal {
  return {
    origin: bakeReveal(material, zone),
    target: material.reveal,
  };
}
