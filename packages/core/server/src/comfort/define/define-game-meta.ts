import { AnyGameIds, GameIds } from "@raining.cards/common";
import { Game, GameMeta } from "@/game/game.types";
import { ListAndLookup, onceBy, withLookup } from "@raining.cards/util";
import { Client } from "@/lobby/participant.types";
import {
  ActionSchema,
  ActionSchemaGroup,
} from "@/game/action/action-schema.types";
import {
  MaterialFaceType,
  MaterialFaceTypeGroup,
} from "@/game/material/material.types";
import { ZoneType } from "@/game/zone/zone.types";
import { ActionCondition } from "@/game/action/action-condition";

export type DefinedGameMeta<GI extends AnyGameIds = GameIds> = Game<GI>["meta"];

const EMPTY_META: GameMeta = {
  actionSchemaGroups: [],
  actionSchemas: withLookup<ActionSchema<GameIds>>([]),
  zoneTypes: [],
  actionConditions: [],
  materialFaceTypeGroups: [],
  materialFaceTypes: [],
};

interface DefineGameMetaOptions {
  cacheClientMeta?: boolean;
}

const DEFAULT_OPTIONS: Required<DefineGameMetaOptions> = {
  cacheClientMeta: true,
};

export interface GameMetaAlike<GI extends AnyGameIds = GameIds> {
  actionSchemas:
    | ListAndLookup<ActionSchema<GI>>
    | (
        | ActionSchema<GI>
        | /* [Loose]DefineAction */ { schema: ActionSchema<GI> }
      )[];
  actionSchemaGroups: ActionSchemaGroup<GI>[];
  actionConditions: (
    | ActionCondition<GI>
    | /* DefineActionCondition */ { data: ActionCondition<GI> }
  )[];

  materialFaceTypes: (
    | MaterialFaceType<GI>
    | /* Define[d]MaterialFace */ { type: MaterialFaceType<GI> }
  )[];
  materialFaceTypeGroups: MaterialFaceTypeGroup<GI>[];

  zoneTypes: (ZoneType<GI> | /* Define[d]Zone */ { type: ZoneType<GI> })[];
}

export function defineGameMeta<GI extends AnyGameIds = GameIds>(
  staticMeta: Partial<GameMetaAlike<GI>> = {},
  clientMetaFactory?: (
    client: Client,
    isPlayer: boolean
  ) => Partial<GameMetaAlike<GI>>,
  options: DefineGameMetaOptions = {}
): DefinedGameMeta<GI> {
  const baseMeta = {
    ...(EMPTY_META as GameMeta<GI>),
    ...sanitizeGameMetaAlike(staticMeta),
  };

  if (clientMetaFactory === undefined) {
    return baseMeta;
  }
  const { cacheClientMeta } = { ...DEFAULT_OPTIONS, ...options };

  const clientMeta = cacheClientMeta
    ? onceBy(clientMetaFactory)
    : clientMetaFactory;

  return (client: Client, isPlayer: boolean): GameMeta<GI> => ({
    ...baseMeta,
    ...sanitizeGameMetaAlike(clientMeta(client, isPlayer)),
  });
}

function sanitizeGameMetaAlike<GI extends AnyGameIds>(
  it: Partial<GameMetaAlike<GI>>
): Partial<GameMeta<GI>> {
  const result = { ...it } as Partial<GameMeta<GI>>;
  if (it.actionSchemas) {
    if (isListAndLookup(it.actionSchemas)) {
      result.actionSchemas = it.actionSchemas;
    } else {
      result.actionSchemas = withLookup(
        it.actionSchemas.map(pluckActionSchema)
      );
    }
  }
  if (it.actionConditions) {
    result.actionConditions = it.actionConditions.map(pluckActionCondition);
  }
  if (it.materialFaceTypes) {
    result.materialFaceTypes = it.materialFaceTypes.map(pluckMaterialFaceType);
  }
  if (it.zoneTypes) {
    result.zoneTypes = it.zoneTypes.map(pluckZoneType);
  }
  return result;
}

function isListAndLookup<T>(
  it: ListAndLookup<T> | unknown
): it is ListAndLookup<T> {
  return Reflect.has(it as any, "asList") && Reflect.has(it as any, "asLookup");
}

function pluckActionSchema<GI extends AnyGameIds>(
  it: { schema: ActionSchema<GI> } | ActionSchema<GI>
): ActionSchema<GI> {
  if (Reflect.has(it, "schema")) {
    return (it as { schema: ActionSchema<GI> }).schema;
  }
  return it as ActionSchema<GI>;
}

function pluckActionCondition<GI extends AnyGameIds>(
  it: ActionCondition<GI> | { data: ActionCondition<GI> }
): ActionCondition<GI> {
  if (Reflect.has(it, "data")) {
    return (it as { data: ActionCondition<GI> }).data;
  }
  return it as ActionCondition<GI>;
}

function pluckMaterialFaceType<GI extends AnyGameIds>(
  it: MaterialFaceType<GI> | { type: MaterialFaceType<GI> }
): MaterialFaceType<GI> {
  if (Reflect.has(it, "type")) {
    return (it as { type: MaterialFaceType<GI> }).type;
  }
  return it as MaterialFaceType<GI>;
}

function pluckZoneType<GI extends AnyGameIds>(
  it: ZoneType<GI> | { type: ZoneType<GI> }
): ZoneType<GI> {
  if (Reflect.has(it, "type")) {
    return (it as { type: ZoneType<GI> }).type;
  }
  return it as ZoneType<GI>;
}
