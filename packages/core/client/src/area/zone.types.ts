import { UIClient, UIMaterialFaceType } from "@/game/game.types";
import { AnyGameIds, GameIds, UIMeta } from "@raining.cards/common";
import { Emitter } from "@raining.cards/util";
import {
  UIGameDeltaCreate,
  UIGameDeltaDestroy,
  UIGameDeltaUpdate,
} from "@/game/game-delta.types";
import { UIMaterialChunk, UIMaterialPositionInChunk } from "@/game/material";

export interface UIMaterialPosition<GI extends AnyGameIds = GameIds> {
  zone: UIZone<GI>;
  index: number;
}

export interface UIZoneEvtBeforeDetach<GI extends AnyGameIds = GameIds> {
  delta: UIGameDeltaUpdate<GI>;
  from: UIMaterialPositionInChunk<GI>;
  toZone: UIMaterialPosition<GI>;
  toChunk: UIMaterialPositionInChunk<GI>;
}

export interface UIZoneEvtCreated<GI extends AnyGameIds = GameIds> {
  delta: UIGameDeltaCreate<GI>;
  position: UIMaterialPositionInChunk<GI>;
}

export interface UIZoneEvtDestroy<GI extends AnyGameIds = GameIds> {
  delta: UIGameDeltaDestroy<GI>;
  position: UIMaterialPositionInChunk<GI>;
}

export type UIZoneEventMap<GI extends AnyGameIds = GameIds> = {
  // triggered for each atomic change (e.g. after destroyOne, before createOne,
  // ...)
  updated: void;
  // triggered after chunk is created
  created: UIZoneEvtCreated<GI>;
  // triggered after target chunk is created, before origin chunk is removed
  beforeDetach: UIZoneEvtBeforeDetach<GI>;
  // triggered before chunk is removed
  beforeDestroy: UIZoneEvtDestroy<GI>;
};

export interface UIZoneType<GI extends AnyGameIds = GameIds> {
  id: GI["zoneType"];
  label?: string;
  description?: string;
  relatedTo?: UIClient[];
  uiMeta?: UIMeta;
}

export interface UIZone<GI extends AnyGameIds = GameIds>
  extends Emitter<UIZoneEventMap<GI>> {
  id: GI["zone"];
  type: UIZoneType<GI>;
  children?: UIZone<GI>[];
  customMeta?: Partial<Omit<UIZoneType<GI>, "id">>;
  materialChunks: UIMaterialChunk<GI>[];
  materialFaceTypeCountTotal: number;
  materialFaceTypeCount: Map<UIMaterialFaceType<GI>, number>;
}
