import {
  defineAction,
  GameLogs,
  IdFactory,
  TurnOrder,
} from "@raining.cards/server";
import { ElementType, GameIds } from "@raining.cards/common";

export function useFoldAction(
  idf: IdFactory<GameIds["actionSchema"]>,
  localTurnOrder: TurnOrder,
  gameLogs: GameLogs
) {
  return defineAction(
    {
      id: idf("fold"),
      name: "Fold",
      description:
        "End your turn without drawing a monster. You won't send the hero into the dungeon.",
      origin: { type: ElementType.GLOBAL },
      target: { type: ElementType.GLOBAL },
    },
    ({ player }) => {
      localTurnOrder.remove(player);
      gameLogs.add(`${player.name} folded.`);
    }
  );
}
