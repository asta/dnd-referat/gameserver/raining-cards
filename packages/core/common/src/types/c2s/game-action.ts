import { AnyGameIds, ElementType } from "@/types";

export type C2SMessageDTOGameAction<GI extends AnyGameIds> = [
  /* actionsRevision */ number,
  ClientActionDTO<GI>
];

export interface ClientActionDTO<GI extends AnyGameIds> {
  schema: GI["actionSchema"];
  origin: ClientActorDTO<GI>;
  target: ClientActorSingleDTO<GI>;
}

export type ClientActorSingleDTO<GI extends AnyGameIds> =
  | ClientActorGlobalDTO
  | ClientActorZoneDTO<GI>
  | ClientActorMaterialDTO<GI>;

export type ClientActorDTO<GI extends AnyGameIds> =
  | ClientActorSingleDTO<GI>
  | ClientActorMaterialsDTO<GI>;

export interface ClientActorGlobalDTO {
  type: ElementType.GLOBAL;
}

export interface ClientActorZoneDTO<GI extends AnyGameIds> {
  type: ElementType.ZONE;
  zone: GI["zone"];
}

export interface ClientActorMaterialDTO<GI extends AnyGameIds>
  extends MaterialPositionDescriptorDTO<GI> {
  type: ElementType.MATERIAL;
}

export interface ClientActorMaterialsDTO<GI extends AnyGameIds> {
  type: ElementType.MATERIALS;
  materials: MaterialPositionDescriptorDTO<GI>[];
}

export interface MaterialPositionDescriptorDTO<GI extends AnyGameIds> {
  zone: GI["zone"];
  index: number;
}
