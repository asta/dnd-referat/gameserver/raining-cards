import { MaterialFaceDefinition } from "@/comfort";
import { IdFactory } from "@/util";
import {
  MaterialFaceType,
  MaterialFaceTypeGroup,
} from "@/game/material/material.types";
import { AnyGameIds, GameIds } from "@raining.cards/common";

export interface DefinedMaterialFaceTypeGroup<GI extends AnyGameIds = GameIds>
  extends MaterialFaceTypeGroup<GI> {
  faces: MaterialFaceType<GI>[];
  toJSON: () => MaterialFaceTypeGroup<GI>;
}

export function defineMaterialFaceTypeGroup<GI extends AnyGameIds = GameIds>(
  idf: GI["materialFaceTypeGroup"] | IdFactory<GI["materialFaceTypeGroup"]>,
  faceDefs: MaterialFaceDefinition<GI>[]
): DefinedMaterialFaceTypeGroup<GI> {
  return {
    id: typeof idf === "function" ? idf() : idf,
    faceIds: faceDefs.map((it) => it.type.id),
    faces: faceDefs.map((it) => it.type),
    toJSON: groupToJSON,
  };
}

function groupToJSON<GI extends AnyGameIds>(
  this: DefinedMaterialFaceTypeGroup<GI>
): MaterialFaceTypeGroup<GI> {
  return { id: this.id, faceIds: this.faceIds };
}
