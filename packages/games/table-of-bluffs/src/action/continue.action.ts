import { ToBGame } from "@/ToB.game";
import { ToBActionSchema } from "@/action/action.types";
import { ToBGameIds } from "@/ToB.types";
import { ActionSchema, ClientAction } from "@raining.cards/server";
import { ElementType } from "@raining.cards/common";

export function continueSchema(): ActionSchema<ToBGameIds> {
  return {
    id: ToBActionSchema.CONTINUE,
    name: "Continue",
    description: `Do not block or doubt.`,
    origin: { type: ElementType.GLOBAL },
    target: { type: ElementType.GLOBAL },
  };
}

export async function performContinue(
  game: ToBGame,
  action: ClientAction<ToBGameIds>
) {
  game.logs.add(`${action.player.name} concedes`);
}
