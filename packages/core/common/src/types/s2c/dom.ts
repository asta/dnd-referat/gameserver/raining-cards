import { StringifySet } from "@raining.cards/util";

export type UIMeta = {
  uiClasses?: StringifySet<string>;
  styles?: UIStyles;
} & any;

export type UIStyles = Partial<CSSStyleDeclaration>;
