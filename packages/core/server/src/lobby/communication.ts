import { ClientAction } from "@/game/action/client-action.types";
import { S2CMessageServer, s2cMessageToDTO } from "@/api/ws/s2c.serializer";
import { Client, Participants } from "@/lobby/participant.types";
import { isActive } from "@/lobby/participant";
import { debug, debugLazy } from "@/util";
import { AnyGameIds, SocketMessageLowLevelDTO } from "@raining.cards/common";

const ACTION_CALLBACKS = new WeakMap<Client, (action: ClientAction) => void>();

export function send<GI extends AnyGameIds, GameProps>(
  client: Client,
  message: S2CMessageServer<GI, GameProps>
) {
  if (isActive(client)) {
    _sendDTO(client, s2cMessageToDTO(message));
  }
}

export function sendDTO(client: Client, dto: SocketMessageLowLevelDTO) {
  if (isActive(client)) {
    _sendDTO(client, dto);
  }
}

function _sendDTO(client: Client, dto: SocketMessageLowLevelDTO) {
  debug("send message to", client.name, dto);
  client.socket!.send(dto);
}

export function commitNextClientAction<GI extends AnyGameIds>(
  client: Client,
  action: ClientAction<GI>
) {
  if (!ACTION_CALLBACKS.has(client)) {
    console.warn("No action handler registered.");
    return;
  }
  ACTION_CALLBACKS.get(client)!(action as ClientAction);
}

export async function nextClientAction<GI extends AnyGameIds = never>(
  client: Client
): Promise<ClientAction<GI>> {
  return await (new Promise<ClientAction>((resolve) => {
    ACTION_CALLBACKS.set(client, (action) => {
      ACTION_CALLBACKS.delete(client);
      resolve(action);
    });
  }) as Promise<ClientAction<GI>>);
}

export function broadcastOthers<GI extends AnyGameIds, GameProps>(
  actor: Client,
  participants: Participants,
  message: S2CMessageServer<GI, GameProps>
): SocketMessageLowLevelDTO {
  const dto = s2cMessageToDTO(message);
  broadcastOthersDTO(actor, participants, dto);
  return dto;
}

export function broadcastOthersDTO(
  actor: Client,
  { players, spectators }: Participants,
  dto: SocketMessageLowLevelDTO
) {
  debug("broadcast message to others by", actor.name, dto);
  for (const client of players) {
    if (client === actor || !isActive(client)) {
      continue;
    }
    client.socket.send(dto);
  }
  for (const client of spectators) {
    if (client === actor || !isActive(client)) {
      continue;
    }
    client.socket.send(dto);
  }
}

export function multicast<GI extends AnyGameIds, GameProps>(
  clients: Client[],
  message: S2CMessageServer<GI, GameProps>
): SocketMessageLowLevelDTO {
  const dto = s2cMessageToDTO(message);
  multicastDTO(clients, dto);
  return dto;
}

export function multicastDTO(clients: Client[], dto: SocketMessageLowLevelDTO) {
  debugLazy(() => ["multicast message to", clients.map((it) => it.name), dto]);
  for (const client of clients) {
    if (!isActive(client)) {
      continue;
    }
    client.socket.send(dto);
  }
}

export function broadcast<GI extends AnyGameIds, GameProps>(
  participants: Participants,
  message: S2CMessageServer<GI, GameProps>
): SocketMessageLowLevelDTO {
  const dto = s2cMessageToDTO(message);
  broadcastDTO(participants, dto);
  return dto;
}

export function broadcastDTO(
  { players, spectators }: Participants,
  dto: SocketMessageLowLevelDTO
) {
  multicastDTO(players, dto);
  multicastDTO(spectators, dto);
}

export function closeAllParticipants({ players, spectators }: Participants) {
  closeAll(players);
  closeAll(spectators);
}

export function closeAll(clients: Client[]) {
  for (const client of clients) {
    if (isActive(client)) {
      client.socket.close();
    }
  }
}

export function broadcastIndividual<GI extends AnyGameIds, GameProps>(
  { players, spectators }: Participants,
  messageFactory: (
    client: Client,
    isPlayer: boolean
  ) => S2CMessageServer<GI, GameProps>
) {
  for (const player of players) {
    send(player, messageFactory(player, true));
  }
  if (spectators.length > 0) {
    for (const spectator of spectators) {
      send(spectator, messageFactory(spectator, false));
    }
  }
}
