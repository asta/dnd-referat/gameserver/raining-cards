import * as WebSocket from "ws";
import { keepConnectionsAlive } from "@/server/keep-alive";
import { IncomingMessage, Server } from "http";
import { GameBoxMap } from "@/game/game.types";
import { c2sRouter, clientDisconnectHandler } from "@/api/ws/c2s.router";
import { debug, isDebug } from "@/util";
import {
  c2sFromDTO,
  C2SMessageServer,
  parseClientCommand,
} from "@/api/ws/c2s.parser";
import { s2cMessageToDTO } from "@/api/ws/s2c.serializer";
import { useMulticast } from "@/server/notification";
import {
  isS2CMessageServerError,
  S2CMessageServerError,
} from "@/api/ws/error.s2c";
import {
  ClientCommand,
  GameIds,
  S2CMessageErrorCode,
  ServerCommand,
} from "@raining.cards/common";
import { ServerOptions } from "@/server/server";
import { STATS_CONTEXT, StatsContext } from "@/meta/stats";
import { broadcastDTO } from "@/lobby/communication";
import { getClientContext } from "@/lobby/participant-context";

const CLOSE_ON_ERROR = new Set([
  ClientCommand.LOBBY_CREATE,
  ClientCommand.LOBBY_JOIN,
  ClientCommand.LOBBY_RECONNECT,
]);

export function useWebsocketServer(
  server: Server,
  { gameBoxes, origin }: ServerOptions,
  stats: StatsContext
) {
  const wsServer = createWebsocketServer();

  server.on("upgrade", (request: IncomingMessage, socket, head) => {
    wsServer.handleUpgrade(request, socket, head, (ws) => {
      if (
        origin !== undefined &&
        (request.headers.origin === undefined ||
          !origin.has(request.headers.origin))
      ) {
        if (isDebug()) {
          debug("Socket upgrade failed. Origin:", request.headers.origin);
        } else {
          console.debug(
            "[DEBUG] Socket upgrade failed. Origin:",
            request.headers.origin
          );
        }
        socket.destroy();
        return;
      }
      wsServer.emit("connection", ws, request);
    });
  });

  const gameBoxMap: GameBoxMap<GameIds> = Object.fromEntries(
    gameBoxes.map((box) => [box.id, box])
  );

  wsServer.on("connection", (socket) => {
    STATS_CONTEXT.set(socket, stats);
    stats.connectionCreated();

    socket.on("close", () => {
      stats.connectionClosed();
      clientDisconnectHandler(socket);
    });
    socket.on("message", (message) =>
      socketMessageRouter(gameBoxMap, socket, message)
    );
  });

  const { multicast } = useMulticast(wsServer);

  return { wsServer, multicast };
}

export function createWebsocketServer() {
  const wss = new WebSocket.Server({
    noServer: true,
    perMessageDeflate: true,
    maxPayload: 4096, // max. 4KiB per message
  });

  keepConnectionsAlive(wss);

  wss.on("error", console.error);
  wss.on("connection", (socket) => socket.on("error", console.error));

  return wss;
}

function getErrorMessage(
  err: unknown,
  clientAssertions: boolean
): S2CMessageServerError {
  if (isS2CMessageServerError(err)) {
    return err;
  } else if (err instanceof Error) {
    const isClientFault =
      clientAssertions && err.message.startsWith("Assertion Error: ");
    return {
      cmd: ServerCommand.ERROR,
      code: isClientFault
        ? S2CMessageErrorCode.GENERIC_CLIENT
        : S2CMessageErrorCode.GENERIC_SERVER,
      message: err.message,
    };
  }
  return {
    cmd: ServerCommand.ERROR,
    code: S2CMessageErrorCode.GENERIC_SERVER,
  };
}

function socketMessageRouter(
  gameBoxMap: GameBoxMap<GameIds>,
  socket: WebSocket,
  message: WebSocket.Data
) {
  debug("recv message", message);
  let msg: C2SMessageServer<GameIds, unknown>;
  try {
    const [cmd, payload] = parseClientCommand(message);
    try {
      msg = c2sFromDTO(socket, cmd, payload, gameBoxMap);
    } catch (err: unknown) {
      if (isDebug()) {
        console.warn("[WARN] Socket sent illegal payload.", message, err);
      } else {
        console.warn("[WARN] Socket sent illegal payload.", err);
      }
      socket.send(s2cMessageToDTO(getErrorMessage(err, true)));
      if (CLOSE_ON_ERROR.has(cmd)) {
        socket.close();
      }
      return;
    }
  } catch (err: unknown) {
    if (isDebug()) {
      console.warn("[WARN] Socket sent illegal command.", message, err);
    } else {
      console.warn("[WARN] Socket sent illegal command.", err);
    }
    socket.send(s2cMessageToDTO(getErrorMessage(err, true)));
    return;
  }
  try {
    c2sRouter(socket, msg);
  } catch (err: unknown) {
    if (isDebug()) {
      console.error("[ERROR] Message handler failed.", msg, err);
    } else {
      console.error("[ERROR] Message handler failed.", err);
    }
    const dto = s2cMessageToDTO(getErrorMessage(err, false));
    const ctx = getClientContext(socket);
    if (ctx === undefined) {
      socket.send(dto);
    } else {
      const participants = ctx.lobby.participants;
      broadcastDTO(participants, dto);
    }
  }
}
