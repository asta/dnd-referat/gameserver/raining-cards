import { defineMaterialFaceTypeGroup, idFactory } from "@raining.cards/server";
import { GameIds } from "@raining.cards/common";
import { useMonsterMaterials } from "@/material/monster-materials";
import { useTokenMaterials } from "@/material/token-materials";
import { useEquipmentMaterials } from "@/material/equipment-materials";
import { useHeroMaterials } from "@/material/hero-materials";

export type Materials = ReturnType<typeof useMaterials>;

export function useMaterials() {
  const idfFace = idFactory<GameIds["materialFaceType"]>("face");
  const idfMaterial = idFactory<GameIds["material"]>("material");
  const idfFaceGroup = idFactory<GameIds["materialFaceTypeGroup"]>(
    "face-type-group"
  );

  const monsterMaterials = useMonsterMaterials(
    idfFace,
    idfMaterial,
    idfFaceGroup
  );
  const tokenMaterials = useTokenMaterials(idfFace, idfMaterial);
  const equipmentMaterials = useEquipmentMaterials(
    idfFace,
    idfMaterial,
    monsterMaterials
  );
  const heroMaterials = useHeroMaterials(
    idfFace,
    idfMaterial,
    equipmentMaterials
  );

  const equipmentFaceGroup = defineMaterialFaceTypeGroup(
    idfFaceGroup("equipment"),
    equipmentMaterials.all.asList.map((it) => it.face)
  );

  const faceTypeGroups = [
    monsterMaterials.backdropFaceGroup,
    monsterMaterials.monsterFaceGroup,
    equipmentFaceGroup,
  ];

  return {
    ...monsterMaterials,
    ...tokenMaterials,
    equipment: equipmentMaterials,
    heroes: heroMaterials,

    equipmentFaceGroup,
    faceTypeGroups,
    faceTypes: [
      monsterMaterials.backdropFace,
      ...monsterMaterials.monsterFaceTypes,
      ...tokenMaterials.tokenFaceTypes,
      ...equipmentMaterials.all.asList.map((it) => it.face),
      ...Object.values(heroMaterials).map((it) => it.face),
    ],
  };
}
