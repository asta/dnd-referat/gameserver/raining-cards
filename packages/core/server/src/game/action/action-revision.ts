import { ActionRevisionDTO, AnyGameIds, GameIds } from "@raining.cards/common";
import { Client } from "@/lobby/participant.types";

const PLAYER_ACTION_GROUPS = new WeakMap<Client, ActionRevisionDTO<GameIds>>();

export function getLatestActionRevision<GI extends AnyGameIds = never>(
  client: Client
): ActionRevisionDTO<GI> {
  const latestActionGroups = PLAYER_ACTION_GROUPS.get(client);
  if (latestActionGroups !== undefined) {
    return latestActionGroups as ActionRevisionDTO<GI>;
  }
  const initActionGroups: ActionRevisionDTO<GameIds> = {
    groups: [],
    revision: -1,
  };
  PLAYER_ACTION_GROUPS.set(client, initActionGroups);
  return initActionGroups as ActionRevisionDTO<GI>;
}

export function setNextActionRevision<GI extends AnyGameIds>(
  client: Client,
  groups: GI["actionSchemaGroup"][]
): ActionRevisionDTO<GI> {
  const latestActionGroups = getLatestActionRevision(client);
  const nextActionGroups: ActionRevisionDTO<GI> = {
    groups,
    revision: (latestActionGroups?.revision ?? -1) + 1,
  };
  PLAYER_ACTION_GROUPS.set(
    client,
    nextActionGroups as ActionRevisionDTO<GameIds>
  );
  return nextActionGroups;
}
