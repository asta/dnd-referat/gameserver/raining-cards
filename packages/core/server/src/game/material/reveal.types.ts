import { Player } from "@/lobby/participant.types";
import { Opaque } from "ts-essentials";

export interface Reveal {
  global?: number;
  secret?: SecretRevealMap;
}

export type BakedReveal = Opaque<Reveal, "baked">;

export type SecretRevealMap = Partial<Record<Player["id"], number>>;
