import { UIMaterialPosition, UIZone } from "@/area/zone.types";
import { deepEqual, increaseMapValue, spliceCopy } from "@raining.cards/util";
import { debugClone } from "@/util/debug";
import {
  UIMaterialFace,
  UIMaterialPositionInChunk,
  UIMaterialView,
} from "@/game/material";
import { AnyGameIds } from "@raining.cards/common";

export function uiAccessMaterial<GI extends AnyGameIds>({
  zone,
  index,
}: UIMaterialPosition<GI>): UIMaterialPositionInChunk<GI> {
  for (let i = 0; i < zone.materialChunks.length; i++) {
    const chunk = zone.materialChunks[i];
    if (chunk.offset + chunk.count > index) {
      return { chunkIndex: i, chunk, index: index - chunk.offset };
    }
  }
  throw new Error(`Material not found (zone:${zone.type.id}, index:${index}).`);
}

export function uiDestroyMaterial<GI extends AnyGameIds>(
  zone: UIZone<GI>,
  { chunkIndex, chunk }: UIMaterialPositionInChunk<GI>
) {
  const chunks = zone.materialChunks;
  // reduce offset of trailing chunks
  for (let i = chunkIndex + 1; i < chunks.length; i++) {
    chunks[i].offset--;
  }

  if (chunk.count > 1) {
    // just decrease the count if chunk persists
    chunk.count--;
  } else {
    if (chunkIndex > 0 && chunkIndex + 1 < chunks.length) {
      const prev = chunks[chunkIndex - 1];
      const next = chunks[chunkIndex + 1];
      if (isSameView(prev, next)) {
        // remove chunk and unite surrounding chunks
        zone.materialChunks = spliceCopy(chunks, chunkIndex - 1, 3, [
          { ...prev, count: prev.count + next.count },
        ]);
        addZoneCount(zone, chunk, -1);
        return;
      }
    }
    // remove chunk
    zone.materialChunks = spliceCopy(chunks, chunkIndex, 1);
  }
  addZoneCount(zone, chunk, -1);
}

export function uiAddMaterial<GI extends AnyGameIds>(
  { zone, index }: UIMaterialPosition<GI>,
  material: UIMaterialView<GI>
): UIMaterialPositionInChunk<GI> {
  const chunks = zone.materialChunks;
  const { global, secret } = material;
  // todo de-duplicate with "add to previous chunk or prepend new chunk" below
  if (index === zone.materialFaceTypeCountTotal) {
    // add to last chunk or append new chunk
    let chunkIndex = chunks.length - 1;
    let chunk = chunks[chunkIndex];
    if (chunk !== undefined && isSameView(chunk, material)) {
      chunk.count++;
      addZoneCount(zone, material, 1);
      return { chunkIndex, chunk, index };
    }
    chunkIndex++;
    chunk = { count: 1, secret, global, offset: index };
    chunks.push(chunk);
    addZoneCount(zone, material, 1);
    return { chunkIndex, chunk, index };
  }

  // find chunk that currently contains the index
  let chunkIndex = 0;
  let chunk = chunks[chunkIndex];
  let capIdx = chunk.count;
  while (capIdx <= index) {
    chunk = chunks[++chunkIndex];
    capIdx += chunk.count;
  }
  // increase offset of trailing chunks
  for (let j = chunkIndex + 1; j < chunks.length; j++) {
    chunks[j].offset++;
  }

  if (isSameView(chunk, material)) {
    chunk.count++;
    addZoneCount(zone, material, 1);
    return { chunkIndex, chunk, index };
  }
  if (index === chunk.offset) {
    // add to previous chunk or prepend new chunk
    chunk.offset++;
    const prevIndex = chunkIndex - 1;
    const prev = chunks[prevIndex];
    if (prev !== undefined && isSameView(prev, material)) {
      prev.count++;
      addZoneCount(zone, material, 1);
      return { chunkIndex: prevIndex, chunk: prev, index };
    }
    chunk = { count: 1, secret, global, offset: index };
    zone.materialChunks = spliceCopy(chunks, chunkIndex, 0, [chunk]);
    addZoneCount(zone, material, 1);
    return { chunkIndex, chunk, index };
  }
  // split chunk and insert new chunk in between
  const newChunk = { count: 1, secret, global, offset: index };
  zone.materialChunks = spliceCopy(chunks, chunkIndex, 1, [
    { ...chunk, count: index - chunk.offset },
    newChunk,
    { ...chunk, count: capIdx - index, offset: index + 1 },
  ]);
  addZoneCount(zone, material, 1);
  return { chunkIndex: chunkIndex + 1, chunk: newChunk, index };
}

function addZoneCount<GI extends AnyGameIds>(
  zone: UIZone<GI>,
  { secret, global }: UIMaterialView<GI>,
  count = 1
) {
  // update zone counts
  const counts = zone.materialFaceTypeCount;
  if (secret !== undefined) {
    increaseMapValue(counts, secret.type, count);
    if (counts.get(secret.type) === 0) {
      counts.delete(secret.type);
    }
  }
  if (global !== secret) {
    increaseMapValue(counts, global.type, count);
    if (counts.get(global.type) === 0) {
      counts.delete(global.type);
    }
  }
  zone.materialFaceTypeCountTotal += count;
  console.debug("zone:updated", debugClone(zone));
  zone.emit("updated");
}

function isSameView<GI extends AnyGameIds>(
  a: UIMaterialView<GI>,
  b: UIMaterialView<GI>
): boolean {
  return (
    isSameFaceView(a.global, b.global) && isSameFaceView(a.secret, b.secret)
  );
}

function isSameFaceView<GI extends AnyGameIds>(
  a?: UIMaterialFace<GI>,
  b?: UIMaterialFace<GI>
): boolean {
  return a?.type === b?.type && deepEqual(a?.customMeta, b?.customMeta);
}
