import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
import TheLandingPage from "@/modules/games/TheLandingPage.vue";
import LobbyCreate from "@/modules/lobby/LobbyCreate.vue";
import TheLobbyPage from "@/modules/lobby/TheLobbyPage.vue";
import TheImprintPage from "@/modules/legal/TheImprintPage.vue";
import TheDataProtectionPage from "@/modules/legal/TheDataProtectionPage.vue";

const routes: Array<RouteRecordRaw> = [
  { path: "/", component: TheLandingPage },
  { path: "/:gameBoxId", component: LobbyCreate },
  { path: "/:gameBoxId/:lobbyId", component: TheLobbyPage },
  { path: "/legal/imprint", component: TheImprintPage },
  { path: "/legal/data-protection", component: TheDataProtectionPage },
];

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes,
});

export default router;
