import { ToBGame } from "@/ToB.game";
import { getLives, PlayerZonesMap } from "@/zone/zones";
import { ToBActionSchemaGroup, ToBActionSchema } from "@/action/action.types";
import { ToBGameIds } from "@/ToB.types";
import {
  accessOrThrow,
  ActionSchema,
  ClientAction,
  ClientActorMaterial,
  Player,
} from "@raining.cards/server";
import { ElementType } from "@raining.cards/common";
import { ToBZoneType } from "@/zone/zone-types";
import { ToBMaterialFaceType } from "@/material/face-types";

export function forfeitLifeSchema(
  game: ToBGame,
  { zonesSelf }: PlayerZonesMap
): ActionSchema<ToBGameIds> {
  return {
    id: ToBActionSchema.FORFEIT_LIFE,
    name: "Forfeit life",
    description: `Forfeit this life. It will be flipped so everyone knows that the role is out of the game. Flipped roles don't count towards a players roles anymore.`,
    origin: {
      type: ElementType.MATERIAL,
      position: [{ zone: zonesSelf[ToBZoneType.PLAYER_ROLES].id }],
      materialView: { public: [ToBMaterialFaceType.ROLE_BACKDROP] },
    },
    target: { type: ElementType.GLOBAL },
  };
}

export async function performForfeitLife(
  game: ToBGame,
  action: ClientAction<ToBGameIds>
) {
  const origin = action.origin as ClientActorMaterial<ToBGameIds>;
  game.transaction.revealGlobal(origin);
  const { faces } = accessOrThrow(origin).material;
  if (getLives(origin.zone).length) {
    game.logs.add(`${action.player.name} forfeits a ${faces[1].typeId}`);
  } else {
    game.logs.add(
      `${action.player.name} forfeits a ${faces[1].typeId} and is defeated`
    );
    game.transaction.playerDied(action.player);
  }
}

export async function forcePlayerForfeitLife(game: ToBGame, player: Player) {
  const zone = game.zones.byPlayer.get(player)![ToBZoneType.PLAYER_ROLES];
  const lives = getLives(zone);
  if (lives.length > 1) {
    const actionStackItem = game.actionStack.push(() => {
      game.transaction.addPlayerAction(
        player,
        ToBActionSchemaGroup.FORFEIT_LIFE
      );
    });
    await game.performTask();
    game.actionStack.popAssert(actionStackItem);
  } else if (lives.length === 1) {
    const pos = lives[0];
    game.transaction.revealGlobal(pos);
    game.logs.add(
      `${player.name} forfeits a ${pos.material.faces[1].typeId} and is defeated`
    );
    game.transaction.playerDied(player);
  }
}
