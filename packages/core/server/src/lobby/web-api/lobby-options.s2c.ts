import { s2cLobbyUpdateToDTO } from "@/lobby/web-api/lobby-update.s2c";

export const s2cLobbyOptionsToDTO = s2cLobbyUpdateToDTO;
