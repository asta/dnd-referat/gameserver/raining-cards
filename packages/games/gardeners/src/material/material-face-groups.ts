import { mapValues } from "@raining.cards/util";
import { MaterialFaces } from "@/material/material-faces";
import {
  DefinedMaterialFaceTypeGroup,
  defineMaterialFaceTypeGroup,
  idFactory,
} from "@raining.cards/server";
import { GameIds } from "@raining.cards/common";

export type MaterialFaceGroups = ReturnType<typeof useMaterialFaceGroups>;

export function useMaterialFaceGroups({
  tokens,
  byColor,
  clippers,
  rain,
}: MaterialFaces) {
  const idf = idFactory<GameIds["materialFaceTypeGroup"]>(
    "material-face-group"
  );

  const tokenGroup = defineMaterialFaceTypeGroup(
    idf("tokens"),
    Object.values(tokens)
  );
  const whiteGroup = defineMaterialFaceTypeGroup(idf("white"), [
    clippers,
    rain,
  ]);
  const byColorGroups = mapValues(byColor, (list, color) =>
    defineMaterialFaceTypeGroup(
      idf(`cards:${color}`),
      list.map((it) => it.faceDef)
    )
  );

  const asList: DefinedMaterialFaceTypeGroup[] = [
    tokenGroup,
    whiteGroup,
    ...Object.values(byColorGroups),
  ];

  return {
    tokens: tokenGroup,
    white: whiteGroup,
    byColor: byColorGroups,

    asList,
  };
}
