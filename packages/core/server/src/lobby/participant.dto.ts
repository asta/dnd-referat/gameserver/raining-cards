export interface Team {
  id: string;
  name: string;
  uiMeta: unknown; // unused as of now
}
