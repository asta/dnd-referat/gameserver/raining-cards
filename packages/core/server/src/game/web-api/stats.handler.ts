import { ServerResponse } from "http";
import { HTTP_HEADER_JSON_CONTENT_TYPE } from "@/api/http/common";
import { StatsContext } from "@/meta/stats";

export function statsHandler(res: ServerResponse, { collect }: StatsContext) {
  res.writeHead(200, HTTP_HEADER_JSON_CONTENT_TYPE);
  res.write(JSON.stringify(collect()));
  res.end();
}
