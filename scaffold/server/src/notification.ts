import * as path from "path";
import { readFile } from "fs/promises";
import {
  assertNotification,
  MulticastOptions,
  Notification,
  startServer,
} from "@raining.cards/server";
import { once, Subscription } from "@raining.cards/util";
import { assert } from "ts-essentials";

const FILE_NAME = path.join(__dirname, "..", "notifications.json");

let latestNotifications: Record<string, Notification> = {};
const hasSent = new WeakMap<Notification, Set<unknown>>();

export function startBroadcastInterval(
  multicast: ReturnType<typeof startServer>["multicast"],
  ms: number,
  options?: Omit<MulticastOptions, "filter">
): Subscription {
  const intervalId = setInterval(
    () => refreshNotifications(multicast, options).catch(console.error),
    ms
  );
  refreshNotifications(multicast, { live: true }).catch(console.error);
  return once(() => clearInterval(intervalId));
}

export async function refreshNotifications(
  multicast: ReturnType<typeof startServer>["multicast"],
  options?: Omit<MulticastOptions, "filter">
) {
  const notificationsList = mergeNotifications(await readNotifications());
  for (const notification of notificationsList) {
    broadcastToNewClients(notification, multicast, options);
  }
}

function mergeNotifications(
  list: ({ id: string } & Notification)[]
): Notification[] {
  const notifications: Record<string, Notification> = {};
  for (const item of list) {
    notifications[item.id] = latestNotifications[item.id] ?? item;
  }
  latestNotifications = notifications;
  return Object.values(notifications);
}

function broadcastToNewClients(
  it: Notification,
  multicast: ReturnType<typeof startServer>["multicast"],
  options?: Omit<MulticastOptions, "filter">
) {
  if (!hasSent.has(it)) {
    hasSent.set(it, new Set());
  }
  const sentTo = hasSent.get(it)!;
  multicast(it, {
    ...(options ?? {}),
    filter: (socket: unknown) => {
      if (!sentTo.has(socket)) {
        sentTo.add(socket);
        return true;
      }
      return false;
    },
  });
}

async function readNotifications(): Promise<({ id: string } & Notification)[]> {
  let content: string;
  try {
    content = (await readFile(FILE_NAME)).toString();
  } catch (err) {
    if (err.code !== "ENOENT") {
      console.warn(err);
    }
    return [];
  }
  const parsed: ({ id: string } & Notification)[] = [];
  try {
    let data = JSON.parse(content) as unknown;
    if (!Array.isArray(data)) {
      data = [data];
    }
    for (const item of data as unknown[]) {
      assertValidNotificationWithId(item);
      parsed.push(item);
    }
  } catch (err) {
    console.error("Failed to parse notification.json", err);
    return [];
  }
  return parsed;
}

function assertValidNotificationWithId(
  it: unknown
): asserts it is { id: string } & Notification {
  const { id, date } = it as any;
  assert(
    typeof id === "string",
    `Invalid value for Notification#id; expected string, got ${typeof id}`
  );
  if (typeof date === "string" || typeof date === "number") {
    (it as Notification).date = new Date(date);
  }
  assertNotification(it);
}
