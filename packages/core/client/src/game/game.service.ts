import {
  UIActionRevision,
  UIGame,
  UIGameLog,
  UIGameMeta,
  UIParticipants,
} from "@/game/game.types";
import {
  delay,
  Emitter,
  fromEntries,
  RESOLVED,
  RingBuffer,
  Subscription,
  unsubscribeAll,
  withEmitter,
} from "@raining.cards/util";
import {
  uiAccessMaterial,
  uiAddMaterial,
  uiDestroyMaterial,
} from "@/area/zone";
import {
  AnyGameIds,
  ClientActorDTO,
  ClientActorSingleDTO,
  ClientCommand,
  DeltaType,
  ELEMENT_TYPES,
  ELEMENT_TYPES_SINGLE,
  ElementType,
  GameIds,
  MaterialPositionDescriptorDTO,
  S2CMessageDTOActionReject,
  S2CMessageDTOGameLog,
  S2CMessageDTOUpdateClosure,
  S2CMessageDTOUpdateDelta,
  S2CMessageDTOUpdateKeyframe,
  ServerCommand,
  ZoneDTO,
} from "@raining.cards/common";
import {
  UIGameDeltaCreate,
  UIGameDeltaDestroy,
  UIGameDeltaFlush,
  UIGameDeltaStep,
  UIGameDeltaUpdate,
} from "@/game/game-delta.types";
import { UILobbyState } from "@/lobby/lobby.service";
import { SocketMessageLowLevel, UISocketState } from "@/lobby/socket.service";
import { UIMaterialPosition, UIZone } from "@/area/zone.types";
import {
  s2cGameUpdateClosureFromDTO,
  S2CMessageClientUpdateClosure,
} from "@/game/dto/s2c/update-closure";
import { zoneFromDTO, zonesToLookupMaps } from "@/game/dto/s2c/update-keyframe";
import { c2sGameActionToDTO } from "@/game/dto/c2s/game-action";
import { s2cGameLogFromDTO } from "@/game/dto/s2c/game-log";
import { s2cGameUpdateDeltaFromDTO } from "@/game/dto/s2c/update-delta";
import {
  s2cGameActionRejectFromDTO,
  S2CMessageClientActionReject,
} from "@/game/dto/s2c/action-reject";
import { UIActionSchema, UIActorTypeMap } from "@/game/action-schema.types";
import { actionRevisionFromDTO } from "@/game/dto/s2c/update-actions";
import { debugClone } from "@/util/debug";

type UIGameEventMap<GI extends AnyGameIds = GameIds> = {
  log: UIGameLog;
  keyframe: UIZone<GI>[];
  preDeltas: UIGameDeltaStep<GI>[];
  deltaBulkStep: {
    index: number;
    step: Exclude<UIGameDeltaStep<GI>, UIGameDeltaFlush>;
  };
  deltaFlush: UIGameDeltaStep<GI>;
  postDeltas: UIGameDeltaStep<GI>[];
  allowedActions: {
    current: UIActionRevision<GI>;
    preDeltas: UIActionRevision<GI>;
    postDeltas: UIActionRevision<GI>;
  };
  closure: S2CMessageClientUpdateClosure;
  actionReject: S2CMessageClientActionReject;
};

export interface UIGameState<GI extends AnyGameIds = GameIds>
  extends Emitter<UIGameEventMap<GI>> {
  game: UIGame<GI>;
  participants: UIParticipants;
  allowedActions: UIActionRevision<GI>;
  logs: RingBuffer<UIGameLog>;
  closure?: S2CMessageClientUpdateClosure;

  perform: (
    schema: UIActionSchema<GI>,
    actor: UIActorTypeMap<GI>[typeof schema["origin"]["type"]] extends never
      ? UIActorTypeMap<GI>[typeof schema["target"]["type"]]
      : UIActorTypeMap<GI>[typeof schema["origin"]["type"]],
    actee: UIActorTypeMap<GI>[typeof schema["origin"]["type"]] extends never
      ? never
      : UIActorTypeMap<GI>[typeof schema["target"]["type"]]
  ) => void;
  abortGame: () => void;
  destroy: () => void;
}

export interface UIGameStateRef<GI extends AnyGameIds = GameIds> {
  gameState?: UIGameState<GI>;
}

export async function uiCreateGame<GI extends AnyGameIds>(
  socketState: UISocketState<GI>,
  lobbyState: UILobbyState<GI>,
  meta: UIGameMeta<GI>
): Promise<UIGameState<GI>> {
  const participants = lobbyState.lobby.participants;
  const { payload } = await socketState.when(ServerCommand.UPDATE_KEYFRAME);
  const [actionsDTO, zoneDTOS] = payload as S2CMessageDTOUpdateKeyframe<GI>;
  const game: UIGame<GI> = {
    meta,
    zones: {
      asMap: {} as Record<GI["zone"], UIZone<GI>>,
      asTree: [],
      byType: {} as Record<GI["zoneType"], UIZone<GI>[]>,
    },
  };
  console.groupCollapsed("game:keyframe");
  applyS2CGameUpdateKeyframeDTO(zoneDTOS);
  console.info("game:board", debugClone(game.zones));
  console.groupEnd();
  const state = withEmitter<UIGameState<GI>, UIGameEventMap<GI>>({
    game,
    participants,
    allowedActions: actionRevisionFromDTO(meta, actionsDTO),
    logs: new RingBuffer<UIGameLog>(5),

    perform,
    abortGame,
    destroy,
  });
  const emptyActions: UIActionRevision<GI> = createEmptyActions();

  let updateSync = Promise.resolve();
  let subscriptions: Subscription[] = [
    socketState.on(ServerCommand.GAME_LOG, onGameLog),
    socketState.on(ServerCommand.ACTION_REJECT, onActionReject),
    socketState.on(ServerCommand.UPDATE_KEYFRAME, (it) => {
      updateSync = updateSync.then(() => onGameKeyframe(it));
      updateSync.catch(console.error);
    }),
    socketState.on(ServerCommand.UPDATE_DELTA, (it) => {
      updateSync = updateSync.then(() => onGameDeltas(it));
      updateSync.catch(console.error);
    }),
    socketState.on(ServerCommand.UPDATE_CLOSURE, (it) => {
      updateSync = updateSync.then(() => onGameClosure(it));
      updateSync.catch(console.error);
    }),
  ];

  return state;

  function applyS2CGameUpdateKeyframeDTO(zonesDTO: ZoneDTO<GI>[]) {
    const asTree = zonesDTO.map((it) =>
      zoneFromDTO(lobbyState.lobby.participants, game.meta, it)
    );
    game.zones = {
      asTree,
      ...zonesToLookupMaps(asTree, game.meta.zoneTypes),
    };
  }

  function onGameLog({ payload }: SocketMessageLowLevel) {
    const gameLog = s2cGameLogFromDTO(payload as S2CMessageDTOGameLog);
    state.logs.push(gameLog);
    console.debug("game:log", debugClone(gameLog));
    state.emit("log", gameLog);
  }

  function onActionReject({ payload }: SocketMessageLowLevel) {
    const actionReject = s2cGameActionRejectFromDTO(
      payload as S2CMessageDTOActionReject
    );
    console.info("game:actionReject", debugClone(actionReject));
    state.emit("actionReject", actionReject);
  }

  async function onGameKeyframe({ payload }: SocketMessageLowLevel) {
    const [actionsDTO, zonesDTO] = payload as S2CMessageDTOUpdateKeyframe<GI>;

    console.groupCollapsed("game:keyframe");
    applyS2CGameUpdateKeyframeDTO(zonesDTO);
    console.info("game:board", debugClone(game.zones));
    state.emit("keyframe", game.zones.asTree);
    console.groupEnd();

    if (actionsDTO !== null) {
      state.allowedActions = actionRevisionFromDTO(game.meta, actionsDTO);
      console.debug("game:allowedActions", debugClone(state.allowedActions));
      state.emit("allowedActions", {
        current: state.allowedActions,
        preDeltas: state.allowedActions,
        postDeltas: state.allowedActions,
      });
    }
  }

  async function onGameDeltas({ payload }: SocketMessageLowLevel) {
    const { allowedActions, steps } = s2cGameUpdateDeltaFromDTO(
      state,
      payload as S2CMessageDTOUpdateDelta<GI>
    );
    if (allowedActions !== undefined) {
      state.emit("allowedActions", {
        current: emptyActions,
        preDeltas: emptyActions,
        postDeltas: allowedActions,
      });
    }
    console.groupCollapsed("game:deltas", steps);
    state.emit("preDeltas", steps); // todo emit per flush bulk, with flush step attached

    let animationPromises: Promise<void>[] = [];
    let bulkIndex = 0;

    for (const step of steps) {
      if (step.type === DeltaType.FLUSH) {
        console.info("game:delta", debugClone(step));
        await Promise.allSettled(animationPromises);
        animationPromises = [];
        state.emit("deltaFlush", step);
        if (step.delay) {
          await delay(step.delay);
        }
        bulkIndex = 0;
      } else {
        await state.trigger("deltaBulkStep", { index: bulkIndex++, step });
        /* IF DEBUG */
        console.groupCollapsed("game:delta", debugClone(step));
        /* ELSE */
        console.info("game:delta", debugClone(step));
        /* ENDIF */
        switch (step.type) {
          case DeltaType.CREATE:
            animationPromises.push(performDeltaCreate(step));
            break;
          case DeltaType.UPDATE:
            animationPromises.push(performDeltaUpdate(step));
            break;
          case DeltaType.DESTROY:
            animationPromises.push(performDeltaDestroy(step));
            break;
        }
        /* region IF DEBUG */
        console.groupEnd();
        /* ENDIF endregion  */
      }
    }
    console.groupEnd();

    await Promise.allSettled(animationPromises);
    state.emit("postDeltas", steps);
    if (allowedActions !== undefined) {
      state.allowedActions = allowedActions;
      console.debug("game:allowedActions", debugClone(allowedActions));
      state.emit("allowedActions", {
        current: allowedActions,
        preDeltas: emptyActions,
        postDeltas: allowedActions,
      });
    }
  }

  async function onGameClosure({ payload }: SocketMessageLowLevel) {
    const closure = s2cGameUpdateClosureFromDTO(
      state,
      payload as S2CMessageDTOUpdateClosure
    );
    state.closure = closure;
    destroy();
    console.info("game:closure", debugClone(closure));
    state.emit("closure", closure);
  }

  function abortGame() {
    socketState.send(ClientCommand.GAME_ABORT);
  }

  function perform(
    schema: UIActionSchema<GI>,
    actor: UIActorTypeMap<GI>[typeof schema["origin"]["type"]] extends undefined
      ? UIActorTypeMap<GI>[typeof schema["target"]["type"]]
      : UIActorTypeMap<GI>[typeof schema["origin"]["type"]],
    actee: UIActorTypeMap<GI>[typeof schema["origin"]["type"]] extends undefined
      ? never
      : UIActorTypeMap<GI>[typeof schema["target"]["type"]]
  ) {
    const originActor =
      schema.origin.type === ElementType.GLOBAL ? undefined : actor;
    const targetActor =
      schema.origin.type === ElementType.GLOBAL
        ? (actor as typeof actee)
        : actee;

    socketState.send(
      ClientCommand.GAME_ACTION,
      c2sGameActionToDTO({
        actionsRevision: state.allowedActions.revision,
        action: {
          schema: schema.id,
          origin: actorToDTO(schema.origin.type, originActor),
          target: actorToDTO(
            schema.target.type,
            targetActor
          ) as ClientActorSingleDTO<GI>,
        },
      })
    );
  }

  function actorToDTO(
    type: ElementType,
    actor: UIActorTypeMap<GI>[typeof type]
  ): ClientActorDTO<GI> {
    if (type === ElementType.GLOBAL) {
      return { type: ElementType.GLOBAL };
    }
    if (type === ElementType.ZONE) {
      const { id } = actor as UIActorTypeMap<GI>[ElementType.ZONE];
      return { type: ElementType.ZONE, zone: id };
    }
    if (type === ElementType.MATERIAL) {
      const {
        index,
        zone: { id },
      } = actor as UIActorTypeMap<GI>[ElementType.MATERIAL];
      return { type: ElementType.MATERIAL, zone: id, index };
    }
    const materials = actor as UIActorTypeMap<GI>[ElementType.MATERIALS];
    return {
      type: ElementType.MATERIALS,
      materials: materials.map(materialPositionDescriptorToDTO),
    };
  }

  function materialPositionDescriptorToDTO({
    index,
    zone: { id },
  }: UIMaterialPosition<GI>): MaterialPositionDescriptorDTO<GI> {
    return { zone: id, index };
  }

  function destroy() {
    unsubscribeAll(subscriptions);
    subscriptions = [];
  }

  async function performDeltaCreate(
    delta: UIGameDeltaCreate<GI>
  ): Promise<void> {
    /* region IF DEBUG */
    const debugMaterial = debugClone(delta.materialView);
    const debugPosition = debugClone({
      zone: delta.zone,
      index: delta.index,
    });
    const debugChunksPre = debugClone(delta.zone.materialChunks);
    /* ENDIF endregion */
    const position = uiAddMaterial(delta, delta.materialView);
    /* region IF DEBUG */
    const debugChunksPost = debugClone(delta.zone.materialChunks);
    const debugAddedPosition = debugClone(position);
    console.debug("material added", {
      material: debugMaterial,
      position: debugPosition,
      "chunks:pre": debugChunksPre,
      "chunks:post": debugChunksPost,
      "position:return": debugAddedPosition,
    });
    /* ENDIF endregion */
    await delta.zone.trigger("created", { delta, position });
  }

  async function performDeltaUpdate(
    delta: UIGameDeltaUpdate<GI>
  ): Promise<void> {
    const { origin, target, newMaterialView } = delta;
    const dest = target
      ? { zone: target.zone ?? origin.zone, index: target.index }
      : { ...origin };
    const materialView = newMaterialView ?? uiAccessMaterial(origin).chunk;
    /* region IF DEBUG */
    const debugMaterial = debugClone(materialView);
    const debugTargetPosition = debugClone(dest);
    const debugTargetChunksPre = debugClone(dest.zone.materialChunks);
    /* ENDIF endregion */
    const targetPosition = uiAddMaterial(dest, materialView);
    /* region IF DEBUG */
    const debugTargetChunksPost = debugClone(dest.zone.materialChunks);
    const debugTargetAddedPosition = debugClone(targetPosition);
    console.debug("material added", {
      material: debugMaterial,
      position: debugTargetPosition,
      "chunks:pre": debugTargetChunksPre,
      "chunks:post": debugTargetChunksPost,
      "position:return": debugTargetAddedPosition,
    });
    /* ENDIF endregion */
    if (origin.zone === dest.zone && dest.index <= origin.index) {
      origin.index++;
    }
    const originPosition = uiAccessMaterial(origin);
    let promise: Promise<unknown> | undefined;
    if (dest.zone !== origin.zone) {
      promise = origin.zone.trigger("beforeDetach", {
        delta,
        from: originPosition,
        toZone: dest,
        toChunk: targetPosition,
      });
    }
    /* region IF DEBUG */
    const debugOriginPosition = debugClone(originPosition);
    const debugOriginChunksPre = debugClone(origin.zone.materialChunks);
    /* ENDIF endregion */
    uiDestroyMaterial(origin.zone, originPosition);
    /* region IF DEBUG */
    const debugOriginChunksPost = debugClone(origin.zone.materialChunks);
    console.debug("material destroyed", {
      position: debugOriginPosition,
      "chunks:pre": debugOriginChunksPre,
      "chunks:post": debugOriginChunksPost,
    });
    /* ENDIF endregion */
    await (promise ?? RESOLVED);
  }

  async function performDeltaDestroy(
    delta: UIGameDeltaDestroy<GI>
  ): Promise<void> {
    const position = uiAccessMaterial(delta);
    const promise = delta.zone.trigger("beforeDestroy", { delta, position });
    /* region IF DEBUG */
    const debugPosition = debugClone(position);
    const debugChunksPre = debugClone(delta.zone.materialChunks);
    /* ENDIF endregion */
    uiDestroyMaterial(delta.zone, position);
    /* region IF DEBUG */
    const debugChunksPost = debugClone(delta.zone.materialChunks);
    console.debug("material destroyed", {
      position: debugPosition,
      "chunks:pre": debugChunksPre,
      "chunks:post": debugChunksPost,
    });
    /* ENDIF endregion */
    await promise;
  }
}

function createEmptyActions<GI extends AnyGameIds>(): UIActionRevision<GI> {
  return {
    revision: -1,
    schemas: new Set(),
    ...fromEntries(
      ELEMENT_TYPES.map((type) => [
        type,
        fromEntries(ELEMENT_TYPES_SINGLE.map((it) => [it, []])),
      ])
    ),
  };
}
