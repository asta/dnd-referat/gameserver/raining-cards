export async function fetchJSON<T>(url: string): Promise<T> {
  const res = await fetch(url);
  if (res.status >= 200 && res.status < 300) {
    return (await res.json()) as T;
  }
  throw res;
}
