import { IdFactory } from "@raining.cards/server";
import { GameIds } from "@raining.cards/common";
import { MonsterMaterials } from "@/material/monster-materials";
import { useEquipmentFactories } from "@/material/equipment-factories";
import { withPartialLookup } from "@raining.cards/util";

export type EquipmentMaterials = ReturnType<typeof useEquipmentMaterials>;

export function useEquipmentMaterials(
  idfFace: IdFactory<GameIds["materialFaceType"]>,
  idfMaterial: IdFactory<GameIds["material"]>,
  monsterMaterials: MonsterMaterials
) {
  const {
    useEquipmentHP,
    useEquipmentKill,
    useEquipmentLifeSteal,
    useEquipmentBan,
    useEquipmentPotion,
  } = useEquipmentFactories(idfFace, idfMaterial, monsterMaterials);

  /*-------------------------------- HP boost --------------------------------*/

  const shield = useEquipmentHP("shield", {
    hp: 3,
    label: "Shield: +3HP",
    description: "A simple shield. Adds 3 health points.",
  });
  const armGuard = useEquipmentHP("arm-guard", {
    hp: 3,
    label: "Arm guard: +3HP",
    description: "An arm guard. Adds 3 health points.",
  });
  const loincloth = useEquipmentHP("loincloth", {
    hp: 4,
    label: "Loincloth: +4HP",
    description: "A suit of loincloth. Adds 4 health points.",
  });
  const armor = useEquipmentHP("armor", {
    hp: 5,
    label: "Armor: +5HP",
    description: "A suit of armor. Adds 5 health points.",
  });

  /*------------------------------ Species kill ------------------------------*/

  const sword = useEquipmentKill("sword", {
    defeats: [
      monsterMaterials.minion,
      monsterMaterials.skeleton,
      monsterMaterials.wolf,
    ],
    label: "Sword: Kills ≤3",
    description: "A good sword. Defeats monsters with a strength of 3 or less.",
  });
  const axe = useEquipmentKill("axe", {
    defeats: sword.data.defeats,
    label: "Axe: Kills ≤3",
    description: "A simple axe. Defeats monsters with a strength of 3 or less.",
  });
  const hammer = useEquipmentKill("hammer", {
    defeats: [monsterMaterials.golem],
    label: "Hammer: Kills Golems (5)",
    description:
      "A hammer to shatter apart golems. You know it from the Gothic game series.",
  });
  const deadBeGone = useEquipmentKill("deadBeGone", {
    defeats: [
      monsterMaterials.skeleton,
      monsterMaterials.vampire,
      monsterMaterials.ghost,
    ],
    label: "Dead be gone: Kills undead",
    description: `A guarding aura to banish undead creatures: ${monsterMaterials.skeletonFace.type.label}, ${monsterMaterials.vampireFace.type.label} and ${monsterMaterials.ghostFace.type.label}.`,
  });
  const cloak = useEquipmentKill("cloak", {
    defeats: [
      monsterMaterials.ghost,
      monsterMaterials.demon,
      monsterMaterials.dragon,
    ],
    label: "Cloak: Avoid ≥6",
    description:
      "A cloak of invisibility. Hides you from monsters with a strength of 6 or more.",
  });
  const lance = useEquipmentKill("lance", {
    defeats: [monsterMaterials.dragon],
    label: "Lance: Kills the Dragon (9)",
    description: "A long lance, well crafted to defeat dragons.",
  });

  /*-------------------------------- Special  --------------------------------*/

  const talisman = useEquipmentLifeSteal("talisman", {
    defeats: [monsterMaterials.minion, monsterMaterials.skeleton],
    label: "Talisman: Life-steal ≤2",
    description:
      "A weak talisman. Defeats monsters with a strength of 2 or less. Adds the defeated monsters' strength as health points.",
  });

  const ban = useEquipmentBan("ban", {
    label: "Ban (spell): Species",
    description:
      "A ban spell. Choose one species of monster before the hero enters the dungeon. That species is defeated automatically.",
  });
  const stinkBomb = useEquipmentBan("stink-bomb", {
    label: "Stink Bomb: Species",
    description:
      "A stink bomb. Choose one species of monster before the hero enters the dungeon. That species is defeated automatically.",
  });

  const healthPotion = useEquipmentPotion("health-potion", {
    hp: 3,
    label: "Health Potion",
    description:
      "One-time: When the hero dies, they come back to life with 3 health points.",
  });
  const ragePotion = useEquipmentPotion("rage-potion", {
    hp: 4,
    label: "Rage Potion",
    description:
      "One-time: When the hero dies, they come back to life with 4 health points.",
  });

  const asList = [
    shield,
    armGuard,
    loincloth,
    armor,

    sword,
    axe,
    hammer,
    deadBeGone,
    cloak,
    lance,

    talisman,
    ban,
    stinkBomb,
    healthPotion,
    ragePotion,
  ];

  return {
    shield,
    armGuard,
    loincloth,
    armor,

    sword,
    axe,
    hammer,
    deadBeGone,
    cloak,
    lance,

    talisman,
    ban,
    stinkBomb,
    healthPotion,
    ragePotion,

    all: withPartialLookup(asList, (it) => it.face.type.id),
  };
}
