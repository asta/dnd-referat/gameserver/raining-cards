export class RingBuffer<T> {
  private readonly items: T[];
  private readonly __len;

  private start = 0; // inclusive
  private end = 0; // exclusive
  private empty = true; // flag in case start === end

  constructor(size: number) {
    this.items = new Array(size);
    this.__len = size - 1;
  }

  *[Symbol.iterator]() {
    if (this.empty) {
      return;
    }
    let i = this.start;
    yield this.items[i];
    i = this.nextIndex(i);
    while (i !== this.end) {
      yield this.items[i];
      i = this.nextIndex(i);
    }
  }

  push(item: T) {
    this.items[this.end] = item;
    if (!this.empty && this.end === this.start) {
      this.end = this.start = this.nextIndex(this.end);
    } else {
      this.end = this.nextIndex(this.end);
    }
    this.empty = false;
  }

  private nextIndex(idx: number): number {
    return idx === this.__len ? 0 : idx + 1;
  }

  toJSON(): T[] {
    return Array.from(this);
  }
}
