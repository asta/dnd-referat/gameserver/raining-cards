[Base Repository](https://gitlab.com/xoria/raining-cards/)

# Raining Cards: Server and Game API

This package provides an API to start a _raining.cards_ instance as well as the
API for game development.

## Server instances

To start a new server, simply call the `startServer` function with the necessary
options and game box definitions of your choice.

## Game API

See dedicated
[README_GAME_DEV.md](https://gitlab.com/xoria/raining-cards/-/tree/master/README_GAME_DEV.md).
