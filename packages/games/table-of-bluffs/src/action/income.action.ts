import { ToBGame } from "@/ToB.game";
import { ToBActionCondition } from "@/condition";
import { creditMaterialFactory } from "@/material/material-factories";
import { ToBActionSchema } from "@/action/action.types";
import { ToBGameIds } from "@/ToB.types";
import {
  ActionSchema,
  ClientAction,
  ClientActorZone,
  last,
} from "@raining.cards/server";
import { ElementType } from "@raining.cards/common";
import { PlayerZonesMap } from "@/zone/zones";
import { ToBZoneType } from "@/zone/zone-types";
import { matchIt } from "@raining.cards/util";
import { CREDIT_SYMBOL } from "@/material/face-types";

export function incomeSchema(
  game: ToBGame,
  { zonesSelf }: PlayerZonesMap
): ActionSchema<ToBGameIds> {
  return {
    id: ToBActionSchema.INCOME,
    name: "Income",
    description: `Income: ${CREDIT_SYMBOL}1; cannot be blocked.`,
    origin: {
      type: ElementType.ZONE,
      zone: [zonesSelf[ToBZoneType.PLAYER_CREDITS].id],
    },
    target: { type: ElementType.GLOBAL },
    condition: matchIt(ToBActionCondition.CREDITS_MAX_9),
  };
}

export async function performIncome(
  game: ToBGame,
  action: ClientAction<ToBGameIds>
) {
  game.logs.add(`${action.player.name} collects ${CREDIT_SYMBOL}1 income`);
  const origin = action.origin as ClientActorZone<ToBGameIds>;
  game.transaction.create(creditMaterialFactory(game), last(origin.zone));
  game.turn.step();
}
