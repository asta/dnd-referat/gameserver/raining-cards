const apiUrl = import.meta.env.VITE_API_URL;
const envBaseUrl = import.meta.env.BASE_URL;

export const CONFIG = {
  apiUrl,
  wsUrl: import.meta.env.VITE_WS_URL ?? apiUrl.replace(/^http/, "ws"),
  baseUrl: envBaseUrl.startsWith("http")
    ? envBaseUrl
    : `${window.location.origin}${envBaseUrl}`,
};
