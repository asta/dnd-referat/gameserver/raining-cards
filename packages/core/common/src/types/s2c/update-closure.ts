export type S2CMessageDTOUpdateClosure = [
  /* endTime */ number,
  GameResultGroupDTO[]
];

export interface GameResultGroupDTO {
  name?: string;
  stats: GameResultPlayerStatsDTO[];
}

export interface GameResultPlayerStatsDTO {
  playerId: string;
  text?: string;
}
