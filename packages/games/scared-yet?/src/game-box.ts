import {
  commonPkgMeta,
  defineGameBox,
  Game,
  pushLobbyParticipants,
  useGameLogs,
  useTransaction,
  useTurnOrder,
} from "@raining.cards/server";
import * as pkg from "../package.json";
import { shuffle } from "@raining.cards/util";
import { useMaterials } from "@/material/materials";
import { useZones } from "@/zones";
import { useGameMeta } from "@/game-meta";
import { useActions } from "@/action/actions";
import { useGame } from "@/game";
import { useDealer } from "@/dealer";
import { useScore } from "@/score";
import { useDungeon } from "@/dungeon";

const description = `
Related game:
<ul>
<li><a href="https://iellousa.com/products/welcome-to-the-dungeon">Welcome to the Dungeon</a> (<a href="https://boardgamegeek.com/boardgame/150312">BGG</a>)</li>
</ul>

<p>In each turn a player can either add a drawn monster to the dungeon, weaken the
hero by removing some equipment, or fold.
The last player that did not fold must send the hero through the dungeon,
beating all the present monsters; one after the other.</p>

<p>A monster can be beaten by the specialties of the heroes' equipment. Otherwise,
the hero loses the monsters' strength in life points to defeat the monster.</p>

<p>If some player scores his second successful dungeon, they win the game.
If some player scores his second unsuccessful dungeon, they drop out of the
game. If all but one player are dropped out, the last remaining player wins the
game.</p>

Monsters:

<ul>
  <li>Dragon (Strength: 9)</li>
  <li>Demon (Strength: 7)</li>
  <li>Ghost (Strength: 6)</li>
  <li>2x Golem (Strength: 5)</li>
  <li>2x Vampire (Strength: 4)</li>
  <li>2x Wolf (Strength: 3)</li>
  <li>2x Skeleton (Strength: 2)</li>
  <li>2x Minion (Strength: 1)</li>
</ul>`;

export const SCARED_YET_BOX = defineGameBox(
  "scared-yet",
  { players: { min: 2, max: 4 } },
  { name: "Scared yet?", description, meta: commonPkgMeta(pkg) },
  (lobby): Game => {
    shuffle(lobby.participants.players);
    pushLobbyParticipants(lobby);

    const transaction = useTransaction(lobby.participants);
    const turnOrder = useTurnOrder(lobby.participants.players);
    const localTurnOrder = useTurnOrder(lobby.participants.players);
    const gameLogs = useGameLogs(lobby.participants);

    const materials = useMaterials();
    const zones = useZones(lobby.participants.players);
    const score = useScore(lobby.participants.players, turnOrder, gameLogs);
    const dealer = useDealer(transaction, materials, zones);
    const actions = useActions(
      transaction,
      localTurnOrder,
      gameLogs,
      materials,
      zones
    );
    const dungeon = useDungeon(materials, zones);

    const gameMeta = useGameMeta(materials, zones, actions);

    return useGame({
      participants: lobby.participants,
      turnOrder,
      localTurnOrder,
      transaction,
      gameLogs,
      dungeon,
      materials,
      zones,
      actions,
      gameMeta,
      dealer,
      score,
    });
  }
);
