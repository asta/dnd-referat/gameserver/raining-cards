# Raining Cards -- News

## [2021-04-15] - Lobby control and Who Knows Whom

The latest releases include an important bugfix within the server code that was
causing faulty game state deltas in some cases.

Feature-wise some control has been added to the lobby, such as player/spectator
changes and game abortion. In addition, a new _game_ has been added: _Who Knows
Whom_. It's not a game per se, but rather a companion app that targets to support
a custom variation of the [privacy](https://boardgamegeek.com/boardgame/38062).
In that target variant, the answers are not supposed to be secret and in
addition it may be allowed/enforced to answer for other players instead of only
for oneself.

Within the next days, there will probably be a release of the new game _Scared
yet?_, which is being developed on its own branch. This will be a game that is
related to [Welcome to the Dungeon](https://boardgamegeek.com/boardgame/150312),
with its own style.

The next tasks to focus on will probably be either lifecycle hooks (player join
/ disconnect) for games, or separation of the web UI from the scaffold.

Have fun and stay tuned!

_xoria_

## [2021-03-30] - Comfort API rework, nested zones and tooltips

In the previous weeks since the initial release, I've put some work into
reworking the comfort API. It now is clearly separated into three parts:
`define*`, `push*` and `use*`. While `push*` has stayed mostly unchanged, the
old `Transaction` and `ActionStack` have been reworked into `useTransaction` and
`useExecutorStack` accordingly. In addition, the new `define*` factory functions
have been introduced as a great new way to define and compose all the game
components.

While the new `define*` approach is definitely a great way to go for any game
development, we still support the traditional approach which allows for
statically defined aspects of the game and ID mapping by enum. This pattern can
still be seen within Table of Bluffs, while Gardeners is now the first game to
utilize great lengths of the new comfort API.

Aside of those major changes in the comfort API, the core has also seen two
significant changes:

1. Zones are now able to be nested. In conclusion, the obsolete model of domains
   has been removed.
1. Materials do not strictly have two faces anymore. They may now have any
   number of faces (at least one). Still, there is at most one _global_ face and
   one _secret_ face per client.

At last, the UI has also seen some minor improvement. As of now, action and
material descriptions are shown as tooltips on hover.

For the near future, I'm going to focus on lobby actions (change team, abort
game, etc.) and restrictions (player count and unique aliases).

## [2021-03-11] - Initial release

Today marks the initial release of the packages to the npm registry. As a
consequence, we will now properly adhere to [semver](https://semver.org/).

While the initial release will be `0.1.0` for all packages, we do not intend to
keep those version numbers in sync as other projects do. In fact, I regard that
practice an anti-pattern; it breaks with the expressiveness of major/minor
version increments of semver.
Instead, every package will have its own version and accompanying changelog.

While not being just a minimal viable product anymore, this project is still far
from done. The major features to date are:

- Two games are implemented:
  - Table of Bluffs: A game of bluffing and deceit.
  - Gardeners: A simple trick-taking game.
- A generic UI that may display any games, including
  - move animations for material position updates,
  - type-specific style definitions can be specified by the game backend, and
  - a game log to solve any kinds of confusion regarding "what happened?".
- Both client and server are kept in a library-like fashion to provide as much
  freedom to administrators as possible.
- The server API for game development provides moderate convenience with
  Transactions and an ActionStack.

We will also start hosting a _stable_ public instance, that is kept in sync with
the master branch, at [https://raining.cards](https://raining.cards).

A _less stable_ and more cutting edge public instance will be hosted at
[https://storm.raining.cards](https://storm.raining.cards).

Have fun and share the word!

_xoria_
