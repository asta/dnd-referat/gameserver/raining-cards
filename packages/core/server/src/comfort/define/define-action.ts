import {
  DefineActionSchema,
  defineActionSchemaToRaw,
  DefineActorSchema,
  DefineActorSchemaSingle,
} from "./define-action-schema.types";
import {
  ClientAction,
  ClientActorByElementType,
} from "@/game/action/client-action.types";
import { AnyGameIds, GameIds } from "@raining.cards/common";
import {
  ActionSchema,
  ActorSchemaByType,
} from "@/game/action/action-schema.types";

export interface DefinedAction<
  GI extends AnyGameIds = GameIds,
  Origin extends DefineActorSchema<GI> = DefineActorSchema<GI>,
  Target extends DefineActorSchemaSingle<GI> = DefineActorSchemaSingle<GI>
> {
  schema: ActionSchema<
    GI,
    ActorSchemaByType<GI>[Origin["type"]],
    ActorSchemaByType<GI>[Target["type"]]
  >;
  handler: <
    CA extends ClientAction<
      GI,
      ClientActorByElementType<GI>[Origin["type"]],
      ClientActorByElementType<GI>[Target["type"]]
    >
  >(
    action: CA
  ) => unknown;
}

export interface LooseDefinedAction<
  GI extends AnyGameIds = GameIds,
  CA extends ClientAction<GI> = ClientAction<GI>
> {
  schema: ActionSchema<GI>;
  handler: (action: CA) => unknown;
}

export function defineAction<
  GI extends AnyGameIds = GameIds,
  Origin extends DefineActorSchema<GI> = DefineActorSchema<GI>,
  Target extends DefineActorSchemaSingle<GI> = DefineActorSchemaSingle<GI>
>(
  schema: DefineActionSchema<GI, Origin, Target>,
  handler: (
    action: ClientAction<
      GI,
      ClientActorByElementType<GI>[Origin["type"]],
      ClientActorByElementType<GI>[Target["type"]]
    >
  ) => unknown
): DefinedAction<GI, Origin, Target> {
  return {
    // todo figure out correct type definitions for conversion
    schema: defineActionSchemaToRaw(schema) as DefinedAction<
      GI,
      Origin,
      Target
    >["schema"],
    handler,
  };
}
