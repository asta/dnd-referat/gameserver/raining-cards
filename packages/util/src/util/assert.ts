import { assert } from "ts-essentials";

export function assertArray(arr: unknown): unknown[] {
  assert(Array.isArray(arr));
  return arr;
}

export function assertLookup<T>(
  obj: Record<string, T>,
  key: unknown,
  msg?: string
): T {
  assert(typeof key === "string");
  assert(Reflect.has(obj, key), msg);
  return obj[key];
}
