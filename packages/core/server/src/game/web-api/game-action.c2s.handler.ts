import { C2SMessageServerGameAction } from "@/game/web-api/game-action.c2s";
import { commitNextClientAction, send } from "@/lobby/communication";
import * as WebSocket from "ws";
import { AnyGameIds, ServerCommand } from "@raining.cards/common";
import { getLatestActionRevision } from "@/game/action/action-revision";

export function c2sGameActionHandler<GI extends AnyGameIds>(
  socket: WebSocket,
  { actionsRevision, action, ctx: { client } }: C2SMessageServerGameAction<GI>
) {
  const latestActionGroups = getLatestActionRevision(client);
  if (latestActionGroups.revision !== actionsRevision) {
    return send(client, {
      cmd: ServerCommand.ACTION_REJECT,
      actionIdx: actionsRevision,
      errorMsg: "Game state changed.",
    });
  }
  // TODO validate action on game state
  commitNextClientAction(client, action);
}
