import { GameBoxDTO } from "@/types";

export type C2SMessageDTOLobbyCreate = [
  GameBoxDTO["id"],
  /* clientName */ string
];
