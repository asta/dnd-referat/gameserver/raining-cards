# Raining Cards -- Core development

## Requirements

- Up to date node.js and npm (at least version `7`). Recommended: Install
  node.js and npm via `nvm`.

## Setup

1. Run `npm install` within the project root.
1. Run the _ci/core_build.sh_ script to rebuild everything within _packages/_.
1. Within _scaffold/server/_:
   1. Create a _.env.local_ file, containing `RAINING_CARDS_ENV=development`.
   1. Run `npm run build`, then start the server with `npm run start`.
1. Within _scaffold/web/_:
   1. Run `npm run dev` to start the web service.

Now the local server should be available at
[http://localhost:3000](http://localhost:3000).

## Rebuild package

To keep it easy, just use the _ci/core_build.sh_ script again to rebuild after
code changes. Rebuild and restart the server for server-side changes and restart
the web dev service for client-side changes to take effect.

Alternatively, you can use the dependency tree below for more efficient rebuild:

```text
scaffold/server
  -> packages/games/_collections/all -> packages/games/*
  -> packages/core/server -> packages/core/common -> packages/core/util

scaffold/web
  -> packages/core/client -> packages/core/common -> packages/core/util
```

## Code style

We use prettier and eslint to sanitize code.
