import { assertContext, ClientContext } from "@/lobby/participant-context";
import * as WebSocket from "ws";
import { assert } from "ts-essentials";
import { AnyGameIds, ClientCommand, GameIds } from "@raining.cards/common";
import { Lobby } from "@/lobby/Lobby";
import { bakeCountRange } from "@raining.cards/util";
import { isDev } from "@/util";

export interface C2SMessageServerGameStart<GI extends AnyGameIds, GameProps> {
  cmd: ClientCommand.GAME_START;
  ctx: ClientContext<GI, GameProps>;
}

// type C2SMessageDTOGameStart = never;

export interface C2SMessageClientGameStart {
  cmd: ClientCommand.GAME_START;
}

export function c2sGameStartFromDTO(
  socket: WebSocket,
  data: unknown // C2SMessageDTOGameStart
): C2SMessageServerGameStart<GameIds, unknown> {
  assert(data === undefined);
  const ctx = assertContext(socket);
  if (!isDev()) {
    assertPlayerCount(ctx.lobby);
  }
  return { cmd: ClientCommand.GAME_START, ctx };
}

function assertPlayerCount<GI extends AnyGameIds, GameProps>(
  lobby: Lobby<GI, GameProps>
) {
  const counts = bakeCountRange(lobby.gameBox.lobby.players);
  const { players } = lobby.participants;
  assert(players.length >= counts.min, "Not enough players.");
  assert(players.length <= counts.max, "Too many players.");
}
