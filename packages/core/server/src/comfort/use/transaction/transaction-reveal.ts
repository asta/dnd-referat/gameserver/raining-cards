import { MaterialLocator } from "@/game/material/material.types";
import { Player } from "@/lobby/participant.types";
import { accessOrThrow, bakeReveal } from "@/game/material/material";
import { debug, debugLazy } from "@/util";
import { Reveal } from "@/game/material/reveal.types";
import { AnyGameIds } from "@raining.cards/common";
import { TransactionDirtyState } from "@/comfort/use/transaction/transaction-dirty-state";

export interface TransactionReveal<GI extends AnyGameIds> {
  flipGlobal: (pos: MaterialLocator<GI>) => void;
  revealGlobal: (pos: MaterialLocator<GI>, revealIdx?: number) => void;

  flipSecret: (pos: MaterialLocator<GI>, players: Player[]) => void;
  revealSecret: (
    pos: MaterialLocator<GI>,
    players: Player[],
    revealIdx?: number
  ) => void;
  clearSecret: (pos: MaterialLocator<GI>) => void;
}

export function useTransactionReveal<GI extends AnyGameIds>(
  dirtyState: TransactionDirtyState<GI>
): TransactionReveal<GI> {
  return {
    flipGlobal,
    revealGlobal,

    flipSecret,
    revealSecret,
    clearSecret,
  };

  function flipSecret(pos: MaterialLocator<GI>, players: Player[]) {
    const { material, position } = accessOrThrow(pos);
    const origin = bakeReveal(material, position.zone);
    debugLazy(() => [
      "Transaction: Flip secret reveal",
      {
        position,
        material,
        players: players.map((it) => it.name),
      },
    ]);
    const target: Reveal = {
      global: material.reveal.global,
      secret: { ...(material.reveal.secret || {}) },
    };
    for (const player of players) {
      const oldIdx = origin.secret?.[player.id] ?? origin.global ?? 0;
      target.secret![player.id] = oldIdx > 0 ? 0 : 1;
    }
    material.reveal = target;
    dirtyState.add(position, { material, reveal: { origin, target } });
  }

  function flipGlobal(pos: MaterialLocator<GI>) {
    const { material, position } = accessOrThrow(pos);
    debug("Transaction: Flip global reveal", { position, material });
    const origin = bakeReveal(material, position.zone);
    const oldIdx = origin.global ?? 0;
    const target: Reveal = {
      global: oldIdx > 0 ? 0 : 1,
      secret: material.reveal.secret,
    };
    material.reveal = target;
    dirtyState.add(position, { material, reveal: { origin, target } });
  }

  /*--------------------------------- Reveal ---------------------------------*/

  function revealSecret(
    pos: MaterialLocator<GI>,
    players: Player[],
    revealIdx?: number
  ) {
    const { material, position } = accessOrThrow(pos);
    if (revealIdx === undefined) {
      revealIdx = material.faces.length - 1;
    }
    debugLazy(() => [
      "Transaction: Set secret reveal",
      {
        position,
        material,
        players: players.map((it) => it.name),
        revealIdx,
      },
    ]);
    const origin = bakeReveal(material, position.zone);
    const target: Reveal = {
      global: material.reveal.global,
      secret: material.reveal.secret ? { ...material.reveal.secret } : {},
    };
    for (const player of players) {
      target.secret![player.id] = revealIdx;
    }
    material.reveal = target;
    dirtyState.add(position, { material, reveal: { origin, target } });
  }

  function clearSecret(pos: MaterialLocator<GI>) {
    const { material, position } = accessOrThrow(pos);
    debug("Transaction: Clear secret reveals", { position, material });
    const origin = bakeReveal(material, position.zone);
    const target: Reveal = { global: material.reveal.global };
    material.reveal = target;
    dirtyState.add(position, { material, reveal: { origin, target } });
  }

  function revealGlobal(pos: MaterialLocator<GI>, revealIdx?: number) {
    const { material, position } = accessOrThrow(pos);
    if (revealIdx === undefined) {
      revealIdx = material.faces.length - 1;
    }
    debug("Transaction: Update global reveal", {
      position,
      material,
      revealIdx,
    });
    const origin = bakeReveal(material, position.zone);
    const target: Reveal = {
      global: revealIdx,
      secret: material.reveal.secret,
    };
    material.reveal = target;
    dirtyState.add(position, { material, reveal: { origin, target } });
  }
}
