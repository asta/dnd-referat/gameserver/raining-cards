import { createClient } from "@/lobby/participant";
import { C2SMessageServerLobbyCreate } from "@/lobby/web-api/lobby-create.c2s";
import { createLobby } from "@/lobby/Lobby";
import { createClientContext } from "@/lobby/participant-context";
import { send } from "@/lobby/communication";
import * as WebSocket from "ws";
import { AnyGameIds, ServerCommand } from "@raining.cards/common";
import { STATS_CONTEXT } from "@/meta/stats";

export function c2sLobbyCreateHandler<GI extends AnyGameIds>(
  socket: WebSocket,
  { clientName, gameBox }: C2SMessageServerLobbyCreate<GI>
) {
  const client = createClient(socket, clientName);
  const lobby = createLobby(gameBox, client);
  createClientContext(socket, lobby, client);
  send(client, { cmd: ServerCommand.LOBBY_CREATED, lobby, client });
  STATS_CONTEXT.get(socket)!.lobbyCreated();
}
