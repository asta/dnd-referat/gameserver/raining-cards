import {
  DefinedMaterial,
  defineMaterial,
  defineMaterialFaceSingleton,
  defineMaterialFaceTypeGroup,
  defineMaterialSingleton,
  IdFactory,
  Material,
  MaterialFaceType,
} from "@raining.cards/server";
import { GameIds } from "@raining.cards/common";
import { times } from "@raining.cards/util";

export type MonsterMaterials = ReturnType<typeof useMonsterMaterials>;

export function useMonsterMaterials(
  idfFace: IdFactory<GameIds["materialFaceType"]>,
  idfMaterial: IdFactory<GameIds["material"]>,
  idfFaceGroup: IdFactory<GameIds["materialFaceTypeGroup"]>
) {
  const backdropFace = defineMaterialFaceSingleton(idfFace("backdrop"), {
    label: "[Monster]",
    description: "Some monster card.",
  });

  const minionFace = defineMaterialFaceSingleton(idfFace("monster:minion"), {
    label: "Minion (1)",
    description: "A lesser minion. Strength: 1.",
  });
  const minion = defineMaterial(idfMaterial("monster:minion"), [
    backdropFace,
    minionFace,
  ]);

  const skeletonFace = defineMaterialFaceSingleton(
    idfFace("monster:skeleton"),
    {
      label: "Skeleton (2)",
      description: "A weak skeleton. Strength: 2.",
    }
  );
  const skeleton = defineMaterial(idfMaterial("monster:skeleton"), [
    backdropFace,
    skeletonFace,
  ]);

  const wolfFace = defineMaterialFaceSingleton(idfFace("monster:wolf"), {
    label: "Wolf (3)",
    description: "A hungry wolf. Strength: 3.",
  });
  const wolf = defineMaterial(idfMaterial("monster:wolf"), [
    backdropFace,
    wolfFace,
  ]);

  const vampireFace = defineMaterialFaceSingleton(idfFace("monster:vampire"), {
    label: "Vampire (4)",
    description: "A thirsty vampire. Strength: 4.",
  });
  const vampire = defineMaterial(idfMaterial("monster:vampire"), [
    backdropFace,
    vampireFace,
  ]);

  const golemFace = defineMaterialFaceSingleton(idfFace("monster:golem"), {
    label: "Golem (5)",
    description: "A stone golem. Strength: 5.",
  });
  const golem = defineMaterial(idfMaterial("monster:golem"), [
    backdropFace,
    golemFace,
  ]);

  const ghostFace = defineMaterialFaceSingleton(idfFace("monster:ghost"), {
    label: "Ghost (6)",
    description: "It's a ghost. Strength: 6. Spooky!",
  });
  const ghost = defineMaterialSingleton(idfMaterial("monster:ghost"), [
    backdropFace,
    ghostFace,
  ]);

  const demonFace = defineMaterialFaceSingleton(idfFace("monster:demon"), {
    label: "Demon (7)",
    description: "It's an evil demon. Strength: 7. It seems to be angry.",
  });
  const demon = defineMaterialSingleton(idfMaterial("monster:demon"), [
    backdropFace,
    demonFace,
  ]);

  const dragonFace = defineMaterialFaceSingleton(idfFace("monster:dragon"), {
    label: "Dragon (9)",
    description: "It's a dragon. Strength: 9. Run!",
  });
  const dragon = defineMaterialSingleton(idfMaterial("monster:dragon"), [
    backdropFace,
    dragonFace,
  ]);

  const monster = [
    ...times(2, () => minion),
    ...times(2, () => skeleton),
    ...times(2, () => wolf),
    ...times(2, () => vampire),
    ...times(2, () => golem),
    ghost,
    demon,
    dragon,
  ];

  const backdropFaceGroup = defineMaterialFaceTypeGroup(
    idfFaceGroup("backdrop"),
    [backdropFace]
  );

  const monsterFaceTypes = [
    minionFace,
    skeletonFace,
    wolfFace,
    vampireFace,
    golemFace,
    ghostFace,
    demonFace,
    dragonFace,
  ];
  const monsterFaceGroup = defineMaterialFaceTypeGroup(
    idfFaceGroup("monster"),
    monsterFaceTypes
  );

  const strengthByType = new Map<MaterialFaceType, number>();
  strengthByType.set(minionFace.type, 1);
  strengthByType.set(skeletonFace.type, 2);
  strengthByType.set(wolfFace.type, 3);
  strengthByType.set(vampireFace.type, 4);
  strengthByType.set(golemFace.type, 5);
  strengthByType.set(ghostFace.type, 6);
  strengthByType.set(demonFace.type, 7);
  strengthByType.set(dragonFace.type, 9);

  return {
    monster,
    monsterFaceTypes,
    monsterFaceGroup,
    backdropFace,
    backdropFaceGroup,

    minionFace,
    skeletonFace,
    wolfFace,
    vampireFace,
    golemFace,
    ghostFace,
    demonFace,
    dragonFace,

    minion,
    skeleton,
    wolf,
    vampire,
    golem,
    ghost,
    demon,
    dragon,

    getMonsterTypeStrength,
    getMonsterStrength,
  };

  function getMonsterTypeStrength(it: MaterialFaceType): number {
    return strengthByType.get(it)!;
  }

  function getMonsterStrength(it: Material): number {
    return getMonsterTypeStrength((it as DefinedMaterial).faces[1].type);
  }
}
