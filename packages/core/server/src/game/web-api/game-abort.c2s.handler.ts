import { sendClosure } from "@/game/game";
import * as WebSocket from "ws";
import { LOBBY_BY_GAME } from "@/lobby/Lobby";
import { assert } from "ts-essentials";
import { STATS_CONTEXT } from "@/meta/stats";
import { AnyGameIds } from "@raining.cards/common";
import { Game } from "@/game/game.types";
import { C2SMessageServerGameAbort } from "@/game/web-api/game-abort.c2s";

export function c2sGameAbortHandler<GI extends AnyGameIds>(
  socket: WebSocket,
  { ctx: { lobby } }: C2SMessageServerGameAbort<GI>
) {
  assert(socket === lobby.creator.socket, "Not permitted.");
  const stats = STATS_CONTEXT.get(socket)!;
  const { gameBox } = lobby;
  const game = lobby.game!;

  sendClosure(game, [{ name: "Game aborted", stats: [] }]);
  LOBBY_BY_GAME.delete(game as Game);
  lobby.game = undefined;
  stats.gameAborted(gameBox);
}
