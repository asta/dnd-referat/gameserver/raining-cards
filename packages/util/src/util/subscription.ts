import { callEach, callIt, OnceFunction } from "@/util/function";

export type Subscription = OnceFunction<[], void>;

export const unsubscribeAll = callEach;
export const unsubscribe = callIt;
