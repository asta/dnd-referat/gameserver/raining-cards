# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.2.3] - 2021-04-22

### Fixed

- Do not set void action revision if `null` has been sent. Instead, just
  continue to use the previous revision.

## [0.2.2] - 2021-04-15

### Added

- GameAbort client command
- Default to `GameIds` generic parameter on types

## [0.2.1] - 2021-04-01

### Fixed

- Removed debugging console logs

## [0.2.0] - 2021-04-01

### Changed

- API changes of server communication
- Domains are superseded by nested zones
- Some type definitions have been renamed

## [0.1.0] - 2021-03-11

### Added

- Initial Release
