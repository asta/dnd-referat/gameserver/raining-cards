import {
  performStealBlock,
  stealBlockSchema,
} from "@/action/steal.block.action";
import {
  changeTeamSelfSchema,
  performChangeTeamSelf,
} from "@/action/change-team-self.action";
import { continueSchema, performContinue } from "@/action/continue.action";
import {
  collectTreasureSchema,
  performCollectTreasure,
} from "@/action/collect-treasure.action";
import { performTax, taxSchema } from "@/action/tax.action";
import {
  forfeitLifeSchema,
  performForfeitLife,
} from "@/action/forfeit-life.action";
import {
  exchangeRolesSchema,
  performExchangeRoles0,
  performExchangeRoles1,
} from "@/action/exchange-roles.action";
import { ToBActionHandler, ToBActionSchema } from "@/action/action.types";
import { incomeSchema, performIncome } from "@/action/income.action";
import {
  assassinateBlockSchema,
  performAssassinateBlock,
} from "@/action/assassinate.block.action";
import { doubtSchema, performDoubt } from "@/action/doubt.action";
import {
  foreignAidSchema,
  performForeignAid,
} from "@/action/foreign-aid.action";
import {
  changeTeamOtherSchema,
  performChangeTeamOther,
} from "@/action/change-team-other.action";
import { coupActionSchema, performCoup } from "@/action/coup.action";
import { performSteal, stealSchema } from "@/action/steal.action";
import {
  assassinateSchema,
  performAssassinate,
} from "@/action/assassinate.action";
import {
  foreignAidBlockSchema,
  performForeignAidBlock,
} from "@/action/foreign-aid.block.action";
import { ToBGameIds } from "@/ToB.types";
import { PlayerZonesMap, ToBZones } from "@/zone/zones";
import { ActionSchema, Player } from "@raining.cards/server";
import { ToBGame } from "@/ToB.game";

export const ACTIONS: Record<ToBActionSchema, ToBActionHandler> = {
  [ToBActionSchema.DOUBT]: performDoubt,
  [ToBActionSchema.COUP]: performCoup,
  [ToBActionSchema.FORFEIT_LIFE]: performForfeitLife,
  [ToBActionSchema.FOREIGN_AID]: performForeignAid,
  [ToBActionSchema.BLOCK_FOREIGN_AID_DUKE]: performForeignAidBlock,
  [ToBActionSchema.INCOME]: performIncome,
  [ToBActionSchema.TAX]: performTax,
  [ToBActionSchema.STEAL]: performSteal,
  [ToBActionSchema.CONTINUE]: performContinue,
  [ToBActionSchema.ASSASSINATE]: performAssassinate,
  [ToBActionSchema.BLOCK_ASSASSINATE_CONTESSA]: performAssassinateBlock,
  [ToBActionSchema.CHANGE_TEAM_SELF]: performChangeTeamSelf,
  [ToBActionSchema.CHANGE_TEAM_OTHER]: performChangeTeamOther,
  [ToBActionSchema.COLLECT_TREASURE]: performCollectTreasure,
  [ToBActionSchema.EXCHANGE_ROLES_0]: performExchangeRoles0,
  [ToBActionSchema.EXCHANGE_ROLES_1]: performExchangeRoles1,
  [ToBActionSchema.BLOCK_STEAL_AMBASSADOR]: performStealBlock,
  [ToBActionSchema.BLOCK_STEAL_CAPTAIN]: performStealBlock,
};

const ACTION_SCHEMA_FACTORIES: ((
  game: ToBGame,
  playerZones: PlayerZonesMap,
  zones: ToBZones,
  otherPlayers: Player[]
) => ActionSchema<ToBGameIds> | ActionSchema<ToBGameIds>[])[] = [
  doubtSchema,
  coupActionSchema,
  exchangeRolesSchema,
  forfeitLifeSchema,
  foreignAidSchema,
  foreignAidBlockSchema,
  incomeSchema,
  taxSchema,
  stealSchema,
  stealBlockSchema,
  continueSchema,
  assassinateSchema,
  assassinateBlockSchema,
  changeTeamSelfSchema,
  changeTeamOtherSchema,
  collectTreasureSchema,
];

export function actionSchemas(
  game: ToBGame,
  player: Player,
  otherPlayers: Player[],
  zones: ToBZones
): ActionSchema<ToBGameIds>[] {
  const otherPlayerZones = zones.ofOtherPlayers.get(player)!;
  const playerZones: PlayerZonesMap = {
    byPlayer: zones.byPlayer,
    zonesSelf: zones.byPlayer.get(player)!,
    zonesOthers: otherPlayerZones,
  };
  return ACTION_SCHEMA_FACTORIES.flatMap((factory) =>
    factory(game, playerZones, zones, otherPlayers)
  );
}
