import { assert } from "ts-essentials";
import Timeout = NodeJS.Timeout;
import { Client, Participants, Player } from "@/lobby/participant.types";
import { isActive } from "@/lobby/participant";
import { Alphabet, idFactory, debug } from "@/util";
import { assertLookup, getMax } from "@raining.cards/util";
import { Game, GameBox } from "@/game/game.types";
import { AnyGameIds, GameIds } from "@raining.cards/common";
import * as WebSocket from "ws";
import { STATS_CONTEXT, StatsContext } from "@/meta/stats";

export const LOBBIES: Record<string, Lobby> = {};

export const LOBBY_BY_GAME = new WeakMap<Game, Lobby>();

const lobbyIdFactories = [
  idFactory(null, { alphabet: Alphabet.HUMAN_UPPER, size: 5 }),
  idFactory(null, { alphabet: Alphabet.HUMAN_UPPER, size: 6 }),
  idFactory(null, { alphabet: Alphabet.URL, size: 6 }),
];

export interface Lobby<GI extends AnyGameIds = GameIds, GameProps = unknown> {
  id: string;
  creator: Client;
  participants: Participants;
  gameBox: GameBox<GI, GameProps>;
  gameProps: GameProps;
  game?: Game<GI>;
}

export function createLobby<GI extends AnyGameIds, GameProps>(
  gameBox: GameBox<GI, GameProps>,
  creator: Client
): Lobby<GI, GameProps> {
  let id: string;
  let attempt = 0;
  do {
    id = lobbyIdFactories[(attempt / 4) | 0](); // four attempts per id length
    attempt++;
  } while (Reflect.has(LOBBIES, id));

  const lobby: Lobby<GI, GameProps> = {
    id,
    creator,
    gameBox: gameBox,
    gameProps: (undefined as unknown) as GameProps,
    participants: {
      players: [],
      spectators: [],
      teams: gameBox.lobby.teams?.map(() => []),
      all: [],
    },
  };
  LOBBIES[id] = (lobby as unknown) as Lobby<GameIds, unknown>;
  joinLobby(lobby, creator);

  return lobby;
}

export function accessLobby<GI extends AnyGameIds>(
  game: Game<GI>
): Lobby<GI, unknown> {
  return (LOBBY_BY_GAME.get(game as Game)! as unknown) as Lobby<GI, unknown>;
}

export function joinLobby<GI extends AnyGameIds, GameProps>(
  lobby: Lobby<GI, GameProps>,
  client: Client
) {
  const { participants, game, gameBox } = lobby;
  participants.all.push(client);
  if (
    game != null ||
    participants.teams != null ||
    participants.players.length >= getMax(gameBox.lobby.players)
  ) {
    participants.spectators.push(client);
  } else {
    participants.players.push(client as Player);
  }
}

const DANGLING_LOBBIES = new WeakMap<Lobby<GameIds, unknown>, Timeout>();

export function checkLobbyCleanup<GI extends AnyGameIds, GameProps>(
  socket: WebSocket,
  lobby: Lobby<GI, GameProps>
) {
  const stats = STATS_CONTEXT.get(socket)!;
  const { players, spectators } = lobby.participants;
  if (!players.some(isActive) && !spectators.some(isActive)) {
    debug("lobby scheduled for clean-up in 30s", lobby.id);
    DANGLING_LOBBIES.set(
      (lobby as unknown) as Lobby<GameIds, unknown>,
      setTimeout(() => wipeLobby(lobby, stats), 30000)
    );
  }
}

export function abortLobbyCleanup<GI extends AnyGameIds, GameProps>(
  lobby: Lobby<GI, GameProps>
) {
  if (DANGLING_LOBBIES.has((lobby as unknown) as Lobby<GameIds, unknown>)) {
    debug("lobby clean-up disarmed", lobby.id);
    clearTimeout(
      DANGLING_LOBBIES.get((lobby as unknown) as Lobby<GameIds, unknown>)!
    );
    DANGLING_LOBBIES.delete((lobby as unknown) as Lobby<GameIds, unknown>);
  }
}

function wipeLobby<GI extends AnyGameIds, GameProps>(
  lobby: Lobby<GI, GameProps>,
  stats: StatsContext
) {
  debug("clean up lobby", lobby.id);
  Reflect.deleteProperty(LOBBIES, lobby.id);
  stats.lobbyCleaned();
  if (lobby.game !== undefined) {
    stats.gameAborted(lobby.gameBox);
  }
}

export function changeTeam<GI extends AnyGameIds, GameProps>(
  { participants: { teams, spectators, players } }: Lobby<GI, GameProps>,
  client: Client,
  targetTeamIdx: number | null
) {
  const clientPlayer = client as Player;
  if (targetTeamIdx === null) {
    const playersIdx = players.indexOf(clientPlayer);
    assert(playersIdx >= 0, "Already a spectator.");
    removeFromTeam(clientPlayer, teams);
    players.splice(playersIdx, 1);
    spectators.push(client);
  } else {
    if (targetTeamIdx !== 0) {
      assert(
        teams != null && targetTeamIdx >= 0 && targetTeamIdx <= teams.length,
        "Team not found."
      );
    }
    const spectatorsIdx = spectators.indexOf(client);
    if (spectatorsIdx >= 0) {
      spectators.splice(spectatorsIdx, 1);
      players.push(clientPlayer);
    } else {
      removeFromTeam(clientPlayer, teams);
    }
    if (teams != null) {
      teams[targetTeamIdx].push(clientPlayer);
    }
  }
}

function removeFromTeam(client: Player, teams?: Participants["teams"]) {
  let teamIdx: number;
  const team = teams?.find((it) => {
    teamIdx = it.indexOf(client);
    return teamIdx >= 0;
  });
  if (team != null) {
    team.splice(teamIdx!, 1);
  }
}

export function assertLobby(lobbyId: unknown): Lobby<GameIds, unknown> {
  return assertLookup(LOBBIES, lobbyId, "Lobby not found.");
}
