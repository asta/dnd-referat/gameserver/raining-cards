import { AnyGameIds, ElementType, GameIds } from "@raining.cards/common";
import {
  Condition,
  CountRange,
  pluckId,
  transformCondition,
} from "@raining.cards/util";
import { Zone, ZoneType } from "@/game/zone/zone.types";
import { DefineActionCondition } from "@/comfort";
import {
  MaterialFaceType,
  MaterialFaceTypeGroup,
} from "@/game/material/material.types";
import {
  ActionSchema,
  ActorSchemaByType,
  ActorSchemaMaterial,
  ActorSchemaMaterials,
  ActorSchemaZone,
  MaterialViewRestriction,
  PositionRestriction,
} from "@/game/action/action-schema.types";

export interface DefineActionSchema<
  GI extends AnyGameIds,
  Origin extends DefineActorSchema<GI> = DefineActorSchema<GI>,
  Target extends DefineActorSchemaSingle<GI> = DefineActorSchemaSingle<GI>
> {
  id: GI["actionSchema"];
  name: string;
  description?: string;
  origin: Origin;
  originCondition?: Condition<DefineActionCondition<GI>>;
  target: Target;
  targetCondition?: Condition<DefineActionCondition<GI>>;
  condition?: Condition<DefineActionCondition<GI>>;
}

export function defineActionSchemaToRaw<GI extends AnyGameIds>(
  it: DefineActionSchema<GI, DefineActorSchema<GI>, DefineActorSchemaSingle<GI>>
): ActionSchema<
  GI,
  ActorSchemaByType<GI>[typeof it["origin"]["type"]],
  ActorSchemaByType<GI>[typeof it["target"]["type"]]
> {
  return {
    id: it.id,
    name: it.name,
    description: it.description,
    origin: definedActorSchemaToRaw(it.origin),
    originCondition: it.originCondition
      ? transformCondition(it.originCondition, pluckId)
      : undefined,
    target: defineActorSchemaSingleToRaw(it.target),
    targetCondition: it.targetCondition
      ? transformCondition(it.targetCondition, pluckId)
      : undefined,
    condition: it.condition
      ? transformCondition(it.condition, pluckId)
      : undefined,
  };
}

export type DefineActorSchemaSingle<GI extends AnyGameIds> =
  | DefineActorSchemaGlobal
  | DefineActorSchemaZone<GI>
  | DefineActorSchemaMaterial<GI>;

function defineActorSchemaSingleToRaw<GI extends AnyGameIds>(
  it: DefineActorSchemaSingle<GI>
): ActorSchemaByType<GI>[typeof it["type"]] {
  switch (it.type) {
    case ElementType.GLOBAL:
      return it;
    case ElementType.ZONE:
      return defineActorSchemaZoneToRaw(it);
    case ElementType.MATERIAL:
      return defineActorSchemaMaterialToRaw(it);
  }
}

export type DefineActorSchema<GI extends AnyGameIds> =
  | DefineActorSchemaSingle<GI>
  | DefineActorSchemaMaterials<GI>;

function definedActorSchemaToRaw<GI extends AnyGameIds>(
  it: DefineActorSchema<GI>
): ActorSchemaByType<GI>[typeof it["type"]] {
  if (it.type === ElementType.MATERIALS) {
    return defineActorSchemaMaterialsToRaw(it);
  }
  return defineActorSchemaSingleToRaw(it);
}
// meta type, not used for runtime code
export interface DefineActorSchemaByElementType<
  GI extends AnyGameIds = GameIds
> {
  [ElementType.GLOBAL]: DefineActorSchemaGlobal;
  [ElementType.ZONE]: DefineActorSchemaZone<GI>;
  [ElementType.MATERIAL]: DefineActorSchemaMaterial<GI>;
  [ElementType.MATERIALS]: DefineActorSchemaMaterials<GI>;
}

export interface DefineActorSchemaGlobal {
  type: ElementType.GLOBAL;
}

export interface DefineActorSchemaZone<GI extends AnyGameIds = GameIds> {
  type: ElementType.ZONE;
  // restrict zone
  zone?: Zone<GI>[];
  zoneType?: ZoneType<GI>[];
}

function defineActorSchemaZoneToRaw<GI extends AnyGameIds>(
  it: DefineActorSchemaZone<GI>
): ActorSchemaZone<GI> {
  return {
    type: it.type,
    zone: it.zone?.map(pluckId),
    zoneType: it.zoneType?.map(pluckId),
  };
}

export interface DefineMaterialViewRestriction<
  GI extends AnyGameIds = GameIds
> {
  public?: MaterialFaceType<GI>[];
  publicGroups?: MaterialFaceTypeGroup<GI>[];
  secret?: MaterialFaceType<GI>[];
  secretGroups?: MaterialFaceTypeGroup<GI>[];
}

function defineMaterialViewRestrictionToRaw<GI extends AnyGameIds>(
  it: DefineMaterialViewRestriction<GI>
): MaterialViewRestriction<GI> {
  return {
    public: it.public?.map(pluckId),
    publicGroups: it.publicGroups?.map(pluckId),
    secret: it.secret?.map(pluckId),
    secretGroups: it.secretGroups?.map(pluckId),
  };
}

type DefinePositionRestriction<GI extends AnyGameIds> =
  | { zone?: never; zoneType: ZoneType<GI>; index?: number[] }
  | { zone: Zone<GI>; index?: number[] };

function definePositionRestrictionToRaw<GI extends AnyGameIds>(
  it: DefinePositionRestriction<GI>
): PositionRestriction<GI> {
  if (it.zone !== undefined) {
    return { zone: it.zone.id, index: it.index };
  }
  return { zoneType: it.zoneType.id, index: it.index };
}

function definePositionRestrictionsToRaw<GI extends AnyGameIds>(
  list: DefinePositionRestriction<GI>[]
): PositionRestriction<GI>[] {
  return list.map(definePositionRestrictionToRaw);
}

export interface DefineActorSchemaMaterial<GI extends AnyGameIds = GameIds> {
  type: ElementType.MATERIAL;
  // restrict position; negative indices count from top of pile
  position?: DefinePositionRestriction<GI>[];
  // restrict material to any of the listed types
  materialView?: DefineMaterialViewRestriction<GI>;
}

function defineActorSchemaMaterialToRaw<GI extends AnyGameIds>(
  it: DefineActorSchemaMaterial<GI>
): ActorSchemaMaterial<GI> {
  return {
    type: it.type,
    materialView: it.materialView
      ? defineMaterialViewRestrictionToRaw(it.materialView)
      : undefined,
    position: it.position
      ? definePositionRestrictionsToRaw(it.position)
      : undefined,
  };
}

export interface DefineActorSchemaMaterials<GI extends AnyGameIds> {
  type: ElementType.MATERIALS;
  // restrict count
  count?: CountRange;
  // restrict position (e.g. [[A], [B, C]] means either materials within
  // position A or materials within positions B and C.
  position?: DefinePositionRestriction<GI>[][];
  // restrict material to any of the listed types
  materialView?: DefineMaterialViewRestriction<GI>;
}

function defineActorSchemaMaterialsToRaw<GI extends AnyGameIds>(
  it: DefineActorSchemaMaterials<GI>
): ActorSchemaMaterials<GI> {
  return {
    type: it.type,
    count: it.count,
    position: it.position?.map(definePositionRestrictionsToRaw),
    materialView: it.materialView
      ? defineMaterialViewRestrictionToRaw(it.materialView)
      : undefined,
  };
}
