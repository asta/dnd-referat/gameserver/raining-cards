import {
  ActionConditionItemDTO,
  ActionSchemaDTO,
  ActorSchemaDTO,
  ActorSchemaSingleDTO,
  AnyGameIds,
  ElementType,
  GameIds,
  MaterialViewRestrictionDTO,
  PositionRestrictionDTO,
  S2CMessageDTOGameMeta,
  ZoneDTO,
  ZoneTypeDTO,
} from "@raining.cards/common";
import { UILobbyState } from "@/lobby/lobby.service";
import {
  bakeCountRange,
  Condition,
  evaluate,
  fromEntries,
  mergeSets,
  stubTrue,
  transformCondition,
  transformConditionUnwrap,
} from "@raining.cards/util";
import {
  UIActionSchema,
  UIActorSchema,
  UIActorSchemaMap,
  UIActorSchemaSingle,
  UIActorTypeMap,
  UIMaterialViewRestriction,
  UIPositionRestriction,
} from "@/game/action-schema.types";
import {
  UIGame,
  UIGameMeta,
  UIMaterialFaceType,
  UIMaterialPositionCount,
  UIParticipants,
} from "@/game/game.types";
import { UIZoneType } from "@/area/zone.types";
import {
  uiEvaluateMaterials,
  uiEvaluateMaterialsOnZone,
} from "@/game/condition-impl.ui";
import { uiAccessMaterial } from "@/area/zone";
import { UIGameStateRef } from "@/game/game.service";

export type S2CMessageClientGameMeta<
  GI extends AnyGameIds = GameIds
> = UIGameMeta<GI>;

export function s2cGameMetaFromDTO<GI extends AnyGameIds>(
  gameStateRef: UIGameStateRef<GI>,
  { lobby: { participants } }: UILobbyState<GI>,
  dto: S2CMessageDTOGameMeta<GI>
): S2CMessageClientGameMeta<GI> {
  const zoneTypes = fromEntries(
    dto.zoneTypes.map((it) => [it.id, zoneTypeFromDTO<GI>(participants, it)])
  );

  const materialFaceTypes: Record<
    GI["materialFaceType"],
    UIMaterialFaceType<GI>
  > = fromEntries(dto.materialFaceTypes.map((it) => [it.id, it]));

  const materialFaceTypeGroups = fromEntries(
    dto.materialFaceTypeGroups.map(({ id, faceIds }) => [
      id,
      new Set<UIMaterialFaceType<GI>>(
        faceIds.map((it) => materialFaceTypes[it])
      ),
    ])
  );

  const actionConditions = fromEntries(
    dto.actionConditions.map(({ id, condition }) => [
      id,
      actionConditionFromDTO(
        zoneTypes,
        materialFaceTypes,
        materialFaceTypeGroups,
        condition
      ),
    ])
  );

  const actionSchemas = fromEntries(
    dto.actionSchemas.map((it) => [
      it.id,
      actionSchemaFromDTO(
        gameStateRef,
        zoneTypes,
        materialFaceTypes,
        materialFaceTypeGroups,
        actionConditions,
        it
      ),
    ])
  );

  const actionSchemaGroups = fromEntries(
    dto.actionSchemaGroups.map((it) => [
      it.id,
      it.schemaIds.flatMap((id) => actionSchemas[id]),
    ])
  );

  return {
    actionSchemas,
    actionSchemaGroups,
    actionConditions,

    materialFaceTypes,
    materialFaceTypeGroups,

    zoneTypes,
  };
}

function zoneTypeFromDTO<GI extends AnyGameIds>(
  participants: UIParticipants,
  dto: ZoneTypeDTO<GI>
): UIZoneType<GI> {
  return {
    id: dto.id,
    ...zoneTypeMetaFromDTO(participants, dto),
  };
}

export function zoneTypeMetaFromDTO<GI extends AnyGameIds>(
  { players }: UIParticipants,
  {
    label,
    description,
    relatedTo,
    uiMeta,
  }: Exclude<ZoneDTO<GI>["customMeta"], undefined>
) {
  return {
    label,
    description,
    relatedTo: relatedTo?.map((id) => players.asLookup[id]),
    uiMeta,
  };
}

function actionConditionFromDTO<GI extends AnyGameIds>(
  zoneTypeLookup: Record<GI["zoneType"], UIZoneType<GI>>,
  materialFaceTypeLookup: Record<
    GI["materialFaceType"],
    UIMaterialFaceType<GI>
  >,
  materialFaceTypeGroupLookup: Record<
    GI["materialFaceTypeGroup"],
    Set<UIMaterialFaceType<GI>>
  >,
  dto: Condition<ActionConditionItemDTO<GI>>
): Condition<UIMaterialPositionCount<GI>> {
  return transformCondition(
    dto,
    (it): UIMaterialPositionCount<GI> => {
      const materialFaceTypes = mergeSets<UIMaterialFaceType<GI>>(
        new Set(
          it.materialFaceTypeIds?.map((it) => materialFaceTypeLookup[it]) ?? []
        ),
        ...(it.materialFaceTypeGroupIds?.map(
          (it) => materialFaceTypeGroupLookup[it]
        ) ?? [])
      );
      return {
        count: bakeCountRange(it.count),
        materialFaceTypes: Array.from(materialFaceTypes),
        zoneTypes: it.zoneTypeIds?.map((it) => zoneTypeLookup[it]) ?? [],
        zones: it.zoneIds ?? [],
      };
    }
  );
}

function actionSchemaFromDTO<GI extends AnyGameIds>(
  gameStateRef: UIGameStateRef<GI>,
  zoneTypeLookup: Record<GI["zoneType"], UIZoneType<GI>>,
  materialFaces: UIGame<GI>["meta"]["materialFaceTypes"],
  materialFaceGroups: UIGame<GI>["meta"]["materialFaceTypeGroups"],
  conditionsLookup: Record<
    GI["actionCondition"],
    Condition<UIMaterialPositionCount<GI>>
  >,
  dto: ActionSchemaDTO<GI>
): UIActionSchema<GI> {
  const condition =
    dto.condition &&
    transformConditionUnwrap(dto.condition, (it) => conditionsLookup[it]);
  const boundUIEvaluateMaterials = (it: UIMaterialPositionCount<GI>) =>
    uiEvaluateMaterials(gameStateRef, it);
  const origin = actorSchemaFromDTO(
    zoneTypeLookup,
    materialFaces,
    materialFaceGroups,
    dto.origin
  );
  const target = actorSchemaSingleFromDTO(
    zoneTypeLookup,
    materialFaces,
    materialFaceGroups,
    dto.target
  );
  const originCondition =
    dto.originCondition &&
    transformConditionUnwrap(dto.originCondition, (id) => conditionsLookup[id]);
  const targetCondition =
    dto.targetCondition &&
    transformConditionUnwrap(dto.targetCondition, (id) => conditionsLookup[id]);
  return {
    id: dto.id,
    name: dto.name,
    description: dto.description,
    check: condition
      ? // todo add cache
        () => evaluate(boundUIEvaluateMaterials, condition)
      : stubTrue,
    origin,
    checkOrigin: (it: UIActorTypeMap<GI>[typeof origin.type]) =>
      checkActionSchemaActor(origin, originCondition, it),
    target,
    checkTarget: (it: UIActorTypeMap<GI>[typeof target.type]) =>
      checkActionSchemaActor(target, targetCondition, it),
  };
}

// TODO unwrap type-checks into function-mapping (performance improvement)
function checkActionSchemaActor<GI extends AnyGameIds>(
  schema: UIActorSchema<GI>,
  condition: Condition<UIMaterialPositionCount<GI>> | undefined,
  actor: UIActorTypeMap<GI>[typeof schema["type"]]
): boolean {
  if (schema.type === ElementType.ZONE) {
    const zone = actor as UIActorTypeMap<GI>[ElementType.ZONE];
    // validate position
    if (!schema.zoneIds.has(zone.id) && !schema.zoneType.has(zone.type)) {
      return false;
    }
    if (condition === undefined) {
      return true;
    }
    // validate actor condition
    // todo add cache
    return evaluate((it) => uiEvaluateMaterialsOnZone(zone, it), condition);
  }

  if (schema.type === ElementType.MATERIAL) {
    const {
      materialView,
    } = schema as UIActorSchemaMap<GI>[ElementType.MATERIAL];
    const { zone, index } = actor as UIActorTypeMap<GI>[ElementType.MATERIAL];
    // validate position
    if (
      schema.position &&
      // todo validate index
      !schema.position.some((it) => {
        if (it.zone !== undefined) {
          return it.zone === zone.id;
        }
        return it.zoneType!.id === zone.type.id;
      })
    ) {
      return false;
    }
    // validate material type
    if (materialView === undefined) {
      return true;
    }
    const { public: publicTypes, secret: secretTypes } = materialView;
    const { global, secret } = uiAccessMaterial({ zone, index }).chunk;
    return (
      (publicTypes === undefined || publicTypes.has(global.type)) &&
      (secretTypes === undefined ||
        (secret !== undefined && secretTypes.has(secret.type)))
    );
  }

  // TODO validate ElementType.MATERIALS
  return true;
}

function actorSchemaSingleFromDTO<GI extends AnyGameIds>(
  zoneTypeLookup: Record<GI["zoneType"], UIZoneType<GI>>,
  materialFaces: UIGame<GI>["meta"]["materialFaceTypes"],
  materialFaceGroups: UIGame<GI>["meta"]["materialFaceTypeGroups"],
  dto: ActorSchemaSingleDTO<GI>
): UIActorSchemaSingle<GI> {
  switch (dto.type) {
    case ElementType.GLOBAL:
      return dto;
    case ElementType.ZONE:
      return {
        type: ElementType.ZONE,
        zoneIds: new Set(dto.zone),
        zoneType: new Set(dto.zoneType?.map((id) => zoneTypeLookup[id]) ?? []),
      };
    case ElementType.MATERIAL:
      return {
        type: ElementType.MATERIAL,
        materialView:
          dto.materialView &&
          materialViewRestrictionFromDTO(
            materialFaces,
            materialFaceGroups,
            dto.materialView
          ),
        position: (dto.position ?? []).map((pos) =>
          materialLocationRestrictionFromDTO(zoneTypeLookup, pos)
        ),
      };
  }
}

function materialViewRestrictionFromDTO<GI extends AnyGameIds>(
  materialFaces: UIGame<GI>["meta"]["materialFaceTypes"],
  materialFaceGroups: UIGame<GI>["meta"]["materialFaceTypeGroups"],
  dto: MaterialViewRestrictionDTO<GI>
): UIMaterialViewRestriction<GI> {
  return {
    public: materialViewRestrictionItemFromDTO(
      materialFaces,
      materialFaceGroups,
      dto.public,
      dto.publicGroups
    ),
    secret: materialViewRestrictionItemFromDTO(
      materialFaces,
      materialFaceGroups,
      dto.secret,
      dto.secretGroups
    ),
  };
}

function materialViewRestrictionItemFromDTO<GI extends AnyGameIds>(
  materialFaces: UIGame<GI>["meta"]["materialFaceTypes"],
  materialFaceGroups: UIGame<GI>["meta"]["materialFaceTypeGroups"],
  types?: MaterialViewRestrictionDTO<GI>["public"],
  groups?: MaterialViewRestrictionDTO<GI>["publicGroups"]
): Set<UIMaterialFaceType<GI>> | undefined {
  const pub = types ? new Set(types.map((id) => materialFaces[id])) : undefined;
  if (groups === undefined) {
    return pub;
  }
  return mergeSets(
    pub ?? new Set(),
    ...groups.map((it) => materialFaceGroups[it])
  );
}

function materialLocationRestrictionFromDTO<GI extends AnyGameIds>(
  zoneTypeLookup: Record<GI["zoneType"], UIZoneType<GI>>,
  dto: PositionRestrictionDTO<GI>
): UIPositionRestriction<GI> {
  if (dto.zone !== undefined) {
    return { zone: dto.zone, index: dto.index };
  }
  return { zoneType: zoneTypeLookup[dto.zoneType!], index: dto.index };
}

function actorSchemaFromDTO<GI extends AnyGameIds>(
  zoneTypeLookup: Record<GI["zoneType"], UIZoneType<GI>>,
  materialFaces: UIGame<GI>["meta"]["materialFaceTypes"],
  materialFaceGroups: UIGame<GI>["meta"]["materialFaceTypeGroups"],
  dto: ActorSchemaDTO<GI>
): UIActorSchema<GI> {
  if (dto.type !== ElementType.MATERIALS) {
    return actorSchemaSingleFromDTO(
      zoneTypeLookup,
      materialFaces,
      materialFaceGroups,
      dto
    );
  }
  const posFromDTO = (pos: PositionRestrictionDTO<GI>) =>
    materialLocationRestrictionFromDTO(zoneTypeLookup, pos);
  return {
    type: ElementType.MATERIALS,
    count: bakeCountRange(dto.count),
    position: dto.position
      ? dto.position.map((positions) => positions.map(posFromDTO))
      : undefined,
    materialView: dto.materialView
      ? materialViewRestrictionFromDTO(
          materialFaces,
          materialFaceGroups,
          dto.materialView
        )
      : undefined,
  };
}
