# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.2.1] - 2021-04-15

### Added

- GameAbort client command

## [0.2.0] - 2021-04-01

### Changed

- Renamed various types and attributes to better reflect the new naming schema. See `GameIds` definition for hints of the new naming.
- Replaced domains in favor of nested zones
- GameIds now uses specific strings to get typescript errors on wrong use. AnyGameIds has been introduced for `string` types.

### Added

- ZoneType information

### Removed

- `GameResultGroupDTO#id`

## [0.1.0] - 2021-03-11

### Added

- Initial Release
