#!/usr/bin/env bash

set -e

CI_DIR="$(cd "$(dirname "$0")" && pwd)"
BASE_DIR="$(dirname "$CI_DIR")"

echo "$BASE_DIR/packages/util"

echo "$BASE_DIR/packages/core/common"
echo "$BASE_DIR/packages/core/server"
echo "$BASE_DIR/packages/core/client"

for it in "$BASE_DIR/packages/games"/*; do
  if [ -d "$it" ] && [ "$(basename "$it")" != "_collections" ]; then
    echo "$it"
  fi
done

for it in "$BASE_DIR/packages/games/_collections"/*; do
  echo "$it"
done
