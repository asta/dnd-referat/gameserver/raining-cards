export type Booly = unknown;

export type Obj = Record<string | number | symbol, unknown>;

export type Predicate<Args extends unknown[]> = (...args: Args) => Booly;
