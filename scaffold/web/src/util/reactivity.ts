import { reactive, Ref, ref } from "vue";

/**
 * According to https://github.com/vuejs/vue-next/issues/1324#issuecomment-641150163
 * it is recommended to cast the return value of {@link ref} when using generic
 * types. This is a wrapper function for such cases, so we can easily detect
 * all occurrences and remove, when fixed upstream.
 *
 * @param initialValue The initial state.
 */
export function castedRef<T>(initialValue: T): Ref<T> {
  return ref(initialValue) as Ref<T>;
}

// eslint-disable-next-line @typescript-eslint/ban-types
export function castedReactive<T extends object>(initialValue: T): T {
  return reactive<T>(initialValue) as T;
}
