export enum Environment {
  DEVELOPMENT = "development",
  TEST = "test",
  PRODUCTION = "production",
}

const ENVIRONMENTS = new Set(Object.values(Environment));

export function isEnvironment(it: any): it is Environment {
  return ENVIRONMENTS.has(it);
}
