import {
  s2cGameMetaToDTO,
  S2CMessageServerGameMeta,
} from "@/game/web-api/game-meta.s2c";
import {
  s2cGameUpdateClosureToDTO,
  s2cGameUpdateDeltaToDTO,
  s2cGameUpdateKeyframeToDTO,
  S2CMessageServerUpdateClosure,
  S2CMessageServerUpdateDelta,
  S2CMessageServerUpdateKeyframe,
} from "@/game/web-api/game-update.s2c";
import {
  s2cGameLogToDTO,
  S2CMessageServerGameLog,
} from "@/game/web-api/game-log.s2c";
import { S2CMessageServerLobbyInit } from "@/lobby/web-api/lobby-init.s2c";
import { S2CMessageServerLobbyUpdate } from "@/lobby/web-api/lobby-update.s2c";
import {
  s2cGameActionRejectToDTO,
  S2CMessageServerActionReject,
} from "@/game/web-api/game-action-reject.s2c";
import { s2cLobbyOptionsToDTO } from "@/lobby/web-api/lobby-options.s2c";
import { s2cLobbyTeamsToDTO } from "@/lobby/web-api/lobby-teams.s2c";
import { s2cLobbyReconnectedToDTO } from "@/lobby/web-api/lobby-reconnected.s2c";
import { s2cLobbyJoinedToDTO } from "@/lobby/web-api/lobby-joined.s2c";
import { s2cLobbyCreatedToDTO } from "@/lobby/web-api/lobby-created.s2c";
import {
  AnyGameIds,
  ServerCommand,
  SocketMessageLowLevelDTO,
} from "@raining.cards/common";
import { s2cErrorToDTO, S2CMessageServerError } from "@/api/ws/error.s2c";
import {
  S2CMessageServerNotification,
  s2cNotificationToDTO,
} from "@/api/ws/notification.s2c";

export type S2CMessageServer<GI extends AnyGameIds, GameProps> =
  | S2CMessageServerNotification
  | S2CMessageServerError
  | S2CMessageServerLobbyInit<GI, GameProps>
  | S2CMessageServerLobbyUpdate<GI, GameProps>
  | S2CMessageServerGameMeta<GI>
  | S2CMessageServerGameLog
  | S2CMessageServerUpdateKeyframe<GI>
  | S2CMessageServerUpdateDelta<GI>
  | S2CMessageServerUpdateClosure
  | S2CMessageServerActionReject;

const S2C_TO_DTO: {
  [Cmd in ServerCommand]: <GI extends AnyGameIds, GameProps>(
    msg: S2CMessageServer<GI, GameProps> & { cmd: Cmd }
  ) => unknown;
} = {
  [ServerCommand.LOBBY_CREATED]: s2cLobbyCreatedToDTO,
  [ServerCommand.LOBBY_JOINED]: s2cLobbyJoinedToDTO,
  [ServerCommand.LOBBY_RECONNECTED]: s2cLobbyReconnectedToDTO,
  [ServerCommand.LOBBY_TEAMS]: s2cLobbyTeamsToDTO,
  [ServerCommand.GAME_OPTIONS]: s2cLobbyOptionsToDTO,

  [ServerCommand.GAME_META]: s2cGameMetaToDTO,
  [ServerCommand.GAME_LOG]: s2cGameLogToDTO,
  [ServerCommand.UPDATE_KEYFRAME]: s2cGameUpdateKeyframeToDTO,
  [ServerCommand.UPDATE_DELTA]: s2cGameUpdateDeltaToDTO,
  [ServerCommand.UPDATE_CLOSURE]: s2cGameUpdateClosureToDTO,
  [ServerCommand.ACTION_REJECT]: s2cGameActionRejectToDTO,

  [ServerCommand.ERROR]: s2cErrorToDTO,
  [ServerCommand.NOTIFICATION]: s2cNotificationToDTO,
};

export function s2cMessageToDTO<GI extends AnyGameIds, GameProps>(
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  data: S2CMessageServer<GI, GameProps>
): SocketMessageLowLevelDTO {
  // todo create formatted message strings rather than JSON.stringify for better
  //      performance and less traffic
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  return `${data.cmd}:${JSON.stringify(S2C_TO_DTO[data.cmd](data))}`;
}
