import { defineAction, IdFactory, Transaction } from "@raining.cards/server";
import { ElementType, GameIds } from "@raining.cards/common";
import { Zones } from "@/zones";
import { Materials } from "@/material/materials";
import { Hero, HEROES } from "@/material/hero-materials";

export function useChooseHeroAction(
  idf: IdFactory<GameIds["actionSchema"]>,
  materials: Materials,
  zones: Zones,
  transaction: Transaction
) {
  return HEROES.map((it) => {
    const hero = materials.heroes[it];
    return defineAction(
      {
        id: idf(`choose-hero:${it}`),
        name: "choose",
        origin: {
          type: ElementType.MATERIAL,
          position: [{ zone: zones.hero }],
          materialView: { public: [hero.face.type] },
        },
        target: { type: ElementType.GLOBAL },
      },
      async (): Promise<Hero> => {
        transaction.destroyAll(zones.hero);
        return it;
      }
    );
  });
}
