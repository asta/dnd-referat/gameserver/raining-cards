import {
  defineAction,
  IdFactory,
  MaterialFaceSingletonDefinition,
} from "@raining.cards/server";
import { ElementType, GameIds } from "@raining.cards/common";
import { Materials } from "@/material/materials";

export function useChooseMonsterAction(
  idf: IdFactory<GameIds["actionSchema"]>,
  materials: Materials
) {
  return materials.monsterFaceTypes.map((it) =>
    defineAction(
      {
        id: idf(`choose-monster:${it.type.id}`),
        name: it.type.label!,
        description: it.type.description,
        origin: { type: ElementType.GLOBAL },
        target: { type: ElementType.GLOBAL },
      },
      async (): Promise<MaterialFaceSingletonDefinition> => it
    )
  );
}
