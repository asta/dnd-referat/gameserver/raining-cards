# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## [Unreleased]

## 2021-04-22

### Changed

- Adopt new client API (nested zones instead of domains, and some
  type/attribute/function renames)
- Update games & dependencies

### Added

- Tooltips for action and material descriptions

## 2021-03-11

### Added

- Initial Release
