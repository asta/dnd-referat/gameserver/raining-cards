import {
  S2CMessageDTONotification,
  S2CNotificationType,
} from "@raining.cards/common";

export interface S2CMessageClientNotification {
  date?: Date;
  type: S2CNotificationType;
  timeout?: number | false;
  title?: string;
  message: string;
}

export function s2cNotificationFromDTO([
  timestamp,
  type,
  timeout,
  message,
  title,
]: S2CMessageDTONotification): S2CMessageClientNotification {
  return {
    date: timestamp !== null ? new Date(timestamp) : undefined,
    type,
    timeout: timeout ?? undefined,
    title: title ?? undefined,
    message,
  };
}
