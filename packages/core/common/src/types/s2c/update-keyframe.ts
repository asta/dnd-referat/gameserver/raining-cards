import { ActionRevisionDTO, AnyGameIds, ZoneDTO } from "@/types";

export type S2CMessageDTOUpdateKeyframe<GI extends AnyGameIds> = [
  ActionRevisionDTO<GI> | null,
  ZoneDTO<GI>[]
];
