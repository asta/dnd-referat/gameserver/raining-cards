import {
  AnyGameIds,
  GameBoxDTO,
  MaterialChunkDTO,
} from "@raining.cards/common";
import { GameBox } from "@/game/game.types";
import { Zone } from "@/game/zone/zone.types";

export function gameBoxToDTO(gameBox: GameBox): GameBoxDTO {
  return {
    id: gameBox.id,
    name: gameBox.name,
    description: gameBox.description,
    lobby: gameBox.lobby,
  };
}

export interface ZoneWithMaterialChunks<GI extends AnyGameIds> {
  zone: Zone<GI>;
  children?: ZoneWithMaterialChunks<GI>[];
  materialChunks: MaterialChunkDTO<GI>[];
}
