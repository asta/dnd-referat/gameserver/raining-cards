import { ValueOf } from "ts-essentials";

export function mapObject<T, U>(
  obj: T,
  iteratee: <K extends keyof T>(value: T[K], key: K) => U
): U[] {
  const result: U[] = [];
  for (const key in obj) {
    result.push(iteratee(obj[key], key));
  }
  return result;
}

export function mapValues<T, U>(
  obj: T,
  iteratee: <K extends keyof T>(value: T[K], key: K) => U
): Record<keyof T, U> {
  const result = {} as Record<keyof T, U>;
  for (const key in obj) {
    result[key] = iteratee(obj[key], key);
  }
  return result;
}

export function mapKeys<T, RK extends string | number>(
  obj: T,
  iteratee: <K extends keyof T>(value: T[K], key: K) => RK
): Record<RK, ValueOf<T>> {
  const result = {} as Record<RK, ValueOf<T>>;
  for (const key in obj) {
    const value = obj[key];
    result[iteratee(value, key)] = value;
  }
  return result;
}

export function pluckId<T extends { id: unknown }>(obj: T): T["id"] {
  return obj.id;
}

export function fromEntries<Key extends string | number, Value>(
  entries: [Key, Value][]
): Record<Key, Value> {
  return Object.fromEntries(entries) as Record<Key, Value>;
}

export function omit<T, K extends keyof T>(
  it: T,
  keys: K | K[] | Set<K>
): Omit<T, K> {
  if (!(keys instanceof Set)) {
    keys = Array.isArray(keys) ? new Set(keys) : new Set([keys]);
  }
  const clone = {} as Omit<T, K>;
  for (const key in it) {
    if (!(keys as Set<keyof T>).has(key)) {
      (clone as any)[key] = it[key];
    }
  }
  return clone;
}

export function pick<T, K extends keyof T>(
  it: T,
  keys: K | K[] | Set<K>
): Pick<T, K> {
  if (!(keys instanceof Set)) {
    keys = Array.isArray(keys) ? new Set(keys) : new Set([keys]);
  }
  const clone = {} as Pick<T, K>;
  for (const key in it) {
    if ((keys as Set<keyof T>).has(key)) {
      (clone as any)[key] = it[key];
    }
  }
  return clone;
}
