import * as WebSocket from "ws";
import {
  deleteClientContext,
  getClientContext,
} from "@/lobby/participant-context";
import { C2SMessageServer } from "@/api/ws/c2s.parser";
import { c2sGameStartHandler } from "@/game/web-api/game-start.c2s.handler";
import { c2sGameActionHandler } from "@/game/web-api/game-action.c2s.handler";
import { c2sLobbyReconnectHandler } from "@/lobby/web-api/lobby-reconnect.c2s.handler";
import { c2sLobbyOptionsHandler } from "@/lobby/web-api/lobby-options.c2s.handler";
import { c2sLobbyDisconnectHandler } from "@/lobby/web-api/lobby-disconnect.c2s.handler";
import { c2sLobbyCreateHandler } from "@/lobby/web-api/lobby-create.c2s.handler";
import { c2sLobbyJoinHandler } from "@/lobby/web-api/lobby-join.c2s.handler";
import { c2sLobbyChangeTeamHandler } from "@/lobby/web-api/lobby-change-team.c2s.handler";
import { AnyGameIds, ClientCommand, GameIds } from "@raining.cards/common";
import { c2sGameAbortHandler } from "@/game/web-api/game-abort.c2s.handler";

const HANDLERS: {
  [Key in ClientCommand]: (
    socket: WebSocket,
    msg: C2SMessageServer<GameIds, unknown> & { cmd: Key }
  ) => void;
} = {
  [ClientCommand.LOBBY_CREATE]: c2sLobbyCreateHandler,
  [ClientCommand.LOBBY_JOIN]: c2sLobbyJoinHandler,
  [ClientCommand.LOBBY_RECONNECT]: c2sLobbyReconnectHandler,

  [ClientCommand.LOBBY_CHANGE_TEAM]: c2sLobbyChangeTeamHandler,
  [ClientCommand.GAME_OPTIONS]: c2sLobbyOptionsHandler,

  [ClientCommand.GAME_START]: c2sGameStartHandler,
  [ClientCommand.GAME_ABORT]: c2sGameAbortHandler,
  [ClientCommand.GAME_ACTION]: c2sGameActionHandler,
};

export function c2sRouter<GI extends AnyGameIds, GameProps>(
  socket: WebSocket,
  msg: C2SMessageServer<GI, GameProps>
) {
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  HANDLERS[msg.cmd]!(socket, msg as any);
}

export function clientDisconnectHandler<GI extends AnyGameIds>(
  socket: WebSocket
) {
  const ctx = getClientContext<GI>(socket);
  if (ctx == null) {
    return;
  }
  c2sLobbyDisconnectHandler(socket, ctx);
  ctx.client.socket = null;
  deleteClientContext(socket);
}
