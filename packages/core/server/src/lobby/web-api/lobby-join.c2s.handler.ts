import { abortLobbyCleanup, joinLobby } from "@/lobby/Lobby";
import { getKeyframeMessage, getMetaMessage } from "@/game/game";
import { C2SMessageServerLobbyJoin } from "@/lobby/web-api/lobby-join.c2s";
import { createClientContext } from "@/lobby/participant-context";
import { broadcastOthers, send } from "@/lobby/communication";
import * as WebSocket from "ws";
import { AnyGameIds, ServerCommand } from "@raining.cards/common";

export function c2sLobbyJoinHandler<GI extends AnyGameIds, GameProps>(
  socket: WebSocket,
  { clientName, lobby }: C2SMessageServerLobbyJoin<GI, GameProps>
) {
  const { client } = createClientContext(socket, lobby, clientName);
  joinLobby(lobby, client);
  abortLobbyCleanup(lobby);
  send(client, { cmd: ServerCommand.LOBBY_JOINED, lobby, client });
  broadcastOthers(client, lobby.participants, {
    cmd: ServerCommand.LOBBY_TEAMS,
    lobby,
  });
  if (lobby.game != null) {
    send(client, getMetaMessage(lobby.game, client, false));
    send(client, getKeyframeMessage(lobby.game, client, false));
  }
}
