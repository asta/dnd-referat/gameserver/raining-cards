import { Condition, ListAndLookup } from "@raining.cards/util";
import { UIActionSchema } from "@/game/action-schema.types";
import {
  AnyGameIds,
  ClientDTO,
  ElementType,
  ElementTypeSingle,
  GameBoxDTO,
  GameIds,
  MaterialFaceTypeDTO,
} from "@raining.cards/common";
import { UIZone, UIZoneType } from "@/area/zone.types";

export type UIGameBox = GameBoxDTO;
// TODO add `self` attribute to UIClient
export type UIClient = ClientDTO;
export type UIMaterialFaceType<
  GI extends AnyGameIds = GameIds
> = MaterialFaceTypeDTO<GI>;

export interface UILobby {
  id: string;
  gameBox: UIGameBox;
  options: unknown;
  creator: UIClient;
  participants: UIParticipants;
}

export interface UIParticipants {
  players: ListAndLookup<UIClient>;
  spectators: UIClient[];
  teams?: UIClient[][];
}

export interface UIGame<GI extends AnyGameIds = GameIds> {
  meta: UIGameMeta<GI>;
  zones: {
    asTree: UIZone<GI>[];
    asMap: Record<GI["zone"], UIZone<GI>>;
    byType: Record<GI["zoneType"], UIZone<GI>[]>;
  };
}

export interface UIGameLog {
  time: Date;
  message: string;
}

export interface UIGameMeta<GI extends AnyGameIds = GameIds> {
  actionSchemas: Record<GI["actionSchema"], UIActionSchema<GI>>;
  actionSchemaGroups: Record<GI["actionSchemaGroup"], UIActionSchema<GI>[]>;
  actionConditions: Record<
    GI["actionCondition"],
    Condition<UIMaterialPositionCount<GI>>
  >;

  materialFaceTypes: Record<GI["materialFaceType"], UIMaterialFaceType<GI>>;
  materialFaceTypeGroups: Record<
    GI["materialFaceTypeGroup"],
    Set<UIMaterialFaceType<GI>>
  >;

  zoneTypes: Record<GI["zoneType"], UIZoneType<GI>>;
}

export interface UIMaterialPositionCount<GI extends AnyGameIds = GameIds> {
  zoneTypes: UIZoneType<GI>[];
  zones: GI["zone"][];
  materialFaceTypes: UIMaterialFaceType<GI>[];
  count: { min: number; max: number };
}

export type UIActionSchemaTargetLookup<
  GI extends AnyGameIds = GameIds,
  Origin extends ElementType = ElementType
> = {
  [Target in ElementTypeSingle]: UIActionSchema<GI, Origin, Target>[];
};

export type UIActionSchemaLookup<GI extends AnyGameIds = GameIds> = {
  [Origin in ElementType]: UIActionSchemaTargetLookup<GI, Origin>;
};

export interface UIActionRevision<GI extends AnyGameIds = GameIds>
  extends UIActionSchemaLookup<GI> {
  revision: number;
  schemas: Set<UIActionSchema<GI>>;
}
