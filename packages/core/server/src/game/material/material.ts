import { Player } from "@/lobby/participant.types";
import { Zone } from "@/game/zone/zone.types";
import { positionFromLocator } from "@/game/material/material.position";
import {
  Material,
  MaterialLocator,
  MaterialPosition,
  MaterialWithPosition,
} from "@/game/material/material.types";
import { AnyGameIds, MaterialViewDTO } from "@raining.cards/common";
import { BakedReveal } from "@/game/material/reveal.types";

export function getMaterialView<GI extends AnyGameIds>(
  player: Player | null,
  material: Material<GI>,
  zone: Zone<GI>
): MaterialViewDTO<GI> {
  return createView(
    material,
    getGlobalFaceIdx(material, zone),
    getSecretFaceIdx(player, material, zone)
  );
}

export function getBakedMaterialView<GI extends AnyGameIds>(
  player: Player | null,
  material: Material<GI>,
  reveal: BakedReveal
): MaterialViewDTO<GI> {
  return createView(
    material,
    reveal.global ?? 0,
    player === null ? undefined : reveal.secret?.[player.id]
  );
}

function createView<GI extends AnyGameIds>(
  material: Material<GI>,
  globalIdx: number,
  secretIdx?: number
): MaterialViewDTO<GI> {
  return {
    global: material.faces[globalIdx],
    secret:
      secretIdx === undefined || secretIdx === globalIdx
        ? undefined
        : material.faces[secretIdx],
  };
}

export function bakeReveal<GI extends AnyGameIds>(
  { reveal }: Material<GI>,
  zone: Zone<GI>
): BakedReveal {
  const global = reveal.global ?? zone.reveal.global;
  const zoneSecret = zone.reveal.secret;
  if (reveal.secret !== undefined) {
    const secret = { ...reveal.secret };
    if (zoneSecret !== undefined) {
      for (const key in zoneSecret) {
        if (secret[key] === undefined) {
          secret[key] = zoneSecret[key];
        }
      }
    }
    return { global, secret } as BakedReveal;
  }
  if (zoneSecret !== undefined) {
    return { global, secret: { ...zoneSecret } } as BakedReveal;
  }
  return { global } as BakedReveal;
}

function getSecretFaceIdx<GI extends AnyGameIds>(
  player: Player | null,
  { faces, reveal }: Material<GI>,
  zone: Zone<GI>
): number | undefined {
  if (player === null) {
    return undefined;
  }
  const lastFaceIdx = faces.length - 1;
  const idx = reveal.secret?.[player.id];
  if (idx !== undefined) {
    return idx > lastFaceIdx ? lastFaceIdx : idx;
  }
  const zoneIdx = zone.reveal.secret?.[player.id];
  if (zoneIdx !== undefined) {
    return zoneIdx > lastFaceIdx ? lastFaceIdx : zoneIdx;
  }
  return undefined;
}

function getGlobalFaceIdx<GI extends AnyGameIds>(
  { faces, reveal }: Material<GI>,
  zone: Zone<GI>
): number {
  const lastFaceIdx = faces.length - 1;
  const idx = reveal.global;
  if (idx !== undefined) {
    return idx > lastFaceIdx ? lastFaceIdx : idx;
  }
  const zoneIdx = zone.reveal.global ?? 0;
  return zoneIdx > lastFaceIdx ? lastFaceIdx : zoneIdx;
}

export function accessOrThrow<GI extends AnyGameIds>(
  pos: MaterialLocator<GI>
): MaterialWithPosition<GI> {
  const position = positionFromLocator(pos);
  if (position.index < 0 || position.index >= pos.zone.materials.length) {
    throw new Error("Invalid State: Material not found.");
  }
  return {
    material: position.zone.materials[position.index],
    position,
  };
}

export function findOrThrow<GI extends AnyGameIds>(
  zone: Zone<GI>,
  material: Material<GI>
): MaterialPosition<GI> {
  const index = zone.materials.indexOf(material);
  if (index === -1) {
    throw new Error("Invalid State: Material not found.");
  }
  return {
    zone,
    index,
  } as MaterialPosition<GI>;
}
