# Raining Cards -- Game development

## Glossary

| Name          | Description                                                                                                      |
| ------------- | ---------------------------------------------------------------------------------------------------------------- |
| Material      | Some game relevant object that (in a real world) would be on the table. For example some card or figure.         |
| Material Face | Some face of any material. A token material for example would have one face, a card two and a common dice six.   |
| Zone          | Some area on the table that contains an ordered set of materials and nested zones.                               |
| Player        | Some client that is involved in playing the game. They may have secret knowledge of the game state.              |
| Spectator     | Some client without any involvement in the game. They only know public information of the game state.            |
| Transaction   | Some collection of ordered steps that modify the game state.                                                     |
| Action        | Some action that a player may perform within a game. Everything that involves a player choice must be an action. |

## Low-Level API

The low-level API for communication to the client consists of some `send*`,
`multicast*` and `broadcast*` functions that accept any well-defined message
object. The `nextClientAction` function can be used to wait for the next action
of some client (there can only ever be a single handler for each client).

Those are the fundament of the engine, but to develop a game it is highly
recommended using the high-level API instead. The following guide should give
you a basic step-by-step starting point for this. It also explains often used
patterns and tries to convey some of the more difficult aspects of game
development.

## Getting started

The engine requires any game to implement the `GameBox` interface (see
[packages/core/server/src/game/game.types.ts](https://gitlab.com/xoria/raining-cards/-/blob/master/packages/core/server/src/game/game.types.ts)).

However, you may want to use this guide to develop your first game. Refer to the
[Gardeners source code](https://gitlab.com/xoria/raining-cards/-/blob/master/packages/games/gardeners/src)
for any uncertainties. It is also recommended looking up the respective code of
Gardeners accompanying this guide.

Keep in mind: This engine is not built to be the simplest possible solution to
your problem, but to give you as much freedom as possible to design your
game(s).
For an experienced developer, it is definitely possible to define a simple game
within a day --
[Gardeners](https://gitlab.com/xoria/raining-cards/-/blob/master/packages/games/gardeners)
was initially done as a proof of concept for this --, but beginners probably
require a few working days for this.

To allow for way more comfortable game development, some high-level API exists,
called _comfort API_. The code can be found within
[packages/core/server/src/comfort](https://gitlab.com/xoria/raining-cards/-/tree/master/packages/core/server/src/comfort).

It is split into three categories:

1. Define -- Includes helpers to define your game meta and entry-point.
1. Use -- Includes helper-factories for state management.
1. Push -- Includes helpers for communication with the clients.

While the server API only requires you to properly implement the `GameBox`
interface, it is highly recommended utilizing the comfort API for any kind of
game.

An alternative approach to game development without use of the `define*` API and
factory function based code encapsulation can be seen within
[Table of Bluffs](https://gitlab.com/xoria/raining-cards/-/blob/master/packages/games/table-of-bluffs).
This one uses statically defined enums for its types and IDs. It also uses flat
structures with references by ID. It's up to you which pattern you like more,
but I recommend the `define*` API and the comfort API as a whole, which will be
used in this guide.

### GameBox

Use `defineGameBox` to describe your game box. Don't return anything from within
the game factory yet, we're building up to that.

We will use the game factory function for dependency wiring of all the different
components in the following steps.

### Game Meta

1. For the game meta definition, create a `useGameMeta` factory function that
   uses `defineGameMeta` to wire together static and client-specific meta
   information of the game, such as material face types, zone layout, etc. (as
   done in the following steps).
1. Create a factory function `useMaterialFaces` that uses
   `defineMaterialFaceSingleton` and `defineMaterialFace` to define any faces or
   face factories that need be available within the game. The latter is only
   needed if you need to override some appearance of different instances of the
   face during the game or for material definitions.
   Return all face definitions for lookup as well as a list of all face
   definitions for the game meta.
1. Create a type alias
   `type MaterialFaces = ReturnType<typeof useMaterialFaces>`.
1. Add a parameter to the `useGameMeta` factory like this:
   `materialFaces: MaterialFaces`. Pass the list of material faces to the static
   meta parameter (as `materialFaceTypes` attribute).
1. Call `useMaterialFaces` and pass its value to a call of `useGameMeta` within
   the game factory.
1. Create another factory `useMaterialFaceTypeGroups` that accepts a
   `MaterialFaces` parameter as well. Wire it together within the game factory;
   make sure to call `useMaterialFaces` only once. Use
   `defineMaterialFaceTypeGroup` to define any groups of the previously defined
   face types. You need groups for any material face types that should be
   treated as equal for any action condition. For example, in trick taking games
   you'd want to create a face type group for each card color as those are only
   allowed to be played conditionally.
1. The return value is to be wired to the `materialFaceTypeGroups` attribute for
   the game meta.

This is a simple pattern to manage dependencies between the different game
aspects. Use the same pattern for the following sections.

#### Zones

Define zones as needed for the game. This may be dependent on the players
present, so pass those through from the game factory parameter. It is best to
return the player-specific zones in some by-player lookup map since you probably
need to access them that way from the game logic.

Also define the board with `defineBoard`, passing the list of root-level zones.
The children hierarchy of zones needs to be assigned manually (subject to
change): `myZone.children = [myChildZoneA, myChildZoneB]`.

The zones (a flat list, not just the root zones) are to be wired to the
`zoneTypes` attribute for the game meta.

#### Materials

For materials, you should return either singletons, instances, or the
definitions (material factories) as required by the game logic. As you can see
within the
[Gardeners source code](https://gitlab.com/xoria/raining-cards/-/blob/master/packages/games/gardeners/src/material/material.ts),
we also define and return functions within the `useMaterials` factory function
to encapsulate all logic specifically related to materials or their definition,
such as a color and value association in this case. The same pattern is used to
group any aspects of the game logic later on as well.

#### Actions

Every action has an origin and a target. Origin and target types can be one of
global, zone, material or materials. The origin and target type determine the UI
aspect to use for that action. A global to global action would be a simple
trigger, e.g. button. A zone to global action would also be a simple trigger,
but located within the zone area. A material to zone action could be implemented
as drag&drop trigger.

To define, where an action is possible, the action schema can contain three
types of condition:

- `condition` -- The base condition of whether the action is possible.
- `originCondition` -- Some restrictions on the action origin, e.g. only within
  some specific zone or within zones of some type.
- `targetCondition` -- Same as origin condition, but for the action target.

All of this is defined within the game meta, so all action schemas are known by
the clients, for them to evaluate those conditions when needed.

While those conditions define what actions are possible, the action ID (or
rather the action group IDs) in contrast specifies which actions are allowed for
some client at some point in time. With every game state update to the clients,
an action revision can be attached in order to specify the allowed action
groups -- til the next revision.

Since different clients may need to have different actions defined, the game
meta can be specified on a client specific basis. For this the second parameter
of the `defineGameMeta` function is called once for every client to obtain its
specific game meta. This is very common for actions, as one for example should
not be allowed to act upon other players zones in the same way as on your own
zones. Note that any client-specific meta within `defineGameMeta` overrides the
same attribute of the static meta, it is _not_ merged together deeply.

Similarly to the dependency wiring within the `defineGameBox`, it is common to
wire together client-specific meta within the `defineGameMeta` parameter
function.

Now that you know the basics, you should be able to define your actions using
`defineAction` to define the necessary schemas for your game. Keep the handlers
empty for now.

### Game Logic

#### Main loop

At last, you can use `defineGame` to wire together the game meta, the game
board and game logic. The main game logic (e.g. turn loop) can be placed as
async function as the `main` attribute for `defineGame`. Similarly, the game
result is to be returned from a function as `closure` attribute. Return the game
definition at the end of the game factory.

#### Game state and communication

For state manipulation, such as moving / creating / destroying materials as well
as assigning allowed actions to players, you probably want to use the
`useTransaction` factory. Similar to the `use*` factory functions that you have
created above, the `useTransaction` function is supposed to be called just once
and to pass the instance to all dependents. It provides you with a lot of
methods for state modification and action assignments and provides `flush` and
`close` methods to bundle all previous modifications and provide them in a
format that is consumable by the `push*` API. For convenience there is an alias
`pushUpdate` method as well to call `pushMultiUpdate` with the result of
`close`. This will send the modification bundle and action revision to the
individual clients and resolve once the first client responds with an action.

The client action contains the schema that it is based on, so you can look up
the action handler to call (subject to be lifted by comfort API).
Within the action handlers you can use the same `Transaction` instance to modify
the game state again and so on.

In addition to the `useTransaction` function, a few other `use*` functions are
available within the comfort API, such as `useTurnOrder`, `useGameLogs` and
`useExecutorStack`. Especially the former ones are probably useful for most game
implementations. They should be quite simple to understand.

### Host the game

You should now be set to start the game, when running a server and web UI. See
[scaffold/README.md](https://gitlab.com/xoria/raining-cards/-/tree/master/scaffold/README.md)
for details on how to set the server and web UI up.

As this engine is in early development state, and the guide has not been seen by
many eyes yet, any feedback is highly appreciated. If you have any unanswered
questions, after following the guide, feel free to ask (since there is no other
means of communication yet, feel free to use the issue tracker for now).
Still, keep in mind that as of this development state, there is no properly
documented API, so the code (types, function headers, etc.) should serve as
source of information as well.
