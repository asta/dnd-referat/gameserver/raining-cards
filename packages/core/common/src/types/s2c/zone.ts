import { AnyGameIds, MaterialChunkDTO, PlayerDTO, UIMeta } from "@/types";

export interface ZoneTypeDTO<GI extends AnyGameIds> {
  id: GI["zoneType"];
  label?: string;
  description?: string;
  relatedTo?: PlayerDTO["id"][];
  uiMeta?: UIMeta;
}

export interface ZoneDTO<GI extends AnyGameIds> {
  id: GI["zone"];
  typeId: GI["zoneType"];
  children?: ZoneDTO<GI>[];
  materialChunks?: MaterialChunkDTO<GI>[];
  customMeta?: Partial<Omit<ZoneTypeDTO<GI>, "id">>;
}
