import {
  S2CMessageDTOActionReject,
  ServerCommand,
} from "@raining.cards/common";

export interface S2CMessageServerActionReject {
  cmd: ServerCommand.ACTION_REJECT;
  actionIdx: number;
  errorMsg: string;
}

export function s2cGameActionRejectToDTO({
  actionIdx,
  errorMsg,
}: S2CMessageServerActionReject): S2CMessageDTOActionReject {
  return [actionIdx, errorMsg];
}
