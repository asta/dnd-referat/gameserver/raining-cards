import { ToBActionCondition } from "@/condition";
import { ToBActionSchemaGroup, ToBActionSchema } from "@/action/action.types";
import { AnyGameIds, UIMeta } from "@raining.cards/common";
import { StringifySet } from "@raining.cards/util";
import { ToBZoneType } from "@/zone/zone-types";
import {
  ToBMaterialFaceType,
  ToBMaterialFaceTypeGroup,
} from "@/material/face-types";

export interface ToBGameIds extends AnyGameIds {
  actionSchema: ToBActionSchema | string;
  actionSchemaGroup: ToBActionSchemaGroup;
  actionCondition: ToBActionCondition | string;

  material: string;
  materialFaceType: ToBMaterialFaceType;
  materialFaceTypeGroup: ToBMaterialFaceTypeGroup;

  zone: string;
  zoneType: ToBZoneType;
}

export interface ToBPlayerUIMeta extends UIMeta {
  // todo define enum
  classNames: StringifySet<string>;
}
