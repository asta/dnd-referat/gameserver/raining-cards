import { ToBMaterialFaceType } from "@/material/face-types";
import { PlayerZones } from "@/zone/zones";
import { ToBZoneType } from "@/zone/zone-types";

export function deriveTeam(
  zones: PlayerZones
): ToBMaterialFaceType.TEAM_BLUE | ToBMaterialFaceType.TEAM_RED {
  const material = zones[ToBZoneType.PLAYER_TEAM].materials[0];
  const {
    reveal: { global },
    faces,
  } = material;
  return faces[global!].typeId as
    | ToBMaterialFaceType.TEAM_BLUE
    | ToBMaterialFaceType.TEAM_RED;
}
