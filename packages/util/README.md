[Base Repository](https://gitlab.com/xoria/raining-cards/)

# Raining Cards: Utilities

A collection of utilities as used by the _raining.cards_ core and for common use
by _raining.cards_ instances.
