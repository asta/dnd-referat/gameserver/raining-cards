import { ActionRevisionDTO, AnyGameIds, MaterialViewDTO } from "@/types";

export type S2CMessageDTOUpdateDelta<GI extends AnyGameIds> = [
  ActionRevisionDTO<GI> | null,
  MaterialUpdateDelta<GI>[]
];

export type MaterialUpdateDelta<GI extends AnyGameIds> =
  | MaterialUpdateDeltaCreate<GI>
  | MaterialUpdateDeltaDestroy<GI>
  | MaterialUpdateDeltaUpdate<GI>
  | MaterialUpdateDeltaFlush;

export enum DeltaType {
  CREATE = "create",
  DESTROY = "destroy",
  UPDATE = "update",
  FLUSH = "flush",
}

interface MaterialUpdateDeltaBase {
  type: DeltaType;
}

export interface MaterialUpdateDeltaCreate<GI extends AnyGameIds>
  extends MaterialUpdateDeltaBase {
  type: DeltaType.CREATE;
  targetZoneId: GI["zone"];
  targetIndex: number;
  targetView: MaterialViewDTO<GI>;
}

export interface MaterialUpdateDeltaDestroy<GI extends AnyGameIds>
  extends MaterialUpdateDeltaBase {
  type: DeltaType.DESTROY;
  originZoneId: GI["zone"];
  originIndex: number;
}

export interface MaterialUpdateDeltaUpdate<GI extends AnyGameIds>
  extends MaterialUpdateDeltaBase {
  type: DeltaType.UPDATE;
  originZoneId: GI["zone"];
  originIndex: number;
  targetZoneId?: GI["zone"];
  targetIndex?: number;
  targetView?: MaterialViewDTO<GI>;
}

export interface MaterialUpdateDeltaFlush extends MaterialUpdateDeltaBase {
  type: DeltaType.FLUSH;
  delay?: number;
}
