import { first, last, Transaction } from "@raining.cards/server";
import { Zones } from "@/zones";
import { Materials } from "@/material/materials";
import { shuffle, times } from "@raining.cards/util";
import { HeroData, HEROES } from "@/material/hero-materials";

export type Dealer = ReturnType<typeof useDealer>;

export function useDealer(
  transaction: Transaction,
  materials: Materials,
  zones: Zones
) {
  return { clean, showHeroes, deal, addHP, removeHP, countHP };

  function clean() {
    const { dungeonDiscard } = zones;
    for (let i = 0; i < dungeonDiscard.materials.length; i++) {
      transaction.revealGlobal({ zone: dungeonDiscard, index: i }, 0);
    }
    for (const zone of zones.asList) {
      transaction.destroyAll(zone);
    }
  }

  function showHeroes() {
    for (const hero of HEROES) {
      transaction.create(
        materials.heroes[hero].material.instance,
        last(zones.hero)
      );
    }
  }

  function deal({ material, equipment }: HeroData) {
    const deckMaterials = shuffle(materials.monster.map((it) => it.factory()));
    for (const material of deckMaterials) {
      transaction.create(material, last(zones.deck));
    }
    transaction.create(material.instance, last(zones.hero));
    for (const it of equipment) {
      transaction.create(it.material.factory(), last(zones.equipment));
    }
  }

  function addHP(count: number) {
    times(count, () =>
      transaction.create(materials.hpToken.factory(), first(zones.hero))
    );
  }

  function removeHP(count: number) {
    const { hero } = zones;
    times(count, () => {
      if (materials.hpToken.is(hero.materials[0])) {
        transaction.destroy(first(hero));
      }
    });
  }

  function countHP() {
    const idx = zones.hero.materials.findIndex(
      (it) => !materials.hpToken.is(it)
    );
    return idx >= 0 ? idx : 0;
  }
}
