# Raining Cards

An online multiplayer card-game library.

**Work in progress!** This project is in early stage.
[Status details](https://gitlab.com/xoria/raining-cards/-/tree/master/README_STATUS.md)

## Demo

Well, "demo" does not really describe it well, but is used often and has gained
some recognition value as section title...

- [Stable instance](https://raining.cards)
- [Preview instance](https://storm.raining.cards) (less stable, more cutting
  edge)

These services are going to be maintained indefinitely, so feel free to use and
share; free of charge!

## Features

- 🧩 High degrees of freedom for game developers: It's more of a library than of a
  framework.
- 🪶 Lightweight Frontend: Easy on your users devices.
- 💪 Designed with performance in mind.
- ✨ Powerful generic UI: Game developers can focus on the game logic.
- 🃏 Enables a natural game experience for complex games.

## Documentation

- [Administration documentation](https://gitlab.com/xoria/raining-cards/-/tree/master/scaffold/README.md)
- [Game development documentation](https://gitlab.com/xoria/raining-cards/-/tree/master/README_GAME_DEV.md)
- [Core development documentation](https://gitlab.com/xoria/raining-cards/-/tree/master/README_CORE_DEV.md)

## Repository Structure

### Core Packages

The project core consists of three packages:

- [core/client](https://gitlab.com/xoria/raining-cards/-/tree/master/packages/core/client):
  Manages client-side state and communication.
- [core/server](https://gitlab.com/xoria/raining-cards/-/tree/master/packages/core/server):
  Provides the game server logic as well as API for game implementations.
- [core/common](https://gitlab.com/xoria/raining-cards/-/tree/master/packages/core/common):
  Types and enums to be shared between client and server (e.g. DTO definition).

In addition, there is a
[util package](https://gitlab.com/xoria/raining-cards/-/tree/master/packages/util)
that contains utilities, not strongly related to _raining.cards_.

### Scaffold

The scaffold directory contains code that can be used as a starting point to
spin up custom instances. See
[its README.md](https://gitlab.com/xoria/raining-cards/-/tree/master/scaffold/README.md)
for details.

### Games

Currently, there are two first party game implementations
([overview page](https://gitlab.com/xoria/raining-cards/-/tree/master/packages/games)):

1. [Table of Bluffs](https://gitlab.com/xoria/raining-cards/-/tree/master/packages/games/table-of-bluffs)
   (similar to
   [Coup:Reformation](https://indieboardsandcards.com/index.php/our-games/coup-reformation/))
1. [Gardeners](https://gitlab.com/xoria/raining-cards/-/tree/master/packages/games/gardeners)
   (similar to [Druids](https://www.amigo-spiele.de/spiel/druids))
1. [Who Knows Whom](https://gitlab.com/xoria/raining-cards/-/tree/master/packages/games/who-knows-whom)
   (companion to simple yes/no/other question sessions)

## Footnote

This project is published under the GNU Affero General Public License Version 3.
Other license options can be negotiated; contact
[license+raining-cards@xoria.de](mailto:license+raining-cards@xoria.de). Third
party license agreements, different from AGPLv3, will be publicly disclosed at
this place. At this point in time, there are none.

For legal reasons contact
[legal+raining-cards@xoria.de](mailto:legal+raining-cards@xoria.de). I did not
mean any harm and am happy to collaborate in order to resolve potential issues.

Best regards,
_xoria_
